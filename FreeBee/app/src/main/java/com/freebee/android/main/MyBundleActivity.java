package com.freebee.android.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.common.Common;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.adapter.BundleListAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.base.CommonTabActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MyBundleActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    ListView lst_bundle;
    BundleListAdapter _adapter;
    ArrayList<ProductEntity> allCart = new ArrayList<>();
    Button btn_ship;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bundle);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        btn_ship = (Button)findViewById(R.id.btn_ship);
        btn_ship.setOnClickListener(this);
        lst_bundle = (ListView)findViewById(R.id.lst_bundle);
        _adapter = new BundleListAdapter(this);
        lst_bundle.setAdapter(_adapter);

        getCartProducts();

    }

    private void getCartProducts(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_CART;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetCart(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseGetCart(String response){

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray jsonArray = object.getJSONArray(ReqConst.RES_PRODUCT_LIST);

                for (int i = 0; i < jsonArray.length(); i++){

                    JSONObject product =  (JSONObject)jsonArray.get(i);

                    ProductEntity cart = new ProductEntity();

                    int qty_available = product.getInt(ReqConst.REQ_QUANTITY_AVAILABLE);
                    int cart_count = product.getInt(ReqConst.REQ_COUNT);
                    int sold_count = product.getInt(ReqConst.RES_SOLD_COUNT);

                    cart.set_id(product.getInt(ReqConst.RES_PRODUCT_ID));
                    cart.set_name(product.getString(ReqConst.RES_PRODUCT_NAME));
                    cart.set_description(product.getString(ReqConst.RES_PRODUCT_DESCRIPTION));
                    cart.set_imagUrl(product.getString(ReqConst.RES_PRODUCT_IMAGE));
                    cart.set_ownerName(product.getString("product_username"));
                    cart.set_cart_count(cart_count);
                    cart.setType(product.getString(ReqConst.REQ_TYPE));

                    if (qty_available - sold_count < cart_count) continue;

                    allCart.add(cart);
                }

                Commons.orderProducts = allCart;
                _adapter.addAllCart(this, allCart);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void confirmRemoveCart(ProductEntity productEntity){


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_confirm);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        Button btn_remove = (Button) dialog.findViewById(R.id.btn_remove);
        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _adapter.removeCart(productEntity);
                dialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });

        dialog.show();
    }


    public void gotoUserProfile(UserEntity user){

        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(Constants.USER, user);
        startActivity(intent);
    }

    private int getRegular(){

        int regular = 0;

        for (int i = 0; i < allCart.size(); i++){

            if (allCart.get(i).getType().equalsIgnoreCase(Constants.REGULAR))
                regular += allCart.get(i).get_cart_count();
        }

        return regular;
    }

    private int getPremium(){

        int premium = 0;

        for (int j = 0; j < allCart.size(); j++) {

            if (allCart.get(j).getType().equalsIgnoreCase(Constants.PREMIUM))

                premium += allCart.get(j).get_cart_count();
        }

        return premium;
    }

    private boolean checkCount(){

        int _premium = getPremium();
        int _regular = getRegular();

        Log.d("p : : r", String.valueOf(_premium) + ":" + _regular);

        if (_premium < 5){

            showAlertDialog("Please choose " + String.valueOf(5 - _premium) + " more premium item to proceed");
            return false;
        }
        else if (_premium > 5){

            showAlertDialog("You have exceeded the limit of premium items. Please remove "
                    + String.valueOf(_premium - 5) + " of premium items from your bundle to checkout.");
            return false;
        }
        else if (_regular < 10){

            showAlertDialog("Please choose " + String.valueOf(10 - _regular) + " more regular items to proceed");
            return false;
        }

        else if (_regular > 10){

            showAlertDialog("You have exceeded the limit of premium items. Please remove "
                    + String.valueOf(_regular - 10) + " of regular items from your bundle to checkout.");
            return false;
        }


        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.btn_ship:

                if (checkCount()){

                    startActivity(new Intent(MyBundleActivity.this, GetAddressActivity.class));
                    finish();
                }

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
