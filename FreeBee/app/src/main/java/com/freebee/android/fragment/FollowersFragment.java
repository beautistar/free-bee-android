package com.freebee.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.freebee.android.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;

import com.freebee.android.adapter.ListViewFollowerAdapter;
import com.freebee.android.base.BaseFragment;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.main.MyBundleActivity;
import com.freebee.android.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by GoldRain on 10/12/2016.
 */

@SuppressLint("ValidFragment")
public class FollowersFragment extends BaseFragment{

    ArrayList<UserEntity> _followers = new ArrayList<>();

    TextView ui_txvInvite;
    Context _context;
    MyBundleActivity _activity;
    ListView ui_lstFollower;
    ListViewFollowerAdapter _adapter;

    public FollowersFragment(Context context) {
        this._context = context;
        this._activity = (MyBundleActivity)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_followers, container,false);

        _adapter = new ListViewFollowerAdapter(_activity);
        ui_lstFollower = (ListView)view.findViewById(R.id.lst_follower);
        ui_lstFollower.setAdapter(_adapter);

        ui_txvInvite = (TextView)view.findViewById(R.id.txv_invite_friends);
        ui_txvInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.txv_invite_friends:
                        Share("", "");
                        break;
                }
            }
        });

        followerList();

        return view ;
    }

    private void followerList(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_USERFOLLOWER;
        StringRequest myRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                parseResponseFollower(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.showAlertDialog(getString(R.string.error));
                _activity.closeProgress();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                } catch (Exception e) {}

                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);
    }

    private void parseResponseFollower(String response){

        _activity.closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            Log.d("======follower=====responds", response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray followers = object.getJSONArray(ReqConst.RES_USERLIST);

                for (int i = 0; i < followers.length(); i++){

                    JSONObject jsonObject = (JSONObject)followers.get(i);

                    UserEntity follower = new UserEntity();

                    follower.set_idx(jsonObject.getInt(ReqConst.RES_USERID));
                    follower.set_name(jsonObject.getString(ReqConst.RES_USERNAME));
                    follower.set_imageUrl(jsonObject.getString(ReqConst.RES_USERIMAGE));

                    _followers.add(follower);
                }

                _adapter.setData(_followers);
                _adapter.notifyDataSetChanged();

            } else {

                _activity.closeProgress();
            }

        } catch (JSONException e) {

            _activity.closeProgress();
           // _activity.showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

}
