package com.freebee.android.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.freebee.android.R;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.DetailImageActivity;
import com.freebee.android.main.ProductActivity;

import java.util.ArrayList;

/**
 * Created by STS on 10/18/2016.
 */

public class ProductViewPagerAdapter extends PagerAdapter {

    ArrayList<String> imgs = new ArrayList<>();

    LayoutInflater mLayoutInflater;

    ProductActivity activity;

    ImageView ui_imvBack, ui_imvReport, ui_imvPrice, ui_imvLike;

    public ProductViewPagerAdapter(ProductActivity activity) {

        this.activity = activity;
        mLayoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<String> data){
        imgs.clear();
        imgs.addAll(data);
    }

    @Override
    public int getCount() {
        return imgs.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_product_viewpager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imv_product);
        //imageView.setImageResource(imgs.get(position));

        Glide.with(activity).load(imgs.get(position)).placeholder(R.drawable.ic_logo).into(imageView);

  /*      ui_imvBack = (ImageView)itemView.findViewById(R.id.imv_back2);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });*/

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, DetailImageActivity.class);
                intent.putExtra(Constants.IMAGES, imgs);
                intent.putExtra(Constants.POSITION, position);
                activity.overridePendingTransition(R.anim.left_in, R.anim.right_out);
                activity.startActivity(intent);

            }
        });


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
