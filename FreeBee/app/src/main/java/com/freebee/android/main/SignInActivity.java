package com.freebee.android.main;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.common.Common;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.freebee.android.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;

import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.freebee.android.commons.ReqConst.CODE_SUCCESS;

public class SignInActivity extends CommonActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    EditText ui_edtEmail, ui_edtPassword;
    TextView ui_txvNewUser, txv_signin;
    String _email = "";
    String _password = "";

    public static final int REGISTER_CODE = 100;
    private GoogleApiClient mGoogleApiClient;
    public static CallbackManager callbackManager;
    public static final int RC_SIGN_IN = 9001;

    ImageView imv_sign, imv_fb, imv_google;

    String social_email = "", social_id = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this/* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        initValues();
        loadLayout();
    }

    boolean _isFromLogout = false;

    private void initValues(){

        Intent intent = getIntent();

        try{
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);
        } catch (Exception e){}
    }

    private void loadLayout() {

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPassword = (EditText)findViewById(R.id.edt_password);

        ui_txvNewUser = (TextView)findViewById(R.id.txv_newuser);
        ui_txvNewUser.setOnClickListener(this);

        imv_sign = (ImageView)findViewById(R.id.imv_signin);
        imv_sign.setOnClickListener(this);

        imv_fb = (ImageView)findViewById(R.id.imv_fb);
        imv_fb.setOnClickListener(this);

        imv_google = (ImageView)findViewById(R.id.imv_google);
        imv_google.setOnClickListener(this);

        LinearLayout lytContainer = (LinearLayout)findViewById(R.id.lyt_container_singin);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmail.getWindowToken(),0);
                return false;
            }
        });

        if(_isFromLogout){

            //save user to empty.
            Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");

            ui_edtEmail.setText("");
            ui_edtPassword.setText("");


        } else {

            String email = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USEREMAIL, "");
            String pwd = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

            if (email.length() > 0 && pwd.length() > 0) {

                _email = email;
                _password = pwd;

                ui_edtEmail.setText(_email);
                ui_edtPassword.setText(_password);

                //cancel once complete under
                gotoLogin();
            }
        }

        //remove once complete
        ui_edtEmail.setText(_email);
        ui_edtPassword.setText(_password);

    }

    public boolean checkValid() {

        if (ui_edtEmail.getText().length() == 0) {
            showAlertDialog(getString(R.string.input_email));
            return false;
        } else if (ui_edtPassword.getText().length() == 0) {
            showAlertDialog(getString(R.string.input_password));
            return false;
        }

        return true;
    }

    private void gotoSignUp(){

        Intent intent = new Intent(this, SignUpActivity.class);
        startActivityForResult(intent, REGISTER_CODE);
        finish();
    }

    private void gotoLogin(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.REQ_EMAIL, ui_edtEmail.getText().toString());
                    params.put(ReqConst.REQ_PASSWORD, ui_edtPassword.getText().toString());

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseLogin(String response){

        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == CODE_SUCCESS){

                JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

                UserEntity _user = new UserEntity();

                _user.set_idx(userProfile.getInt(ReqConst.RES_USERID));
                _user.set_name(userProfile.getString(ReqConst.RES_USERNAME));
                _user.set_imageUrl(userProfile.getString(ReqConst.RES_USERIMAGE));
                _user.set_count_followers(userProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                _user.set_count_following(userProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                _user.set_isVerified(userProfile.getInt(ReqConst.RES_ISVERIFIED));
                _user.set_verify_facebook(userProfile.getString(ReqConst.RES_VERIFYFACEBOOK));
                _user.set_verify_google(userProfile.getString(ReqConst.RES_VERIFYGOOGLE));
                _user.set_phone(userProfile.getString(ReqConst.REQ_PHONE));
                _user.setAddress(userProfile.getString("address"));
                _user.setCity(userProfile.getString("city"));
                _user.setState(userProfile.getString("state"));
                _user.setZip_code(userProfile.getString("zip_code"));
                _user.setAge(userProfile.getInt("age"));
                _user.setGender(userProfile.getString("gender"));
                _user.setBalance(userProfile.getString("balance"));

                JSONArray jsonArray_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);
                ArrayList<ProductEntity> list_of_product = new ArrayList<>();

                for (int i = 0; i < jsonArray_products.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_product.add(_list);
                }

                JSONArray product_likes = userProfile.getJSONArray(ReqConst.RES_PRODUCTLIKES);
                ArrayList<ProductEntity> list_of_like = new ArrayList<>();

                for (int i = 0; i < product_likes.length(); i++){

                    JSONObject jsonObject = (JSONObject) product_likes.get(i);

                    ProductEntity _like = new ProductEntity();

                    _like.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _like.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _like.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_like.add(_like);
                }

                JSONArray product_bought = userProfile.getJSONArray(ReqConst.RES_PRODUCTBOUGHT);
                ArrayList<ProductEntity> list_of_bought = new ArrayList<>();

                for (int i = 0; i < product_bought.length(); i++){

                    JSONObject jsonObject = (JSONObject)product_bought.get(i);

                    ProductEntity _bought = new ProductEntity();

                    _bought.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _bought.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _bought.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_bought.add(_bought);
                }


                Preference.getInstance().put(this,PrefConst.PREFKEY_USEREMAIL, ui_edtEmail.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtPassword.getText().toString().trim());

                Commons.g_user = _user;

                Commons.g_user.set_listProducts(list_of_product);
                Commons.g_user.set_boughtProducts(list_of_bought);
                Commons.g_user.set_likedProducts(list_of_like);

                registerToken();

                gotoMain();

            } else if (result_code == ReqConst.CODE_NOTEXISTEMAIL){

                showAlertDialog(getString(R.string.notexist_email));
                closeProgress();

            } else if (result_code == ReqConst.CODE_WRONGPWD){

                showAlertDialog(getString(R.string.wrong_password));
                closeProgress();
            }

        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }
    }

    private void registerToken(){

        if (Prefs.getString(Constants.TOKEN, "").length() == 0){
            return;
        }

        String url = ReqConst.SERVER_URL + "register_token";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));
                    params.put("token", Prefs.getString(Constants.TOKEN, ""));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REGISTER_CODE:

                if (resultCode == RESULT_OK) {

                    // load saved user
                    String email = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USEREMAIL, "");
                    String userpwd = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERPWD, "");

                    ui_edtEmail.setText(email);
                    ui_edtPassword.setText(userpwd);

                    gotoMain();
                }

                break;

            case RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    private void googleVerify() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {

        Log.d("status", String.valueOf(result.isSuccess()) + " : " + result.getStatus().getStatusCode() +
                " : " + result.getStatus().getStatusMessage());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            social_email = acct.getEmail();
            social_id = acct.getId();

            processSocialLogin(social_email, social_id);

        } else {

        }
    }

    private void facebookVerify(){

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"/*, "user_photos"*/, "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {


                        try {

                            social_id = object.getString("id");

                            social_email = object.getString("email");
                            if (social_email.length() == 0){
                                social_email = social_id + "@facebook.com";
                            }

                            processSocialLogin(social_email, social_id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

            }

        });

    }

    private void processSocialLogin(String email, String id){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SOCIAL_LOGIN;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSocialLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.REQ_EMAIL, email);
                    params.put(ReqConst.REQ_SOCIAL_ID, id);

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseSocialLogin(String response){


        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == CODE_SUCCESS){

                JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

                UserEntity _user = new UserEntity();

                _user.set_idx(userProfile.getInt(ReqConst.RES_USERID));
                _user.set_name(userProfile.getString(ReqConst.RES_USERNAME));
                _user.set_imageUrl(userProfile.getString(ReqConst.RES_USERIMAGE));
                _user.set_count_followers(userProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                _user.set_count_following(userProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                _user.set_isVerified(userProfile.getInt(ReqConst.RES_ISVERIFIED));
                _user.set_verify_facebook(userProfile.getString(ReqConst.RES_VERIFYFACEBOOK));
                _user.set_verify_google(userProfile.getString(ReqConst.RES_VERIFYGOOGLE));

                _user.set_phone(userProfile.getString(ReqConst.REQ_PHONE));
                _user.setAddress(userProfile.getString("address"));
                _user.setCity(userProfile.getString("city"));
                _user.setState(userProfile.getString("state"));
                _user.setZip_code(userProfile.getString("zip_code"));
                _user.setAge(userProfile.getInt("age"));
                _user.setGender(userProfile.getString("gender"));

                JSONArray jsonArray_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);
                ArrayList<ProductEntity> list_of_product = new ArrayList<>();

                for (int i = 0; i < jsonArray_products.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_product.add(_list);
                }

                JSONArray product_likes = userProfile.getJSONArray(ReqConst.RES_PRODUCTLIKES);
                ArrayList<ProductEntity> list_of_like = new ArrayList<>();

                for (int i = 0; i < product_likes.length(); i++){

                    JSONObject jsonObject = (JSONObject) product_likes.get(i);

                    ProductEntity _like = new ProductEntity();

                    _like.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _like.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _like.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_like.add(_like);
                }

                JSONArray product_bought = userProfile.getJSONArray(ReqConst.RES_PRODUCTBOUGHT);
                ArrayList<ProductEntity> list_of_bought = new ArrayList<>();

                for (int i = 0; i < product_bought.length(); i++){

                    JSONObject jsonObject = (JSONObject)product_bought.get(i);

                    ProductEntity _bought = new ProductEntity();

                    _bought.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _bought.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _bought.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_bought.add(_bought);
                }

                Commons.g_user = _user;

                Commons.g_user.set_listProducts(list_of_product);
                Commons.g_user.set_boughtProducts(list_of_bought);
                Commons.g_user.set_likedProducts(list_of_like);

                gotoMain();

            } else if (result_code == ReqConst.CODE_NOTEXISTEMAIL){

                showToast(object.getString(ReqConst.RES_ERROR));
                closeProgress();

                gotoSignUp();

            } else if (result_code == ReqConst.CODE_WRONGPWD){

                showAlertDialog(getString(R.string.wrong_password));
                closeProgress();
            }

        } catch (JSONException e) {

            closeProgress();

            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_newuser:
                gotoSignUp();
                break;

            case R.id.imv_signin:

                if (checkValid()){

                    _email = ui_edtEmail.getText().toString().trim();
                    _password = ui_edtPassword.getText().toString().trim();

                    gotoLogin();
                }
                break;

            case R.id.imv_fb:
                facebookVerify();
                break;

            case R.id.imv_google:
                googleVerify();
                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
