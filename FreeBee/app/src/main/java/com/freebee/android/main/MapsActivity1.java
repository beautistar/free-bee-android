package com.freebee.android.main;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity1 extends FragmentActivity implements OnMapReadyCallback , View.OnClickListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient ;
    private Location mCurrentLocation ;
    public LatLng _latlng ;
    private int curMapTypeIndex = 1;
    private final int[] MAP_TYPES = {
            GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};

    TextView ui_txvLocation;
    ImageView ui_imvBack;
    double X = 0.0, Y = 0.0;

    public double w_fLatitude = 0.0;
    public double w_fLongitude = 0.0;
    public double w_fspeed = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps1);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        X = getIntent().getDoubleExtra(Constants.CURRENT_MYLATITUDE_X,0.00);
        Y = getIntent().getDoubleExtra(Constants.CURRENT_MYLONGITUDE_Y, 0.00);

        loadLayout();

    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
        }
    };

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_txvLocation = (TextView)findViewById(R.id.txv_userLocation);
        ui_txvLocation.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap = googleMap;

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (mGoogleApiClient == null){

            return;
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        try{

            mCurrentLocation = LocationServices
                    .FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

            //LatLng latLng= new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());


            w_fLatitude = mCurrentLocation.getLatitude();
            w_fLatitude = mCurrentLocation.getLongitude();

            if (X != 0.0 && Y != 0.0){

                w_fLatitude = X;
                w_fLongitude = Y;
            }

            LatLng latLng = new LatLng(w_fLatitude,w_fLongitude);


            MarkerOptions options = new MarkerOptions().position(latLng).title("Current My Loacation");
            options.icon(BitmapDescriptorFactory.defaultMarker());
            mMap.addMarker(options);
            initCamera(mCurrentLocation);

        }catch (Exception e){

        }

    }


    @Override
    public void onConnectionSuspended(int i) { }

    public void initCamera(final Location location) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(/*location.getLatitude()*/X,
                        /*location.getLongitude()*/ Y))
                .zoom(10f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        mCurrentLocation.setLatitude(X);
        mCurrentLocation.setLongitude(Y);

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);

        mMap.setMapType(MAP_TYPES[curMapTypeIndex]);
        mMap.setTrafficEnabled(true);
        mMap.setMyLocationEnabled(true);


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {                      // Add Marker
            @Override
            public void onMapClick(LatLng latlng) {

                mMap.clear();
                MarkerOptions options = new MarkerOptions().position(latlng);
                options.icon(BitmapDescriptorFactory.defaultMarker());
                mMap.addMarker(options);

                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlng, 12f);
                mMap.animateCamera(update);
//              LatLng latLng= new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
//              LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                Log.d("latlang=======", String.valueOf(latlng)) ;
                _latlng = latlng ;

                mCurrentLocation = new Location("position");
                mCurrentLocation.setLatitude(_latlng.latitude);
                mCurrentLocation.setLongitude(_latlng.longitude);
            }
        });

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.getUiSettings().setZoomControlsEnabled( true );
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                /*Intent intent  = new Intent(this ,PostActivity.class);
                startActivity(intent);*/
                finish();
                break;

            case R.id.txv_userLocation:

                Commons.g_postActivity.setMyLocation(mCurrentLocation);
                finish();

                break;
        }

    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}
