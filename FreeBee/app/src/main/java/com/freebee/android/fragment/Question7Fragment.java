package com.freebee.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;

public class Question7Fragment extends Fragment implements View.OnClickListener {

    QuestionsActivity activity;
    View view;

    TextView txv_yes, txv_no;

    public Question7Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question7, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_yes = (TextView)view.findViewById(R.id.txv_yes);
        txv_yes.setOnClickListener(this);

        txv_no = (TextView)view.findViewById(R.id.txv_no);
        txv_no.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_yes:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));

                break;

            case R.id.txv_no:

                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));

                break;


        }

    }



    private void setBackground(int a, int b, int e, int f){

        txv_yes.setBackgroundResource(a);
        txv_no.setBackgroundResource(b);

        txv_yes.setTextColor(e);
        txv_no.setTextColor(f);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity) context;
    }

}
