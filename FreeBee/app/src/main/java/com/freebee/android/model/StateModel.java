package com.freebee.android.model;

import java.util.ArrayList;

public class StateModel {

    int id = 0;
    String state = "";
    boolean _selected = false;
    ArrayList<String> cities = new ArrayList<>();

    ArrayList<CityModel> cityModels = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<String> getCities() {
        return cities;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
    }

    public boolean is_selected() {
        return _selected;
    }

    public void set_selected(boolean _selected) {
        this._selected = _selected;
    }

    public ArrayList<CityModel> getCityModels() {
        return cityModels;
    }

    public void setCityModels(ArrayList<CityModel> cityModels) {
        this.cityModels = cityModels;
    }
}
