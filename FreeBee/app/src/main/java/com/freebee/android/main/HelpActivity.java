package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.utils.RadiusImageView;

public class HelpActivity extends CommonActivity implements View.OnClickListener {

    NavigationView ui_drawer_menu;
    LinearLayout ui_lytSend_us, ui_lytTerms, lyt_faqs;
    ImageView ui_imvCallDrawer;
    DrawerLayout ui_drawerlayout;

    //LinearLayout ui_lyt_home, ui_lyt_post,ui_lyt_mynetwork, ui_lyt_setting, ui_lyt_invite, ui_lyt_help ,ui_lyt_myprofile;
    View headerView;

    //RelativeLayout ui_lyt_listed, ui_lyt_liked , ui_lyt_bought;

    TextView ui_txvName, ui_txvMyprofile ,txv_listed_num,txv_liked_num,txv_bought_num;
    RadiusImageView ui_imvPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        loadLayout();

        setupNavigationBar();

    }

    private void loadLayout() {

        ui_imvCallDrawer = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imvCallDrawer.setOnClickListener(this);

        ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        //headerView = ui_drawer_menu.getHeaderView(0);

        /*ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);
        ui_drawerlayout.setOnClickListener(this);*/

        //navigation header my profile part
        /*
        ui_txvName = (TextView)headerView.findViewById(R.id.txv_name);
        ui_txvMyprofile = (TextView)headerView.findViewById(R.id.txv_myprofile);
        ui_imvPhoto = (RadiusImageView)headerView.findViewById(R.id.imv_photo);
        txv_listed_num = (TextView)headerView.findViewById(R.id.txv_listed_num);
        txv_liked_num = (TextView)headerView.findViewById(R.id.txv_liked_num);
        txv_bought_num = (TextView)headerView.findViewById(R.id.txv_bought_num);
        */
        ////////////////////////////////////////////////////////////

        ui_lytSend_us = (LinearLayout)findViewById(R.id.lyt_send_us);
        ui_lytSend_us.setOnClickListener(this);

        ui_lytTerms = (LinearLayout)findViewById(R.id.lyt_terms_and);
        ui_lytTerms.setOnClickListener(this);

        lyt_faqs = (LinearLayout)findViewById(R.id.lyt_faqs);
        lyt_faqs.setOnClickListener(this);

        /*ui_lyt_home = (LinearLayout)headerView.findViewById(R.id.lyt_home);
        ui_lyt_home.setOnClickListener(this);


        ui_lyt_post = (LinearLayout)headerView.findViewById(R.id.lyt_post);
        ui_lyt_post.setOnClickListener(this);

        ui_lyt_mynetwork = (LinearLayout)headerView.findViewById(R.id.lyt_Mynetwork);
        ui_lyt_mynetwork.setOnClickListener(this);

        ui_lyt_help = (LinearLayout)headerView.findViewById(R.id.lyt_help);
        ui_lyt_help.setOnClickListener(this);

        ui_lyt_invite = (LinearLayout)headerView.findViewById(R.id.lyt_invite);
        ui_lyt_invite.setOnClickListener(this);

        ui_lyt_setting = (LinearLayout)headerView.findViewById(R.id.lyt_setting);
        ui_lyt_setting.setOnClickListener(this);

        ui_lyt_myprofile = (LinearLayout)headerView.findViewById(R.id.lyt_myprofile);
        ui_lyt_myprofile.setOnClickListener(this);

        ui_lyt_listed = (RelativeLayout) headerView.findViewById(R.id.lyt_listed);
        ui_lyt_listed.setOnClickListener(this);

        ui_lyt_liked = (RelativeLayout)headerView.findViewById(R.id.lyt_liked);
        ui_lyt_liked.setOnClickListener(this);

        ui_lyt_bought = (RelativeLayout)headerView.findViewById(R.id.lyt_bought);
        ui_lyt_bought.setOnClickListener(this);*/

        //updateProfile();

    }

    private void updateProfile(){

        ui_txvName.setText(Commons.g_user.get_name());
        Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_menu_avatar).into(ui_imvPhoto);

        txv_listed_num.setText(String.valueOf(Commons.PRODUCT_LIST_COUNT));
        txv_liked_num.setText(String.valueOf(Commons.PRODUCT_LIKE_COUNT));
        txv_bought_num.setText(String.valueOf(Commons.PRODUCT_BOUGHT_COUNT));
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.imv_call_drawer:
                //showDrawer();
                //gotoHomeActivity();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.lyt_send_us:
                //gotoSendUsAnEmilActivity();
                break;

            case R.id.lyt_terms_and:
                //gotoTermsActivity("Terms");
                break;

            case R.id.lyt_faqs:
                gotoFAQsActivity();
                break;

            case R.id.lyt_home:

                gotoHomeActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_post:

                gotoPostActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_bundle:
                gotoMyNetworkActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;


            case R.id.lyt_invite:
                gotoInviteActivity();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_setting:
                gotoSettingsActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_myprofile:
                gotoMyProfileActivity(R.id.lyt_myprofile);
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_liked:
                gotoMyProfileActivity(R.id.lyt_liked);
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_listed:
                gotoMyProfileActivity(R.id.lyt_listed);
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_bought:
                gotoMyProfileActivity(R.id.lyt_bought);
                ui_drawerlayout.closeDrawers();
                finish();
                break;
        }

    }

    private void gotoFAQsActivity(){

        startActivity(new Intent(HelpActivity.this, FAQsActivity.class));
    }


    private void gotoTermsActivity(String title) {
        Intent intent = new Intent(this , TermsAndConditionActivity.class);
        intent.putExtra("Title", title);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in  , R.anim.left_out);
    }

    private void gotoSendUsAnEmilActivity() {

        Intent intent = new Intent(this , SendUsAnEmailActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void gotoHomeActivity() {

        finish();
    }

    private void gotoPostActivity() {

        Intent intent =  new Intent( this , PostActivity.class);
        startActivity(intent);

    }

    private void gotoMyNetworkActivity() {

        Intent intent = new Intent(this , MyBundleActivity.class);
        startActivity(intent);
    }


    private void gotoInviteActivity() {

        Share(" " ,""); ;
    }

    private void gotoSettingsActivity() {

        Intent intent = new Intent(this , SettingActivity.class) ;
        startActivity(intent);

    }


    private void gotoMyProfileActivity(int view_id) {

        switch (view_id){

            case R.id.lyt_myprofile:
                Intent intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent);
                break;

            case R.id.lyt_listed:
                Intent intent1 = new Intent(this, UserProfileActivity.class);
                intent1.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent1.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent1);
                break;

            case R.id.lyt_liked:
                Intent intent2 = new Intent(this, UserProfileActivity.class);
                intent2.putExtra(Constants.MYPROFILE_POSITION, 1);
                intent2.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent2);
                break;

            case R.id.lyt_bought:
                Intent intent3 = new Intent(this, UserProfileActivity.class);
                intent3.putExtra(Constants.MYPROFILE_POSITION, 2);
                intent3.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent3);
                break;
        }

    }

    @Override
    public void onBackPressed() {

       /* if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else */

        finish();
    }
}
