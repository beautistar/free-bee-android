package com.freebee.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.adapter.GridViewChildAdapter;
import com.freebee.android.main.QuestionsActivity;

import java.util.ArrayList;


public class Question3Fragment extends Fragment implements View.OnClickListener{

    QuestionsActivity activity;
    View view;

    TextView txv_yes, txv_no;
    TextView txv_child_1, txv_child_2, txv_child_3, txv_child_4,
             txv_child_5, txv_child_6;

    LinearLayout lyt_many_children;

    GridViewChildAdapter _adapter;
    GridView gdv_child;

    ArrayList<TextView> arrayTextView = new ArrayList<>();


    public Question3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question3, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        lyt_many_children = (LinearLayout) view.findViewById(R.id.lyt_many_children);

        txv_yes = (TextView)view.findViewById(R.id.txv_yes);
        txv_yes.setOnClickListener(this);

        txv_no = (TextView)view.findViewById(R.id.txv_no);
        txv_no.setOnClickListener(this);

        txv_child_1 = (TextView)view.findViewById(R.id.txv_child_1);
        txv_child_1.setOnClickListener(this);

        arrayTextView.add(txv_child_1);

        txv_child_2 = (TextView)view.findViewById(R.id.txv_child_2);
        txv_child_2.setOnClickListener(this);
        arrayTextView.add(txv_child_2);

        txv_child_3 = (TextView)view.findViewById(R.id.txv_child_3);
        txv_child_3.setOnClickListener(this);
        arrayTextView.add(txv_child_3);

        txv_child_4 = (TextView)view.findViewById(R.id.txv_child_4);
        txv_child_4.setOnClickListener(this);
        arrayTextView.add(txv_child_4);

        txv_child_5 = (TextView)view.findViewById(R.id.txv_child_5);
        txv_child_5.setOnClickListener(this);
        arrayTextView.add(txv_child_5);

        txv_child_6 = (TextView)view.findViewById(R.id.txv_child_6);
        txv_child_6.setOnClickListener(this);
        arrayTextView.add(txv_child_6);

        /*for (int i = 0; i<6; i++){

            arrayTextView.get(i).setBackgroundResource(R.drawable.circle_white);
            arrayTextView.get(i).setTextColor(getResources().getColor(R.color.black));
        }
*/
        gdv_child = (GridView) view.findViewById(R.id.gdv_child);
        lyt_many_children.setVisibility(View.GONE);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_yes:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));
                lyt_many_children.setVisibility(View.VISIBLE);

                break;

            case R.id.txv_no:

                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));
                lyt_many_children.setVisibility(View.GONE);
                gdv_child.setVisibility(View.GONE);

                break;

            case R.id.txv_child_1:
                getChildNum(1);
                break;

            case R.id.txv_child_2:
                getChildNum(2);
                break;

            case R.id.txv_child_3:
                getChildNum(3);
                break;

            case R.id.txv_child_4:
                getChildNum(4);
                break;

            case R.id.txv_child_5:
                getChildNum(5);
                break;

            case R.id.txv_child_6:
                getChildNum(6);
                break;

        }

    }

    private void setBackground(int a, int b, int e, int f){

        txv_yes.setBackgroundResource(a);
        txv_no.setBackgroundResource(b);

        txv_yes.setTextColor(e);
        txv_no.setTextColor(f);

    }

    private void getChildNum(int num){

        arrayTextView.get(num-1).setBackgroundResource(R.drawable.circle_small);
        arrayTextView.get(num-1).setTextColor(getResources().getColor(R.color.white));

        for (int i = 0; i<6; i++){

            if (i == (num - 1)) continue;
            arrayTextView.get(i).setBackgroundResource(R.drawable.circle_white_small);
            arrayTextView.get(i).setTextColor(getResources().getColor(R.color.black));
        }

        gdv_child.setVisibility(View.VISIBLE);
        _adapter = new GridViewChildAdapter(activity, num);
        gdv_child.setAdapter(_adapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity)context;

    }

}
