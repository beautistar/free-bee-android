package com.freebee.android.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.freebee.android.R;
import com.freebee.android.fragment.FindFriendsFragment;
import com.freebee.android.fragment.FollowersFragment;
import com.freebee.android.fragment.FollowingFragment;

/**
 * Created by GoldRain on 10/12/2016.
 */

public class ViewPagerAdapter_MyNetworkAdapter extends FragmentStatePagerAdapter {

    Context _context;

    public ViewPagerAdapter_MyNetworkAdapter(FragmentManager fm, Context context) {
        super(fm);
        this._context = context;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null ;
        switch (position){
            case 0:

                fragment = new FollowingFragment(_context);
                break;
            case 1:
                fragment = new FollowersFragment(_context);
                break;
            case 2:
                fragment = new FindFriendsFragment(_context);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){

            case 0:
                title = _context.getString(R.string.following_);
                break;
            case 1:
                title = _context.getString(R.string.followers_);
                break;
            case 2:
                title = _context.getString(R.string.find_friends);
                break;
        }

        return title ;
    }
}
