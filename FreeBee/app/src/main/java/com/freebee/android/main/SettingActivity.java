package com.freebee.android.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.freebee.android.R;
import com.bumptech.glide.Glide;

import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;
import com.freebee.android.utils.RadiusImageView;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

public class SettingActivity extends CommonActivity implements View.OnClickListener {

    DrawerLayout ui_drawerlayout;
    NavigationView ui_drawer_menu;
    ImageView ui_imv_call_draw;
    //LinearLayout ui_lyt_home, ui_lyt_post,ui_lyt_mynetwork, ui_lyt_setting, ui_lyt_invite, ui_lyt_help;
    LinearLayout ui_lyt_myprofile;
    RelativeLayout ui_lyt_listed, ui_lyt_liked , ui_lyt_bought;
    LinearLayout lyt_rate_now;
    View headerView;

    TextView ui_txvName, ui_txvMyprofile ,txv_listed_num,txv_liked_num,txv_bought_num;
    RadiusImageView ui_imvPhoto;

    TextView txv_en, txv_ht;

    SimpleRatingBar srb_review;
    EditText edt_review;
    TextView txv_submit, txv_cancel;

    LinearLayout lyt_en, lyt_ht;
    RadioButton rdb_en, rdb_ht;

    String _cur_status = "english";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        _cur_status = Preference.getInstance().getValue(this, PrefConst.PREFKEY_CURRNETLANG, "");

        Log.d("===language===>", _cur_status);

        loadLayout();

        setupNavigationBar();
    }

    private void loadLayout() {

        lyt_rate_now = (LinearLayout)findViewById(R.id.lyt_rate_now);
        lyt_rate_now.setOnClickListener(this);

        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(this);

       /* ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        headerView = ui_drawer_menu.getHeaderView(0);

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);
        ui_drawerlayout.setOnClickListener(this);*/

        //navigation header my profile part

       /* ui_txvName = (TextView)headerView.findViewById(R.id.txv_name);
        ui_txvMyprofile = (TextView)headerView.findViewById(R.id.txv_myprofile);
        ui_imvPhoto = (RadiusImageView)headerView.findViewById(R.id.imv_photo);
        txv_listed_num = (TextView)headerView.findViewById(R.id.txv_listed_num);
        txv_liked_num = (TextView)headerView.findViewById(R.id.txv_liked_num);
        txv_bought_num = (TextView)headerView.findViewById(R.id.txv_bought_num);*/
        ////////////////////////////////////////////////////////////

        /*ui_lyt_home = (LinearLayout)headerView.findViewById(R.id.lyt_home);
        ui_lyt_home.setOnClickListener(this);


        ui_lyt_post = (LinearLayout)headerView.findViewById(R.id.lyt_post);
        ui_lyt_post.setOnClickListener(this);

        ui_lyt_mynetwork = (LinearLayout)headerView.findViewById(R.id.lyt_Mynetwork);
        ui_lyt_mynetwork.setOnClickListener(this);

        ui_lyt_help = (LinearLayout)headerView.findViewById(R.id.lyt_help);
        ui_lyt_help.setOnClickListener(this);

        ui_lyt_invite = (LinearLayout)headerView.findViewById(R.id.lyt_invite);
        ui_lyt_invite.setOnClickListener(this);

        ui_lyt_setting = (LinearLayout)headerView.findViewById(R.id.lyt_setting);
        ui_lyt_setting.setOnClickListener(this);

        ui_lyt_myprofile = (LinearLayout)headerView.findViewById(R.id.lyt_myprofile);
        ui_lyt_myprofile.setOnClickListener(this);

        ui_lyt_listed = (RelativeLayout)headerView.findViewById(R.id.lyt_listed);
        ui_lyt_listed.setOnClickListener(this);

        ui_lyt_liked = (RelativeLayout)headerView.findViewById(R.id.lyt_liked);
        ui_lyt_liked.setOnClickListener(this);

        ui_lyt_bought = (RelativeLayout)headerView.findViewById(R.id.lyt_bought);
        ui_lyt_bought.setOnClickListener(this);*/


        //updateProfile();


        lyt_en = (LinearLayout)findViewById(R.id.lyt_en);
        lyt_en.setOnClickListener(this);
        rdb_en = (RadioButton)findViewById(R.id.rdb_en);
        rdb_en.setOnClickListener(this);

        lyt_ht = (LinearLayout)findViewById(R.id.lyt_ht);
        lyt_ht.setOnClickListener(this);
        rdb_ht = (RadioButton)findViewById(R.id.rdb_ht);
        rdb_ht.setOnClickListener(this);

        txv_en = (TextView)findViewById(R.id.txv_en);
        txv_ht = (TextView)findViewById(R.id.txv_ht);

        if (_cur_status.equals("english")){

            rdb_en.setChecked(true);
            rdb_ht.setChecked(false);

            txv_en.setTextColor(getResources().getColor(R.color.theme_back_color));
            txv_ht.setTextColor(getResources().getColor(R.color.black));

        } else if (_cur_status.equals("ht")){

            rdb_ht.setChecked(true);
            rdb_en.setChecked(false);

            txv_en.setTextColor(getResources().getColor(R.color.black));
            txv_ht.setTextColor(getResources().getColor(R.color.theme_back_color));
        }

    }

    private void updateProfile(){

        ui_txvName.setText(Commons.g_user.get_name());
        Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_menu_avatar).into(ui_imvPhoto);

        txv_listed_num.setText(String.valueOf(Commons.PRODUCT_LIST_COUNT));
        txv_liked_num.setText(String.valueOf(Commons.PRODUCT_LIKE_COUNT));
        txv_bought_num.setText(String.valueOf(Commons.PRODUCT_BOUGHT_COUNT));
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.imv_call_drawer:

                //showDrawer();

                gotoHomeActivity();

                break;

            case R.id.lyt_rate_now:

                wite_review();

                break;

            case R.id.lyt_home:

                //ui_drawerlayout.closeDrawers();
                gotoHomeActivity();

                break;

            case R.id.lyt_post:

                /*ui_drawerlayout.closeDrawers();
                gotoPostActivity();*/
                break;

            case R.id.lyt_help:
                /*ui_drawerlayout.closeDrawers();
                gotoHelpActivity();*/
                break;

            case R.id.lyt_bundle:
               /* ui_drawerlayout.closeDrawers();
                gotoMynetworkActivity();*/
                break;

            case R.id.lyt_invite:
                /*ui_drawerlayout.closeDrawers();
                gotoInviteActivity();*/

                break;

            case R.id.lyt_myprofile:
                /*ui_drawerlayout.closeDrawers();
                gotoMyProfileActivity(R.id.lyt_myprofile);*/
                break;

            case R.id.lyt_liked:
                /*ui_drawerlayout.closeDrawers();
                gotoMyProfileActivity(R.id.lyt_liked);*/
                break;

            case R.id.lyt_listed:
                /*ui_drawerlayout.closeDrawers();
                gotoMyProfileActivity(R.id.lyt_listed);*/
                break;

            case R.id.lyt_bought:
                /*ui_drawerlayout.closeDrawers();
                gotoMyProfileActivity(R.id.lyt_bought);*/
                break;

            case R.id.rdb_en:

                ui_drawerlayout.closeDrawers();
                if (rdb_en.isChecked()){

                    Preference.getInstance().put(this, PrefConst.PREFKEY_CURRNETLANG, "english");

                    rdb_ht.setChecked(false);

                    txv_en.setTextColor(getResources().getColor(R.color.theme_back_color));
                    txv_ht.setTextColor(getResources().getColor(R.color.black));
                }
                setLanguage("en");

               gotoSettingsActivity();

                break;

            case R.id.rdb_ht:

                ui_drawerlayout.closeDrawers();
                if (rdb_ht.isChecked()){

                    rdb_en.setChecked(false);

                    txv_en.setTextColor(getResources().getColor(R.color.black));
                    txv_ht.setTextColor(getResources().getColor(R.color.theme_back_color));

                    Preference.getInstance().put(this, PrefConst.PREFKEY_CURRNETLANG, "ht");
                }

                setLanguage("ht");
                 gotoSettingsActivity();

        }

    }

    private void gotoHomeActivity() {

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void gotoPostActivity() {

        Intent intent =  new Intent( this , PostActivity.class);
        startActivity(intent);
        finish();

    }


    private void gotoMynetworkActivity() {

        Intent intent = new Intent(this , MyBundleActivity.class);
        startActivity(intent);
        finish();
    }



    private void gotoInviteActivity() {

        Share("","");
    }


    private void gotoSettingsActivity() {

        Intent intent = new Intent(this , SettingActivity.class) ;
        overridePendingTransition(0,0);
        startActivity(intent);
        finish();

    }

    private void gotoHelpActivity() {

        Intent intent = new Intent(this , HelpActivity.class);
        startActivity(intent);
        finish();
    }
    private void gotoMyProfileActivity(int view_id) {

        switch (view_id){

            case R.id.lyt_myprofile:
                Intent intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent);
                finish();
                break;

            case R.id.lyt_listed:
                Intent intent1 = new Intent(this, UserProfileActivity.class);
                intent1.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent1.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent1);
                finish();
                break;

            case R.id.lyt_liked:
                Intent intent2 = new Intent(this, UserProfileActivity.class);
                intent2.putExtra(Constants.MYPROFILE_POSITION, 1);
                intent2.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent2);
                finish();
                break;

            case R.id.lyt_bought:
                Intent intent3 = new Intent(this, UserProfileActivity.class);
                intent3.putExtra(Constants.MYPROFILE_POSITION, 2);
                intent3.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent3);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {

        /*if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else */
        gotoHomeActivity();
    }

    private void wite_review(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_write_review);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        edt_review = (EditText) dialog.findViewById(R.id.edt_review);

        txv_cancel = (TextView)dialog.findViewById(R.id.txv_cancel);
        txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txv_submit = (TextView)dialog.findViewById(R.id.txv_submit);
        txv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showToast("submited");
                //submit();
                //dialog.dismiss();
            }
        });

        srb_review = (SimpleRatingBar)dialog.findViewById(R.id.srb_review);
        srb_review.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {

                double cur_rating = rating;

                showToast(String.valueOf(cur_rating));
            }
        });

        dialog.show();
    }
}
