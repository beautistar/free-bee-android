package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.freebee.android.R;
import com.bumptech.glide.Glide;

import com.freebee.android.main.MyBundleActivity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/4/2017.
 */

public class ListViewFollowerAdapter extends BaseAdapter{

    ArrayList<UserEntity> _followers = new ArrayList<>();
    ArrayList<UserEntity> _allFollower = new ArrayList<>();
    MyBundleActivity _activity;

    public ListViewFollowerAdapter(MyBundleActivity activity){

        this._activity = activity;
    }

    public void setData(ArrayList<UserEntity> followers){

        _allFollower = followers;
        _followers.clear();
        _followers.addAll(_allFollower);

        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return _allFollower.size();
    }

    @Override
    public Object getItem(int position) {
        return _allFollower.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FollowerHolder followerHolder ;

        if (convertView == null){

            followerHolder = new FollowerHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_follower_listview, parent, false);

            followerHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name);
            followerHolder.ui_imvPhoto = (RadiusImageView)convertView.findViewById(R.id.imv_photo);
            followerHolder.imv_state = (ImageView)convertView.findViewById(R.id.imv_state);

            convertView.setTag(followerHolder);

        } else {
            followerHolder = (FollowerHolder)convertView.getTag();
        }

        final UserEntity follower = (UserEntity)_followers.get(position);

        followerHolder.ui_txvName.setText(follower.get_name());
        Glide.with(_activity).load(follower.get_imageUrl()).placeholder(R.drawable.user_no_img).into(followerHolder.ui_imvPhoto);

        if (follower.get_is_followed().equals("Y")){

            followerHolder.imv_state.setSelected(true);

        } else followerHolder.imv_state.setSelected(false);



       /* followerHolder.imv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (followerHolder.imv_state.isSelected() == true){

                    followerHolder.imv_state.setImageResource(R.drawable.ic_add_following);
                    followerHolder.imv_state.setSelected(false);
                    _activity.showAlertDialog("Do you want to unfollow ?");

                }else {
                    followerHolder.imv_state.setImageResource(R.drawable.ic_following_list);
                }
            }
        });   */

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.gotoUserProfile(follower);

            }
        });


        return convertView;
    }

    private class FollowerHolder {

        RadiusImageView ui_imvPhoto ;
        TextView ui_txvName ;
        ImageView imv_state;
    }

}
