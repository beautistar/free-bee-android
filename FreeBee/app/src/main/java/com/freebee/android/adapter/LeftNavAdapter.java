package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.freebee.android.R;


/**
 * Created by GoldRain on 9/30/2016.
 */

public class LeftNavAdapter extends BaseAdapter {

    /** The items.
     *
     */
    private String _items[];

    private Context _context;

    /**
     * The selected.
     */
    private  int selected;

    /** The icons. **/
    private int icons [] = {R.drawable.ic_nav_near_me , R.drawable.ic_nav_sell,R.drawable.ic_nav_help,
                                R.drawable.ic_nav_invite, R.drawable.ic_settings_ };

    /**
     * Setup the current selected position of adpater.
     *
     *  postion the new selection.
     */

    public void setSelection (int position){

        selected =  position ;
        notifyDataSetChanged();
    }

    /**
     * Instantiantes a new left navigation adapter.
     *
     * @param context
     *      the context of _activity
     * @param items
     *       the array of items to be displayed on ListView
     */

    public LeftNavAdapter(Context context, String items[]){

        this._context = context;
        this._items = items ;
    }

    /**(non_Javadoc)
     * @see Adapter#getCount()
     *
     */
    @Override
    public int getCount() {
        return _items.length;
    }

    @Override
    public Object getItem(int arg0) {
        return _items[arg0];
    }

    @Override
    public long getItemId(int postion) {
        return postion;
    }

    @Override
    public View getView(int postion, View convertView, ViewGroup parent) {

        if (convertView == null)

            convertView = LayoutInflater.from(_context).inflate(R.layout.item_draw_header, null);
        TextView lbl = (TextView) convertView;

        lbl.setText((Integer) getItem(postion));
        lbl.setCompoundDrawablesWithIntrinsicBounds(icons[postion],0,0,0);

        return lbl;
    }
}
