package com.freebee.android.main;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.common.Common;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.freebee.android.adapter.CitiesSelectListAdapter;
import com.freebee.android.adapter.RecyclerViewAdapterPost;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.CityModel;
import com.freebee.android.model.InterestsEntity;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.StateModel;
import com.freebee.android.utils.BitmapUtils;
import com.freebee.android.utils.CustomMultipartRequest;
import com.freebee.android.utils.RadiusImageView;
//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.nex3z.flowlayout.FlowLayout;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.freebee.android.commons.ReqConst.CODE_SUCCESS;
import static com.freebee.android.commons.ReqConst.RES_CODE;

public class PostActivity extends BaseActivity implements View.OnClickListener , Serializable, OnMapReadyCallback {

    public static final int MAX_IMAGES = 8 ;

    //NavigationView ui_drawer_menu;
    DrawerLayout ui_drawerlayout;
    View headerView;
    TextView ui_txvPost, ui_txvCategories, txv_type, txv_gender,txv_age,  txv_hobby, txv_occupation, txv_industry, txv_country, txv_state, txv_city;
    String age_min = "0", age_max = "100", gender = "", type = "", hobby = "", industry = "", occupation = "", category_index = "", state = "", city = "";

    //TextView ui_txvName, ui_txvMyprofile ,txv_listed_num,txv_liked_num,txv_bought_num;
    RadiusImageView ui_imvPhoto;

    FrameLayout ui_fytSellPhoto ;
    ImageView ui_imv_call_draw ,ui_imvMap ,ui_imvScope, imv_sell_stuff ;
    //LinearLayout ui_lyt_home, ui_lyt_post, ui_lyt_mynetwork, ui_lyt_setting, ui_lyt_invite, ui_lyt_help ,ui_lytMap;
    //LinearLayout ui_lyt_myprofile;

    RelativeLayout ui_lyt_listed, ui_lyt_liked , ui_lyt_bought;

    EditText edt_selling/*, edt_price*/,  edt_description, edt_quantity_available, edt_quantity_per_user, edt_zipCode;
    RecyclerView ui_recyclerImage ;
    RecyclerViewAdapterPost _imageAdapter = null;
    ArrayList<String> _imagePaths = new ArrayList<>();
    ArrayList<String> _editImagePaths = new ArrayList<>();

    //Resources res = getResources();

    public String[] AGE_GATEGORIES = null;
    public String[] INDUSTRY_CATEGORIES = null;
    public String[] OCCUPATION_CATEGORIES = null;
    public String[] GENDER_CATEGORIES = null;
    int[] strIds = {R.string.select_categories, R.string.home } ;
    int[] _selecttIdx = {-1 ,-1};

    int category_id = 0;

    Uri _imageCaptureUri;
    String _photoPath = "";
    ImageView ui_imvRegPhoto;

    /*private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient ;
    private Location mCurrentLocation;*/
    Double X = 0.0 , Y = 0.0 ;
   /* Double _latitude = 0.00, _longitude = 0.00 ;
    private static final boolean SHOULD_ANIMATE = false;*/


    public LocationManager myLocationManager;
    public boolean w_bGpsEnabled, w_bNetworkEnabled;

    public static double w_fLatitude = 0.0;
    public static double w_fLongitude = 0.0;
    public static double w_fspeed = 0.0;

    GoogleMap mMap ;

    ArrayList<InterestsEntity> userInterests = new ArrayList<>();
    ArrayList<String> stateSelected = new ArrayList<>();

    ///////////////

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS, Manifest.permission.CAMERA};

    CountryPicker picker;

    HashMap<String, ArrayList<String>> stateList = new HashMap<String,ArrayList<String>>();

    ArrayList<StateModel> STATES = new ArrayList<>();
    ArrayList<CityModel> CITIES = new ArrayList<>();

    ProductEntity _product = new ProductEntity();
    boolean fromEdit = false;

    HashMap<String, ArrayList<String>> industryList = new HashMap<String,ArrayList<String>>();
    ArrayList<String> industryArray = new ArrayList<>();

    float amount = 0.f;
    String transactionID = "";
    String balance = "";

    /**************************************************** starting********************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        getCity();
        getOccupation();

        AGE_GATEGORIES =  getResources().getStringArray(R.array.Age);
        INDUSTRY_CATEGORIES =  getResources().getStringArray(R.array.Industries);
        OCCUPATION_CATEGORIES =  getResources().getStringArray(R.array.Occupation);
        GENDER_CATEGORIES =  getResources().getStringArray(R.array.Gender);

        if (mMap == null) {

            SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }

        Commons.g_postActivity = this;

        fromEdit = getIntent().getBooleanExtra(Constants.PRODUCT_EDIT, false);
        getBalance();
        loadLayout();

        TextView txv_post_title = (TextView)findViewById(R.id.txv_post_title);
        if (fromEdit){
            getProduct();
            txv_post_title.setText("Post Edit");
            ui_txvPost.setText("Update");

        }else {
            txv_post_title.setText("Post");
            ui_txvPost.setText("Post");
        }


        //setupNavigationBar();
        /*X = getIntent().getDoubleExtra(Constants.CURRENT_MYLATITUDE_X,0.00);
        Y = getIntent().getDoubleExtra(Constants.CURRENT_MYLONGITUDE_Y, 0.00);*/


        checkAllPermission();

        for (int i = 0; i < Constants.strInterests.size(); i++){

            InterestsEntity interestsEntity = new InterestsEntity();

            interestsEntity.set_id(i);
            interestsEntity.set_item(Constants.strInterests.get(i));
            interestsEntity.set_status(false);

            userInterests.add(interestsEntity);
        }
    }


    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }


    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }


    private void loadLayout() {

        //Log.d("category test : ", Constants.CATEGORIES[5]);

        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(this);

        ui_imvScope = (ImageView)findViewById(R.id.imv_scope);
        ui_imvScope.setOnClickListener(this);

        imv_sell_stuff = (ImageView)findViewById(R.id.imv_sell_stuff);
        imv_sell_stuff.setOnClickListener(this);

        ui_txvPost = (TextView)findViewById(R.id.txv_post);
        ui_txvPost.setOnClickListener(this);

        txv_type = (TextView)findViewById(R.id.txv_type);
        txv_type.setOnClickListener(this);

        ui_txvCategories = (TextView)findViewById(R.id.txv_categories);
        ui_txvCategories.setOnClickListener(this);

        ui_fytSellPhoto = (FrameLayout) findViewById(R.id.fyt_sell_photo);    /**Take photo button **/
        ui_fytSellPhoto.setOnClickListener(this);

        ui_imvRegPhoto = (ImageView)findViewById(R.id.imv_image);

        txv_gender = (TextView) findViewById(R.id.txv_gender);
        txv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseGender(0);
            }
        });

        txv_age = (TextView)findViewById(R.id.txv_age);
        txv_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseAge(0);
            }
        });

        txv_industry = (TextView) findViewById(R.id.txv_industry);
        txv_industry.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                chooseIndustry();
            }
        });

        txv_occupation = (TextView) findViewById(R.id.txv_occupation);
        txv_occupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (industry.length() == 0){

                    showAlertDialog("Choose industry");
                }
                else chooseOccupation();
            }
        });
        txv_hobby = (TextView) findViewById(R.id.txv_hobby);
        txv_hobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseHobbyActivity();
            }
        });

        edt_selling = (EditText)findViewById(R.id.edt_selling);
        //edt_price = (EditText)findViewById(R.id.edt_price);
        edt_description = (EditText)findViewById(R.id.edt_description);
        edt_quantity_available = (EditText)findViewById(R.id.edt_quantity);
        if (fromEdit) edt_quantity_available.setEnabled(false);
        else edt_quantity_available.setEnabled(true);
        edt_quantity_per_user = (EditText)findViewById(R.id.edt_quantity_user);

        edt_zipCode = (EditText)findViewById(R.id.edt_zipCode);

        txv_state = (TextView) findViewById(R.id.txv_state);
        txv_state.setOnClickListener(this);
        txv_city = (TextView) findViewById(R.id.txv_city);
        txv_city.setOnClickListener(this);

        txv_country = (TextView)findViewById(R.id.txv_country);
        txv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPicker(v);
            }
        });

        picker = CountryPicker.newInstance("Select Country");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String country, String s1, String s2, int i) {

                txv_country.setText(country);
                picker.dismiss();
            }
        });

        ui_recyclerImage = (RecyclerView)findViewById(R.id.rev_photo);          // RecyclerViwe Adapter :
        LinearLayoutManager layoutManager = new LinearLayoutManager(this , LinearLayoutManager.HORIZONTAL , false);
        ui_recyclerImage.setLayoutManager(layoutManager);

        _imageAdapter = new RecyclerViewAdapterPost(this);
        ui_recyclerImage.setAdapter(_imageAdapter);

        deleteAllTempImage();
        if (fromEdit) {
            _imagePaths.clear();
        }

        ImageView ui_imvScope = (ImageView)findViewById(R.id.imv_scope);
        ui_imvScope.setOnClickListener(this);

        LinearLayout lytContainer = (LinearLayout)findViewById(R.id.lyt_container);

        lytContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_selling.getWindowToken(),0);
                return false;
            }
        });


        //updateProfile();
    }


    private void getProduct(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_PRODUCTDETAIL;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseProductDetails(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                showAlertDialog(getString(R.string.error));
                closeProgress();
            }

        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCT_ID, String.valueOf(Constants.PRODUCT.get_id()));

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseProductDetails(String respons) {


        try {

            closeProgress();

            JSONObject object = new JSONObject(respons);

            //Log.d("======respons=======", respons);

            int result_code = object.getInt(RES_CODE);

            if (result_code == CODE_SUCCESS) {

                JSONObject jsonProduct = object.getJSONObject(ReqConst.RES_PRODUCTDETAIL);

                ProductEntity product = new ProductEntity();

                product.set_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_ID));
                product.set_name(jsonProduct.getString(ReqConst.RES_PRODUCT_NAME));
                product.set_description(jsonProduct.getString(ReqConst.RES_PRODUCT_DESCRIPTION));
                //product.set_price(jsonProduct.getInt(ReqConst.RES_PRODUCTPRICE));
                product.set_category(jsonProduct.getString(ReqConst.RES_CATEGORY));
                product.set_latitude(jsonProduct.getDouble(ReqConst.RES_PRODUCTLATITUDE));
                product.set_longtitude(jsonProduct.getDouble(ReqConst.RES_PRODUCTLONGTITUDE));
                product.set_quantity_available(jsonProduct.getInt(ReqConst.RES_QUANTITY_AVAILABLE));
                product.set_quantity_per_user(jsonProduct.getInt(ReqConst.RES_QUANTITY_USER));
                product.set_age_max(jsonProduct.getInt(ReqConst.RES_AGE_MAX));
                product.set_age_min(jsonProduct.getInt(ReqConst.RES_AGE_MIN));
                product.set_gender(jsonProduct.getString(ReqConst.RES_GENDER));
                product.set_hobby(jsonProduct.getString(ReqConst.RES_HOBBY));
                product.set_industry(jsonProduct.getString(ReqConst.RES_INDUSTRY));
                product.set_occupation(jsonProduct.getString(ReqConst.RES_OCCUPATION));
                product.set_zipcode(jsonProduct.getString(ReqConst.RES_ZIPCODE));
                product.set_city(jsonProduct.getString(ReqConst.RES_CITY));
                product.set_state(jsonProduct.getString(ReqConst.RES_STATE));
                product.set_country(jsonProduct.getString(ReqConst.RES_COUNTRY));
                product.set_like_product(jsonProduct.getString(ReqConst.RES_ISLIKED));
                product.set_cart_product(jsonProduct.getString(ReqConst.RES_CART_PRODUCT));
                product.set_ownerName(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_NAME));
                product.set_owner_imageUrl(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_IMAGE));
                product.set_owner_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_OWNER_ID));
                product.set_is_sold(jsonProduct.getString(ReqConst.RES_IS_SOLD));
                product.set_verify_facebook(jsonProduct.getString(ReqConst.RES_VERIFYFACEBOOK));
                product.set_owner_phone(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_PHONE));
                product.set_verify_google(jsonProduct.getString(ReqConst.RES_VERIFYGOOGLE));
                product.set_sold_count(jsonProduct.getInt(ReqConst.RES_SOLD_COUNT));
                product.setType(jsonProduct.getString(ReqConst.REQ_TYPE));

                JSONArray productImages = jsonProduct.getJSONArray(ReqConst.RES_PRODUCT_IMAGE);

                ArrayList<String>_productImages = new ArrayList<>();

                for (int i = 0; i < productImages.length();i++){

                    String jsonImage = (String) productImages.get(i);
                    _productImages.add(jsonImage);
                }
                product.set_productImages(_productImages);

                _product = product;

                editProduct();

            } else {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void editProduct(){

        edt_selling.setText(_product.get_name());
        edt_quantity_available.setText(String.valueOf(_product.get_quantity_available()));
        edt_quantity_per_user.setText(String.valueOf(_product.get_quantity_per_user()));
        edt_description.setText(_product.get_description());

        type = _product.getType();
        txv_type.setText(type);

        age_max = String.valueOf(_product.get_age_max());
        age_min = String.valueOf(_product.get_age_min());
        txv_age.setText(age_min + " - " + age_max);

        gender = _product.get_gender();
        txv_gender.setText(gender);

        hobby = _product.get_hobby();
        txv_hobby.setText(hobby);

        industry = _product.get_industry();
        txv_industry.setText(industry);

        occupation = _product.get_occupation();
        txv_occupation.setText(occupation);

        if (occupation.contains(";")){

            String[] sepOcc = industry.split(";");
            for (int i = 0; i < sepOcc.length; i++){
                industryArray.add(sepOcc[i]);
            }
        }
        else industryArray.add(industry);

        category_index = _product.get_category();
        ui_txvCategories.setText(category_index);

        edt_zipCode.setText(_product.get_zipcode());

        city = _product.get_city();
        txv_city.setText(city);

        state = _product.get_state();
        txv_state.setText(state);

       if (state.contains(";")){

           String[] separated = state.split(";");
           for (int i = 0; i < separated.length; i++){
               stateSelected.add(separated[i]);
           }
       }else stateSelected.add(state);

        setImages(_product.get_productImages());
    }

    public void openPicker(View view){
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    private void updateProfile(){

        /*ui_txvName.setText(Commons.g_user.get_name());
        Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_menu_avatar).into(ui_imvPhoto);

        txv_listed_num.setText(String.valueOf(Commons.PRODUCT_LIST_COUNT));
        txv_liked_num.setText(String.valueOf(Commons.PRODUCT_LIKE_COUNT));
        txv_bought_num.setText(String.valueOf(Commons.PRODUCT_BOUGHT_COUNT));*/
    }

    public void addImage (String path) {

        _imagePaths.add(path);
        _imageAdapter.setDatas(_imagePaths);
        _imageAdapter.notifyDataSetChanged();

        if (_imagePaths.size() > 0){
            ui_recyclerImage.setVisibility(View.VISIBLE);

        } else {
            ui_recyclerImage.setVisibility(View.GONE);
        }

        Log.d("========>" , String.valueOf(_imagePaths));

    }

    public void setImages(ArrayList<String> paths) {

        _editImagePaths.addAll(paths);
        _imageAdapter.setDatas(_editImagePaths);

        if (_editImagePaths.size() > 0){
            ui_recyclerImage.setVisibility(View.VISIBLE);

        } else {
            ui_recyclerImage.setVisibility(View.GONE);
        }
    }

    public void removeImage (String path) {

        if (fromEdit){

            _editImagePaths.remove(path) ;
            _imageAdapter.setDatas(_editImagePaths);
            _imageAdapter.notifyDataSetChanged();

        }else {

        _imagePaths.remove(path) ;
        _imageAdapter.setDatas(_imagePaths);
        _imageAdapter.notifyDataSetChanged(); }
    }

    private void onPost(){

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt_selling.getWindowToken(),0);

        Float balance = Float.parseFloat(Commons.g_user.getBalance());


        if (type.equalsIgnoreCase("premium"))

            amount = 1.f * Integer.parseInt(edt_quantity_available.getText().toString().trim());

        else if (type.equalsIgnoreCase("regular"))

            amount = 0.5f * Float.parseFloat(edt_quantity_available.getText().toString().trim());

        if (balance >= 20){

            amount = 0.f;
            postProducts();

        }
        else {

            if ((amount + balance) >= 20)
                amount = 20 - Float.parseFloat(Commons.g_user.getBalance());

            inputCard();
        }

    }

    /*================================== get balance=======================================*/
    private void getBalance(){

        String url = ReqConst.SERVER_URL + "get_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    int resultCode = json.getInt(RES_CODE);
                    if (resultCode == CODE_SUCCESS){

                        Commons.g_user.setBalance(json.getString("balance"));
                        balance = Commons.g_user.getBalance();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }
    private void inputCard(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_payment);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_amount = (TextView)dialog.findViewById(R.id.txv_amount);
        TextView txv_confirm = (TextView)dialog.findViewById(R.id.txv_confirm);
        TextView txv_cancel = (TextView)dialog.findViewById(R.id.txv_cancel);

        txv_amount.setText("$" + amount);

        CardInputWidget card_stripe = (CardInputWidget) dialog.findViewById(R.id.card_input_widget);

        txv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Card card = card_stripe.getCard();

                if (card == null) {
                    // Do not continue token creation.
                    Toast.makeText(PostActivity.this,getString(R.string.no_existCard), Toast.LENGTH_SHORT).show();

                }else {

                    Stripe stripe = new Stripe(PostActivity.this, getString(R.string.stripe_public_key));
                    stripe.createToken(
                            card,
                            new TokenCallback() {
                                public void onSuccess(Token token) {

                                    showProgress();

                                    String url = ReqConst.SERVER_URL + "charge_payment";

                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                JSONObject json = new JSONObject(response);

                                                int resultCode = json.getInt(RES_CODE);

                                                if (resultCode == CODE_SUCCESS){

                                                    transactionID = json.getString("id");

                                                    postProducts();
                                                    dialog.dismiss();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            showAlertDialog(getString(R.string.error));
                                            closeProgress();
                                        }
                                    }){
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<>();

                                            try {

                                                params.put("stripe_token", token.getId());
                                                params.put("email", Commons.g_user.get_email());
                                                params.put("amount", String.valueOf(Math.round(amount * 100)));

                                            } catch (Exception e) {

                                                closeProgress();
                                                showAlertDialog(getString(R.string.error));
                                            }
                                            return params;
                                        }
                                    };

                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
                                }

                                @Override
                                public void onError(@NonNull Exception e) {
                                    closeProgress();
                                    Toast.makeText(PostActivity.this, "Error", Toast.LENGTH_LONG).show();

                                }

                            }
                    );

                }
            }
        });

        txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void postProducts() {

        showProgress();

        String url = "";

        if (!fromEdit) {
            url = ReqConst.SERVER_URL + ReqConst.REQ_POSTAD;
        } else {
            url = ReqConst.SERVER_URL + ReqConst.REQ_EDIT_PRODUCT;
        }

        Map<String, String> mHeaderPart = new HashMap<>();
        mHeaderPart.put("Content-type", "multipart/form-data;");

        //File part
        Map<String, File> mFilePartData = new HashMap<>();

        if (_imagePaths.size() > 0) {

            for (int i = 0; i < _imagePaths.size(); i++) {

                String path = _imagePaths.get(i);
                if (path.length() > 0)
                    mFilePartData.put("product_image[" + i + "]", new File(path));
            }

        }
        //string part

        Map<String, String> params = new HashMap<>();

        try {

            if (fromEdit) params.put(ReqConst.RES_PRODUCT_ID, String.valueOf(_product.get_id()));
            else params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

            params.put(ReqConst.REQ_PRODUCTNAME, edt_selling.getText().toString().trim());
            params.put(ReqConst.REQ_PRODUCTDESCRIPTION, edt_description.getText().toString().trim());
            params.put(ReqConst.REQ_PRODUCTPRICE, String.valueOf(15));
            params.put(ReqConst.REQ_TYPE, type);

            params.put(ReqConst.REQ_CATEGORY, ui_txvCategories.getText().toString());
            params.put(ReqConst.REQ_LATITUDE, String.valueOf(w_fLatitude));
            params.put(ReqConst.REQ_LONGITUDE, String.valueOf(w_fLongitude));

            Log.d("Location", String.valueOf(w_fLatitude + ":" + w_fLongitude));

            params.put(ReqConst.REQ_QUANTITY_AVAILABLE, edt_quantity_available.getText().toString());
            params.put(ReqConst.REQ_QUANTITY_PER_USER, edt_quantity_per_user.getText().toString());
            params.put(ReqConst.REQ_AGE_MIN, age_min);
            params.put(ReqConst.REQ_AGE_MAX, age_max);
            params.put(ReqConst.REQ_GENDER, gender);
            params.put(ReqConst.REQ_HOBBY, hobby);

            if (industry.length() != 0) params.put(ReqConst.REQ_INDUSTRY, industry);
            else params.put(ReqConst.REQ_INDUSTRY, "all");
            if (occupation.length() != 0) params.put(ReqConst.REQ_OCCUPATION, occupation);
            else params.put(ReqConst.REQ_OCCUPATION, "all");

            params.put(ReqConst.REQ_ZIPCODE, edt_zipCode.getText().toString().trim());
            params.put(ReqConst.REQ_CITY, city);
            params.put(ReqConst.REQ_STATE, state);
            params.put(ReqConst.REQ_COUNTRY, "U.S."/*txv_country.getText().toString().trim()*/);
            params.put("transaction_id", transactionID);
            params.put("balance", balance);
            params.put("amount", String.valueOf(amount));

        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

        if (_imagePaths.size() > 0) {

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, this, url, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    parsePostProduct(response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    closeProgress();
                }

            }, mFilePartData, params, mHeaderPart);

            mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            FreeBeeApplication.getInstance().addToRequestQueue(mCustomRequest, url);

        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    parsePostProduct(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    showAlertDialog(getString(R.string.error));
                    closeProgress();
                }

            })  {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    params.put(ReqConst.REQ_PRODUCTIMAGE, "");
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
        }
    }

    private void parsePostProduct(String response){

        Log.d("post", response);
        closeProgress();

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(RES_CODE);

            if (result_code == CODE_SUCCESS){

                startActivity(new Intent(this, MyPostListActivity.class));
                finish();

            } else {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    public void chooseType(final int idx) {

        String[][] itemsArray = { getResources().getStringArray(R.array.Type)} ;
        final String[]  items = itemsArray[idx];

        final TextView txv = txv_type;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(strIds[idx]);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int item) {

                txv.setText(items[item]);
                type = items[item];
                /*_selecttIdx[idx] = item;
                category_id = item;*/
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void chooseIndustry() {

        ArrayList<String> industries = new ArrayList<>();

        for (String key : industryList.keySet().stream().sorted().collect(Collectors.toList())){
            industries.add(key);
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(this, industries);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    /*result.add(categoriesArray[checkedItems.keyAt(i)]);*/

                    result.add(industries.get(checkedItems.keyAt(i)));
                }

                industryArray.addAll(result);
                industry = TextUtils.join(";", result);
                txv_industry.setText(industry);

                txv_occupation.setText("");
                occupation = "";

                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }


    public void chooseOccupation() {

        ArrayList<String> occupations = new ArrayList<>();

        if (industryList.size() > 0){
            for (int i = 0; i < mergeOccupation(industryArray).size(); i++){

                occupations.add(mergeOccupation(industryArray).get(i));
            }
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(this, occupations);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                occupation = "";

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    /*result.add(categoriesArray[checkedItems.keyAt(i)]);*/

                    result.add(occupations.get(checkedItems.keyAt(i)));
                }

                occupation = TextUtils.join(";", result);
                txv_occupation.setText(occupation);

                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();

    }

    private void chooseAge(final int idx){

        ArrayList<String> ageArray = new ArrayList<>();

        String[][] itemsArray = {getResources().getStringArray(R.array.Age)} ;

        //final String[]  items = {getResources().getStringArray(R.array.Age)[0]};
        final String[]  items = itemsArray[idx];
        Log.d("items size==>", items.length + "");

        for (int k = 0; k < items.length; k ++){

            ageArray.add(items[k]);
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(this, ageArray);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    result.add(ageArray.get(checkedItems.keyAt(i)));

                    if (checkedItems.size() == 1){

                        String strAge = ageArray.get(checkedItems.keyAt(i));

                        if(strAge.equalsIgnoreCase("65+ years old")){

                            age_max = String.valueOf(100);
                            age_min = String.valueOf(65);

                        } else {
                            String[] separated = strAge.split("-");
                            age_min = separated[0];
                            age_max = separated[1].split(" ")[0];
                        }
                    }
                    else {

                        String strMin = ageArray.get(checkedItems.keyAt(0));
                        String[] separatedMin = strMin.split("-");
                        age_min = separatedMin[0];

                        String strMax = ageArray.get(checkedItems.keyAt(checkedItems.size() - 1));

                        if (strMax.equalsIgnoreCase("65+ years old")){

                            age_max = String.valueOf(100);

                        }else {
                            String[] separatedMax = strMax.split("-");
                            age_max = separatedMax[1].split(" ")[0];
                        }

                    }
                }

                txv_age.setText(TextUtils.join(";", result));
                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }


    private void chooseGender(int idx){

        String[][] itemsArray = { GENDER_CATEGORIES} ;
        final String[]  items = itemsArray[idx];

        final TextView txv = txv_gender;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(strIds[idx]);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int item) {

                txv.setText(items[item]);
                gender = items[item];

               /* _selecttIdx[idx] = item;
                category_id = item;*/

            }
        });

        AlertDialog alert = builder.create();

        alert.show();
    }

    private void chooseCategories(){

        String category = "";
        String[] categoriesArray = getResources().getStringArray(R.array.Categories);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_category);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        boolean[] selectedStatus = new boolean[11];
        for (int i = 0; i < categoriesArray.length; i++){
            selectedStatus[i] = false;
        }


        LinearLayout lyt_office =(LinearLayout)dialog.findViewById(R.id.lyt_office);
        lyt_office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_office.isSelected());
                lyt_office.setEnabled(true);
                selectedStatus[0] = lyt_office.isSelected();

            }
        });

        LinearLayout lyt_golf =(LinearLayout)dialog.findViewById(R.id.lyt_golf);
        lyt_golf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_golf.isSelected());
                lyt_golf.setEnabled(true);
                selectedStatus[1] = lyt_golf.isSelected();
            }
        });

        LinearLayout lyt_caps =(LinearLayout)dialog.findViewById(R.id.lyt_caps);
        lyt_caps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_caps.isSelected());
                lyt_caps.setEnabled(true);
                selectedStatus[2] = lyt_caps.isSelected();
            }
        });

        LinearLayout lyt_stress =(LinearLayout)dialog.findViewById(R.id.lyt_stress);
        lyt_stress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_stress.isSelected());
                lyt_stress.setEnabled(true);
                selectedStatus[3] = lyt_stress.isSelected();
            }
        });

        LinearLayout lyt_trade =(LinearLayout)dialog.findViewById(R.id.lyt_trade);
        lyt_trade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_trade.isSelected());
                lyt_trade.setEnabled(true);
                selectedStatus[4] = lyt_trade.isSelected();
            }
        });

        LinearLayout lyt_personal =(LinearLayout)dialog.findViewById(R.id.lyt_personal);
        lyt_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_personal.isSelected());
                lyt_personal.setEnabled(true);
                selectedStatus[5] = lyt_personal.isSelected();
            }
        });

        LinearLayout lyt_automotive =(LinearLayout)dialog.findViewById(R.id.lyt_automotive);
        lyt_automotive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_automotive.isSelected());
                lyt_automotive.setEnabled(true);
                selectedStatus[6] = lyt_automotive.isSelected();
            }
        });

        LinearLayout lyt_tools =(LinearLayout)dialog.findViewById(R.id.lyt_tools);
        lyt_tools.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_tools.isSelected());
                lyt_tools.setEnabled(true);
                selectedStatus[7] = lyt_tools.isSelected();
            }
        });

        LinearLayout lyt_calendars =(LinearLayout)dialog.findViewById(R.id.lyt_calendars);
        lyt_calendars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_calendars.isSelected());
                lyt_calendars.setEnabled(true);
                selectedStatus[8] = lyt_calendars.isSelected();
            }
        });

        LinearLayout lyt_home_kitchen =(LinearLayout)dialog.findViewById(R.id.lyt_home_kitchen);
        lyt_home_kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_home_kitchen.isSelected());
                lyt_home_kitchen.setEnabled(true);
                selectedStatus[9] = lyt_home_kitchen.isSelected();
            }
        });

        LinearLayout lyt_food =(LinearLayout)dialog.findViewById(R.id.lyt_food);
        lyt_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setSelected(!lyt_food.isSelected());
                lyt_food.setEnabled(true);
                selectedStatus[10] = lyt_food.isSelected();
            }
        });


        if(lyt_office.isSelected() == true){
            category = "1";}

        if(lyt_golf.isSelected() == true ){

            if(category.length()== 0 || category == null){
                category = "2";
            }else category += ",2";}

        if(lyt_caps.isSelected()==true){

            if(category.length()==0 || category ==null){
                category = "3";
            }else category += ",3";}

        if(lyt_stress.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="4";
            }else category +=",4";}

        if(lyt_trade.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="5";
            }else category +=",5";}

        if(lyt_personal.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="6";
            }else category +=",6";}

        if(lyt_automotive.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="7";
            }else category +=",7";}

        if(lyt_tools.isSelected() == true){

            if(category.length()==0 || category ==null){
                category ="8";
            }else category +=",8";}

        if(lyt_calendars.isSelected() == true){

            if(category.length()==0 || category ==null){
                category ="9";
            }else category +=",9";}

        if(lyt_home_kitchen.isSelected() == true){

            if(category.length()==0 || category ==null){
                category ="10";
            }else category +=",10";}

        if(lyt_food.isSelected() == true){

            if(category.length()==0 || category ==null){
                category ="11";
            }else category +=",11";}

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textArray = "";
                for (int i = 0; i < categoriesArray.length; i++){

                    if (selectedStatus[i]){
                        textArray += "   " + categoriesArray[i];

                        if (category_index.length() > 0 && category_index != null)
                            category_index += ";" + categoriesArray[i];
                        else category_index = String.valueOf(categoriesArray[i]);
                    }
                }


                ui_txvCategories.setText(category_index);

                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void update_userInterests(){
        for (int i = 0; i<userInterests.size(); i++){
            userInterests.get(i).set_status(false);
        }
    }

    private void chooseHobbyActivity(){

        update_userInterests();

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_interests);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        ImageView button = (ImageView) dialog.findViewById(R.id.imv_check);

        ImageView imv_cancel = (ImageView)dialog.findViewById(R.id.imv_cancel);

        //Constants.strInterests.remove(0);

        for(int i = 0 ; i<Constants.strInterests.size() ; i++){

            final TextView textView = new TextView(this);
            textView.setId(i);
            textView.setText(Constants.strInterests.get(i));
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
            textView.setTextColor(getResources().getColor(R.color.black));

            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if((userInterests.get(view.getId()).is_status())) {

                        userInterests.get(view.getId()).set_status(false);
                        textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }
                    else {

                        userInterests.get(view.getId()).set_status(true);

                        textView.setBackgroundResource(R.drawable.signup_round);
                        textView.setTextColor(getResources().getColor(R.color.white));

                        //strSelected.add(userNew.get(view.getId()).get_item());
                    }


                }
            });
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < userInterests.size(); i++){

                    if (userInterests.get(i).is_status()){
                        //interestedStr = interestedStr + "  " + userInterests.get(i).get_item() + " ";

                        if (hobby.length() > 0 && hobby != null) hobby += ";" + userInterests.get(i).get_item();
                        else hobby = userInterests.get(i).get_item();
                    }
                }

                txv_hobby.setText(hobby);

                dialog.dismiss();
            }
        });

        imv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void update_stats(){
        for (int i = 0; i<STATES.size(); i++){
            STATES.get(i).set_selected(false);
        }
    }

    private void chooseStates(){

        update_stats();

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_states);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        ImageView button = (ImageView) dialog.findViewById(R.id.imv_check);

        ImageView imv_cancel = (ImageView)dialog.findViewById(R.id.imv_cancel);

        //Constants.strInterests.remove(0);

        for(int i = 0 ; i<STATES.size() ; i++){

            final TextView textView = new TextView(this);
            textView.setId(i);
            textView.setText(STATES.get(i).getState());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
            textView.setTextColor(getResources().getColor(R.color.black));

            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if((STATES.get(view.getId()).is_selected())) {

                        STATES.get(view.getId()).set_selected(false);
                        textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }
                    else {

                        STATES.get(view.getId()).set_selected(true);

                        textView.setBackgroundResource(R.drawable.signup_round);
                        textView.setTextColor(getResources().getColor(R.color.white));

                        //strSelected.add(userNew.get(view.getId()).get_item());
                    }


                }
            });
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //String _states = "";

                //process selected items

                stateSelected = new ArrayList<>();
                state = "";
                for (int i = 0; i < STATES.size(); i++){

                    if (STATES.get(i).is_selected()){

                        stateSelected.add(STATES.get(i).getState());
                        //state += STATES.get(i).getState() + ";";

                        if (state.length() > 0 && state != null)

                            state += ";" + STATES.get(i).getState();

                        else
                            state = STATES.get(i).getState();
                    }

                }

                //state = _states;
                txv_state.setText(state);

                txv_city.setText("");
                city = "";
                //showSelectedItem(userInterests);

                dialog.dismiss();
            }
        });

        imv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseCities(){

        CITIES.clear();
        ArrayList<String> cities = new ArrayList<>();

        if (stateSelected.size() > 0){
            for (int i = 0; i < mergeCitites(stateSelected).size(); i++){

                CityModel city = new CityModel();

                city.setCity(mergeCitites(stateSelected).get(i));
                city.set_selected(false);

                CITIES.add(city);
                cities.add(mergeCitites(stateSelected).get(i));
            }
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(this, cities);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    /*result.add(categoriesArray[checkedItems.keyAt(i)]);*/

                    result.add(CITIES.get(checkedItems.keyAt(i)).getCity());
                }

                city = TextUtils.join(";", result);
                txv_city.setText(city);
                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }


    /**
     * ..............photo part
     * @param
     */

    public void onSelectPhoto(){

        if (_imagePaths.size() == MAX_IMAGES) {

            showAlertDialog("You can only select up to 8 photos.");
            return ;
        }

        final String[] items = {"Take photo","Choose from Gallary","Cancel"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0){
                    doTakePhoto();

                }else if (item == 1){
                    doTakeGallery();

                }else return;
            }

        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public void doTakePhoto(){

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Long myCurrentTimeMillis = System.currentTimeMillis();
        String filename = String.valueOf(myCurrentTimeMillis) ;

        String picturePath = BitmapUtils.getTempFolderPath()  + "photo_temp.jpg" ;
        _imageCaptureUri = Uri.fromFile(new File(picturePath));


        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK) {

                    try {

                        //File outFile = new File(_photoPath);
                        File outFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                // Free up the memory
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                returnedBitmap = bitmap;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, Constants.PROFILE_IMAGE_SIZE, Constants.PROFILE_IMAGE_SIZE, true);

                        BitmapUtils.saveOutput(outFile, w_bmpSizeLimited);

                        _photoPath = outFile.getAbsolutePath();

                        addImage(_photoPath);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {

                    _imageCaptureUri = data.getData();
                }


            case Constants.PICK_FROM_CAMERA:
            {

                if (resultCode == RESULT_OK) {
                    try {

                        _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                        //beginGrop(_imageCaptureUri ) ;


                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {

                                //We send the message here.
                                //You should also check if the suername is valid here.
                                try {

                                    selectPhoto();

                                } catch (Exception e) {

                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                addImage(_photoPath);                                       // add photo  :

                            }

                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }



    private void selectPhoto() {

        try {
            Bitmap returnedBitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);

            File outFile = getOutputMediaFile();

            BitmapUtils.saveOutput(outFile , returnedBitmap);
            returnedBitmap.recycle();
            returnedBitmap = null;

            _photoPath = outFile.getAbsolutePath();
        }    catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void deleteAllTempImage() {

        File mediaStoreDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data"
                +this.getPackageName() + "/postimage" );

        if (!mediaStoreDir.exists()) {

            if (!mediaStoreDir.mkdirs()) {
                return;
            }

        }  else {
            String[] children = mediaStoreDir.list();

            if (children == null) return;
            for (int i = 0 ; i < children.length ; i++) {
                new File(mediaStoreDir, children[i]).delete();
            }
        }
    }

    public File getOutputMediaFile() {

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/android/data/"
                        + this.getPackageName() + "/postimage");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        long random = new Date().getTime();

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "temp" + random + ".png");

        return mediaFile;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        initeLoacation();
    }

    public void initeLoacation(){

        myLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (w_bGpsEnabled || w_bNetworkEnabled) {
            tryGetLocation();

        } else {

            setMyLocation(null);
        }
    }


    private void tryGetLocation() {


        w_bGpsEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        w_bNetworkEnabled = myLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        if (w_bNetworkEnabled) {

            Location locationNet = myLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (locationNet != null) {
                setMyLocation(locationNet);
            }

        }
        if (w_bGpsEnabled) {

            Location locationGps = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGps != null) {
                setMyLocation(locationGps);
            }
        }

        if (w_bNetworkEnabled) {

            myLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000,
                    0, m_myLocationListener);

        }
        if (w_bGpsEnabled) {
            myLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    1000,
                    0, m_myLocationListener);
        }
    }

    public void setMyLocation(Location p_location) {

        if (p_location != null) {

            w_fLatitude = p_location.getLatitude();
            w_fLongitude = p_location.getLongitude();
            w_fspeed = p_location.getSpeed();



            if (X != 0.0 && Y != 0.0){

                w_fLatitude = X;
                w_fLongitude = Y;
            }

            LatLng latLng = new LatLng(w_fLatitude,w_fLongitude);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(w_fLatitude,w_fLongitude),10.0f));
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setZoomGesturesEnabled(false);
//        mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled( false );
            mMap.clear();

            mMap.addMarker(new MarkerOptions().position(latLng).title("You are here!").snippet("Consider yourself located"));


        }

        System.out.println("Latitude: " + String.valueOf(w_fLatitude) + "Longitude: " + String.valueOf(w_fLongitude)+"Speed"+ String.valueOf(w_fspeed));

    }

    LocationListener m_myLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }


        @Override
        public void onLocationChanged(Location location) {

            String log = "pos = " + location.getLatitude() + "," + location.getLongitude() + "," + location.getSpeed() + ":" + new Date().toString();
            // appendLog(log);
            //setMyLocation(location);
        }
    };

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_call_drawer:
                //showDrawer();
                gotoHomeActivity();
                break;

            case R.id.imv_sell_stuff:

               /* if (!hasPermissions(this, PERMISSIONS)){

                    ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);

                }else {*/

                    onSelectPhoto();//}

                break;

            case R.id.lyt_home:
                gotoHomeActivity();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_bundle:

                gotoMyNetworkActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;
            case R.id.lyt_invite:
                gotoInviteActivity();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_setting:
                gotoSettingsActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.lyt_help:
                gotoHelpActivity();
                ui_drawerlayout.closeDrawers();
                finish();
                break;

            case R.id.txv_type:
                chooseType(0);
                break;

            case R.id.txv_post:
                if (checkValid())
                    onPost();
                break;

            case R.id.txv_categories:
                //showChoiceDialog(0);
                chooseCategories();
                break;

            case R.id.txv_state:
                chooseStates();
                break;

            case R.id.txv_city:

                if (stateSelected.size() == 0){
                    showToast("You didn't select any state.");
                    return;
                }
                chooseCities();
                break;

            case R.id.imv_scope:

                Double x = w_fLatitude;
                Double y = w_fLongitude;

                Intent intent1 = new Intent(this , MapsActivity1.class);

                intent1.putExtra(Constants.CURRENT_MYLATITUDE_X,x);
                intent1.putExtra(Constants.CURRENT_MYLONGITUDE_Y, y);
                startActivity(intent1);

                //finish();
                break;
        }

    }

    private void gotoHomeActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();

       /* Intent intent = new Intent(this , MainActivity.class);
        startActivity(intent);*/

    }

    private void gotoMyNetworkActivity() {

        Intent intent = new Intent(this, MyBundleActivity.class);
        startActivity(intent);
    }

    private void gotoInviteActivity() {

        Share("" ,""); ;

    }

    private void gotoSettingsActivity() {

        Intent intent = new Intent(this , SettingActivity.class) ;
        startActivity(intent);

    }

    private void gotoHelpActivity() {

        Intent intent = new Intent(this , HelpActivity.class);
        startActivity(intent);
    }

    private boolean checkValid(){

        if (edt_selling.getText().length() == 0){

            showAlertDialog("Please input product name");
            return false;
        } /*else if (edt_price.getText().length() == 0){
            showAlertDialog("Please input price");
            return false;
        }*/ else if (edt_quantity_available.getText().length() == 0){
            showAlertDialog("Please input quantity available");
            return false;
        } else if (edt_quantity_per_user.getText().length() == 0){
            showAlertDialog("Please input quantity per user");
            return false;
        } else if (edt_description.getText().length() == 0){
            showAlertDialog("Please input description");
            return false;

        }else if (!fromEdit){
            if (_imagePaths.size() == 0){
                showAlertDialog("Please enter product images");
                return false;
            }

        }else if (ui_txvCategories.getText().toString().length() == 0){
            showAlertDialog("Please select category");
            return false;
        }
        else if (txv_type.getText().toString().length() == 0){

            showAlertDialog("Select type");
            return false;
        }
        /* else if (age_max.length() == 0 && age_min.length() == 0){
            showAlertDialog("Please select age");
            return false;
        } else if (gender.length() == 0){
            showAlertDialog("Please select gender");
            return false;
        } else if (hobby.length() == 0){
            showAlertDialog("Please select hobby/activity");
            return false;
        } else if (industry.length() == 0){
            showAlertDialog("Please select industry");
            return false;
        } else if (occupation.length() == 0){
            showAlertDialog("Please select occupation");
            return false;
        } else if (category_index.length() == 0){
            showAlertDialog("Please select category");
            return false;
        } else if (edt_zipCode.getText().length() == 0){
            showAlertDialog("Please input zip code");
            return false;
        } else if (edt_city.getText().length() == 0){
            showAlertDialog("Please input city");
            return false;
        } else if (edt_state.getText().length() == 0){
            showAlertDialog("Please input state");
            return false;
        } else if (txv_country.getText().length() == 0){
            showAlertDialog("Please select country");
            return false;
        } */
        return true;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
   /* public Action getIndexApiAction() {

        Thing object = new Thing.Builder()
                .setName("Post Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }
*/

    public String loadJSONCities() {
        String json = null;
        try {
            InputStream is = getApplication().getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private ArrayList<String> mergeCitites(List<String> states) {
        ArrayList<String> cities = new ArrayList<>();
        for (String state : states) {
            // TOTO:: Check the null point exception
            try {
                cities.addAll(stateList.get(state));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return cities;
    }

    private void getCity() {

        STATES.clear();

        try {
            JSONObject firstObject = new JSONObject(loadJSONCities());
            Iterator iterator = firstObject.keys();
            while ( iterator.hasNext()) {

                StateModel statesModel = new StateModel();

                String key = (String)iterator.next();
                statesModel.setState(key);

                JSONArray cityArr = firstObject.getJSONArray(key);
                ArrayList<CityModel> cityModels = new ArrayList<>();
                ArrayList cities = new ArrayList();
                for (int i = 0 ; i < cityArr.length(); i++) {

                    CityModel cityModel = new CityModel();

                    String city = cityArr.getString(i);

                    cities.add(city);

                    cityModel.setCity(city);

                    cityModels.add(cityModel);

                }

                statesModel.setCityModels(cityModels);

                STATES.add(statesModel);
                stateList.put(key, cities);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> mergeOccupation(List<String> industries) {
        ArrayList<String> occupations = new ArrayList<>();
        for (String occupation : industries) {
            // TOTO:: Check the null point exception
            try {
                occupations.addAll(industryList.get(occupation));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return occupations;
    }

    public String loadJSONOccupation() {

        String json = null;
        try {
            InputStream is = getApplication().getAssets().open("occupation.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void getOccupation(){

        try {
            JSONObject firstObject = new JSONObject(loadJSONOccupation());
            Iterator iterator = firstObject.keys();
            while ( iterator.hasNext()) {

                String key = (String)iterator.next();
                JSONArray occupationArray = firstObject.getJSONArray(key);
                ArrayList occupations = new ArrayList();

                for (int i = 0 ; i < occupationArray.length(); i++) {

                    String occupation = occupationArray.getString(i);
                    occupations.add(occupation);
                }

                industryList.put(key, occupations);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        gotoHomeActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Commons.g_postActivity = null;
    }

}
