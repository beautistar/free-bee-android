package com.freebee.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by GoldRain on 10/8/2016.
 */

public class RectNetImageView extends NetworkImageView {

    private static final int FADE_IN_TIME_MS = 250 ;

    public RectNetImageView (Context context){super((context));}

    public RectNetImageView (Context context, AttributeSet attrs){ super(context, attrs);}

    public RectNetImageView (Context context, AttributeSet attrs , int defStyle) {

        super(context, attrs, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        TransitionDrawable td = new TransitionDrawable(new Drawable[] {

                new ColorDrawable(android.R.color.transparent),
                new BitmapDrawable(getContext().getResources(), bm)
        });

        setImageDrawable(td);
        td.setCrossFadeEnabled(true);
        td.startTransition(FADE_IN_TIME_MS);
    }
}
