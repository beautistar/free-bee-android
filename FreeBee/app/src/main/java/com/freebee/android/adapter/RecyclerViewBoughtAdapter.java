package com.freebee.android.adapter;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freebee.android.R;
import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/6/2017.
 */

public class RecyclerViewBoughtAdapter extends RecyclerView.Adapter<RecyclerViewBoughtAdapter.BoughtHolder> {

    UserProfileActivity _activity;
    ArrayList<ProductEntity> _allBought = new ArrayList<>();

    public RecyclerViewBoughtAdapter(UserProfileActivity activity, ArrayList<ProductEntity> bought){

        this._activity = activity;
        _allBought = bought;
    }


    @Override
    public BoughtHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bought_gdv, parent, false);


        return new BoughtHolder(view);
    }

    @Override
    public void onBindViewHolder(BoughtHolder holder, int position) {

        final  ProductEntity bought = _allBought.get(position);

        holder.txvName.setText(bought.get_name());
        Glide.with(_activity).load(bought.get_imagUrl()).placeholder(R.drawable.place_holder_pattern).into(holder.imvImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.showToast("Bought!!!");

                Intent intent = new Intent(_activity, ProductActivity.class);
                intent.putExtra(Constants.PRODUCTID, bought.get_id());
                _activity.startActivity(intent);
                _activity.finish();

            }
        });

    }

    @Override
    public int getItemCount() {
        return _allBought.size();
    }

    public class BoughtHolder extends  RecyclerView.ViewHolder {

        public RoundedImageView imvImage;
        public TextView txvName;

        public BoughtHolder(View view) {
            super(view);

            txvName = (TextView) view.findViewById(R.id.txv_name);
            imvImage = (RoundedImageView)view.findViewById(R.id.imv_product);

        }
    }
}
