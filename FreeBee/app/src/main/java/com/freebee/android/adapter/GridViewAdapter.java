package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.freebee.android.R;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.MainActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;


import java.util.ArrayList;

/**
 * Created by GoldRain on 10/11/2016.
 */

public class GridViewAdapter extends BaseAdapter{

    MainActivity _context;

    ArrayList<ProductEntity> _product = new ArrayList<>();
    ArrayList<ProductEntity> _allProduct = new ArrayList<>();

    public GridViewAdapter (MainActivity context){

        super();
        this._context= context;

        //this._procut = product ;
    }

    public void setProduct(ArrayList<ProductEntity> products){

        _allProduct = products ;
        _product.clear();
        _product.addAll(_allProduct);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _product.size();
    }

    @Override
    public Object getItem(int position) {

        return _product.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        ContentsHolder holder;

        if (convertView == null) {

            holder = new ContentsHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView =  inflater.inflate(R.layout.item_contents_gdv , viewGroup ,false);

            holder.imvImage = (RoundedImageView)convertView.findViewById(R.id.imv_shop);
            holder.txvName = (TextView) convertView.findViewById(R.id.txv_name);

            convertView.setTag(holder);

        }else {
            holder = (ContentsHolder)convertView.getTag();
        }

        final ProductEntity productEntity = _product.get(position);

        holder.txvName.setText(productEntity.get_name());

        Glide.with(_context).load(productEntity.get_imagUrl()).placeholder(R.drawable.bg_placeholder).into(holder.imvImage);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.list_position = position ;

                int productID = productEntity.get_id();
                _context.gotoProduct(productID);

            }
        });

        return convertView;
    }

    public void searchProduct(String charText){

        charText = charText.toLowerCase();
        _product.clear();

        if (charText.length() == 0){

            _product.addAll(_allProduct);

        } else {

            for (ProductEntity product : _allProduct){

                String value = product.get_name().toLowerCase();

                if (value.contains(charText))
                    _product.add(product);
            }
        }

        notifyDataSetChanged();

    }


    public class ContentsHolder {

        public RoundedImageView imvImage;
        public TextView txvName;

    }

}
