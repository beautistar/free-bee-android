package com.freebee.android.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;

import com.freebee.android.R;
import com.freebee.android.adapter.ListViewFindViewAdapter;

import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FindFriendsActivity extends CommonActivity implements View.OnClickListener {

    ListViewFindViewAdapter _adapter;
    ListView _listview;
    ImageView ui_imvBack, ui_imvClear;
    EditText ui_edtSearch;

    UserEntity _isFolloweds = new UserEntity();
    ArrayList<UserEntity> _searchFriends = new ArrayList<>();
    String clear = "";
    String name = "";
    UserEntity _user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        loadLayout();
    }
    private void loadLayout() {

        _user = Commons.g_user;

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvClear = (ImageView)findViewById(R.id.imv_clear);
        ui_imvClear.setOnClickListener(this);

        ui_edtSearch = (EditText)findViewById(R.id.edt_search);

        _adapter = new ListViewFindViewAdapter(this);

        _listview = (ListView)findViewById(R.id.lst_friend);
        _listview.setAdapter(_adapter);

       /* ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (ui_edtSearch.getText().length() > 0){

                    ui_imvClear.setVisibility(View.VISIBLE);
                    //searchFriend();
                    return ;

                }else {
                    ui_imvClear.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

                name = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                searchFriend();

            }
        });
*/
        ui_edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                switch (actionId){

                    case EditorInfo.IME_ACTION_SEARCH:

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(ui_edtSearch.getWindowToken(),0);

                        if (ui_edtSearch.getText().length() > 0)
                            searchFriend();

                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    private void searchFriend(){

        showProgress();

        _searchFriends.clear();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCHFRIEND;
        StringRequest myRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                parseResponseSearchResult(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.REQ_SEARCHTEXT, ui_edtSearch.getText().toString().trim());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);
    }

    private void parseResponseSearchResult(String response){

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            Log.d("==followin=responds", response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray searchFriends = object.getJSONArray(ReqConst.RES_USERLIST);

                for (int i = 0; i < searchFriends.length(); i++){

                    JSONObject jsonObject = (JSONObject)searchFriends.get(i);

                    UserEntity searchFriend = new UserEntity();

                    searchFriend.set_idx(jsonObject.getInt(ReqConst.RES_USERID));
                    searchFriend.set_name(jsonObject.getString(ReqConst.RES_USERNAME));
                    searchFriend.set_imageUrl(jsonObject.getString(ReqConst.RES_USERIMAGE));
                    searchFriend.set_is_followed(jsonObject.getString("is_followed"));

                    _searchFriends.add(searchFriend);

                    if (jsonObject.getString("is_followed") == "Y") {

                        Commons.g_user.addFollower(searchFriend);
                    }
                }

                _adapter.setFriendData(_searchFriends);
                _adapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.imv_clear:
                ui_edtSearch.setText(clear);
//                ui_imvClear.setVisibility(View.VISIBLE);
                break;
        }

    }

    public void setFollow(final UserEntity entity) {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FOLLOWFRIEND;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseResponseSetFollow(response, entity);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.REQ_FRIENDID, String.valueOf(entity.get_idx()));

                } catch (Exception e) {
                    closeProgress();
                showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseResponseSetFollow(String response, UserEntity entity){

        closeProgress();

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                // if successs
                _user.addFollower(entity);

                searchFriend();
                //                _adapter.notifyDataSetChanged();

                showToast("Success Follow");

            } else {

                showAlertDialog("Failed");
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }
    }

    public void setUnFollow(final UserEntity entity) {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UNFOLLOW;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                parseResponseSetUnFollow(response, entity);
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.REQ_FRIENDID, String.valueOf(entity.get_idx()));

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseResponseSetUnFollow(String response, UserEntity entity){

        closeProgress();

        try {

            Log.d("=response=unfollow=", response);

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                // if successs
                _user.removeFollower(entity);

                searchFriend();
//                _adapter.notifyDataSetChanged();
                showToast("Success Unfollow");
            } else {

                closeProgress();
                showAlertDialog("Failed");
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();
        }
    }
}
