package com.freebee.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.freebee.android.R;
import com.freebee.android.main.SplashActivity;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;

@SuppressLint("ValidFragment")
public class FirstTutirialFragment extends Fragment implements View.OnClickListener {

    SplashActivity activity;
    View view;
    FrameLayout fly_bg;
    Button btn_next;

    int idx = 0;
    int _position = 0;


    public FirstTutirialFragment(SplashActivity context, int position) {
        // Required empty public constructor
        idx = position;
        activity = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_first_tutirial, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        fly_bg = (FrameLayout)view.findViewById(R.id.fly_bg);

        btn_next = (Button)view.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Preference.getInstance().put(activity, PrefConst.PREFKEY_FIRSTUSER, true);
                activity.userRegister();
            }
        });

        switch (idx){

            case 0:
                fly_bg.setBackgroundResource(R.drawable.splash_1);
                btn_next.setVisibility(View.GONE);
                break;

            case 1:
                fly_bg.setBackgroundResource(R.drawable.splash_2);
                btn_next.setVisibility(View.GONE);
                break;

            case 2:
                fly_bg.setBackgroundResource(R.drawable.splash_3);
                btn_next.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void onClick(View v) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (SplashActivity)context;

    }


}
