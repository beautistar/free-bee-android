package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.freebee.android.R;
import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/5/2017.
 */

public class GridViewAdapterList extends BaseAdapter{

    UserProfileActivity _activity;

    ArrayList<ProductEntity> _allProduct = new ArrayList<>();

    public GridViewAdapterList ( UserProfileActivity activity, ArrayList<ProductEntity> products ){

        _activity = activity;
        _allProduct = products;
    }
    @Override
    public int getCount() {
        return _allProduct.size();
    }

    @Override
    public Object getItem(int position) {
        return _allProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ListHolder listHolder;

        if (convertView == null) {

            listHolder = new ListHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView =  inflater.inflate(R.layout.item_likes_gdv , parent ,false);

            listHolder.imvImage = (RoundedImageView)convertView.findViewById(R.id.imv_product);
            listHolder.txvName = (TextView) convertView.findViewById(R.id.txv_name);

            convertView.setTag(listHolder);

        }else {

            listHolder = (ListHolder) convertView.getTag();
        }

        final ProductEntity productEntity = _allProduct.get(position);

        listHolder.txvName.setText(productEntity.get_name());

        Glide.with(_activity).load(productEntity.get_imagUrl()).placeholder(R.drawable.place_holder_pattern).into(listHolder.imvImage);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.list_position = position ;

                int productID = productEntity.get_id();
                //_activity.gotoProduct(productID);

            }
        });

        return convertView;
    }

    public class ListHolder {

        public RoundedImageView imvImage;
        public TextView txvName;
    }
}
