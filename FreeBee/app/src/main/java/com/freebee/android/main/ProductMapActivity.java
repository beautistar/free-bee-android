package com.freebee.android.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.freebee.android.R;

import com.freebee.android.commons.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ProductMapActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient ;
    private Location mCurrentLocation;
    public LatLng _latlng ;
    private int curMapTypeIndex = 1;

    Double _myLatitude_X = 0.000, _myLongitude_Y = 0.000;

    private final int[] MAP_TYPES = {
            GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};

    TextView ui_txvLocation;
    ImageView ui_imvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_map);

        _myLatitude_X = (Double)getIntent().getSerializableExtra(Constants.CURRENT_MYLATITUDE_X);
        _myLongitude_Y = (Double)getIntent().getSerializableExtra(Constants.CURRENT_MYLONGITUDE_Y);

        Log.d("========latitude=====>>>>=====", String.valueOf(_myLatitude_X));

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        loadLayout();
        mGoogleApiClient.connect();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_txvLocation = (TextView)findViewById(R.id.txv_userLocation);
        ui_txvLocation.setOnClickListener(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        mMap = googleMap ;

        mMap.setMapType(MAP_TYPES[curMapTypeIndex]);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(_myLatitude_X,_myLongitude_Y),10.0f));
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setTrafficEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled( true );

       /* MarkerOptions markerOptions = new MarkerOptions().position(_latlng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        currentLocation = mMap.addMarker(markerOptions);*/

        MarkerOptions options = new MarkerOptions().position(new LatLng(_myLatitude_X,_myLongitude_Y));
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(options);

        mMap.setMyLocationEnabled(true);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }

    }

    private void gotoProductActivity() {

        Intent intent = new Intent(this , ProductActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


      /*  if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mCurrentLocation.setLatitude(_myLatitude_X);
        mCurrentLocation.setLongitude(_myLongitude_Y);

       *//* mCurrentLocation = LocationServices
                .FusedLocationApi
                .getLastLocation(mGoogleApiClient);*//*

        Log.d("+++++++++++", String.valueOf(_myLatitude_X));
        Log.d("++++++++++++", String.valueOf(_myLongitude_Y));

        LatLng latLng= new LatLng(_myLatitude_X,_myLongitude_Y);
        MarkerOptions options = new MarkerOptions().position(latLng).title("Current My Loacation");
        options.icon(BitmapDescriptorFactory.defaultMarker());
        mMap.addMarker(options);

        initCamera(latLng);

        Log.d("=======currentLocation=========", String.valueOf(mCurrentLocation));*/
    }

    private void initCamera(LatLng location) {

        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.latitude,
                        location.longitude))
                .zoom(10f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);

        mMap.setMapType(MAP_TYPES[curMapTypeIndex]);
        mMap.setTrafficEnabled(true);
        mMap.setMyLocationEnabled(true);


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {                      // Add Marker
            @Override
            public void onMapClick(LatLng latlng) {

                mMap.clear();
                MarkerOptions options = new MarkerOptions().position(latlng);
                options.icon(BitmapDescriptorFactory.defaultMarker());
                mMap.addMarker(options);

                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlng, 12f);
                mMap.animateCamera(update);

//              LatLng latLng= new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
//              LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                Log.d("latlang===<<<<<<<<====", String.valueOf(latlng)) ;
                _latlng = latlng ;


                mCurrentLocation = new Location("position");
                mCurrentLocation.setLatitude(_latlng.latitude);
                mCurrentLocation.setLongitude(_latlng.longitude);

            }
        });

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.getUiSettings().setZoomControlsEnabled( true );
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}
