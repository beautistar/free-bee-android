package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.fragment.Question1Fragment;
import com.freebee.android.fragment.Question2Fragment;
import com.freebee.android.fragment.Question3Fragment;
import com.freebee.android.fragment.Question4Fragment;
import com.freebee.android.fragment.Question5Fragment;
import com.freebee.android.fragment.Question6Fragment;
import com.freebee.android.fragment.Question7Fragment;
import com.freebee.android.fragment.Question8Fragment;
import com.shuhart.stepview.StepView;

public class QuestionsActivity extends CommonActivity {

    StepView stepView;
    ImageView btn_next;
    int i = 1, _currentPage = 0;

    TextView txv_stage;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);


       /* stepView.getState()
                .nextStepCircleEnabled(isChecked)
                .nextStepCircleColor(Color.GRAY)
                .commit();*/

        loadLayout();
    }

    private void loadLayout() {

        txv_stage = (TextView)findViewById(R.id.txv_stage);
        txv_stage.setText("First" + " " + "Stage");

        stepView = (StepView)findViewById(R.id.step_view) ;
        btn_next = (ImageView)findViewById(R.id.imv_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (_currentPage){

                    case 0:
                        stepView.go(_currentPage, true);
                        FirstQuestionFragment();
                        break;

                    case 1:
                        stepView.go(_currentPage, true);
                        thirdQuestionFragment();
                        break;

                    case 2:
                        stepView.go(_currentPage, true);
                        fourthQuestionFragment();
                        break;

                    case 3:
                        stepView.go(_currentPage, true);
                        fifthQuestionFragment();
                        break;

                    case 4:

                        stepView.go(_currentPage, true);
                        sixthQuestionFragment();
                        break;

                    case 5:
                        stepView.go(_currentPage, true);
                        eighthQuestionFragment();
                        break;

                    case 6:
                        stepView.go(_currentPage, true);
                        //gotoSignActivity();
                        gotoMainActivity();
                        break;

                    /*case 7:
                        stepView.go(_currentPage, true);
                        break;

                    case 8:

                        break;*/
                }

            }


        });

        findViewById(R.id.imv_pre).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* _currentPage--;
                stepView.go(_currentPage, false);*/


            }
        });

        findViewById(R.id.imv_forward).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                /*if (i <8){
                    i++;
                }*/
            }
        });

        stepView.getState()
                .selectedTextColor(ContextCompat.getColor(this, R.color.step_indicator))
                .animationType(StepView.ANIMATION_ALL)
                .selectedCircleColor(ContextCompat.getColor(this, R.color.step_indicator))
                .selectedStepNumberColor(ContextCompat.getColor(this, R.color.white))
                // You should specify only stepsNumber or steps array of strings.
                // In case you specify both steps array is chosen.

                // You should specify only steps number or steps array of strings.
                // In case you specify both steps array is chosen.
                .stepsNumber(6)
                /*.animationDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))*/
                .stepLineWidth(5)
                .textSize(25)
                .nextStepCircleColor(getResources().getColor(R.color.grey))
                .stepNumberTextSize(35)
                // other state methods are equal to the corresponding xml attributes
                .commit();



        stepView.setOnStepClickListener(new StepView.OnStepClickListener() {
            @Override
            public void onStepClick(int step) {
                // 0 is the first step


            }
        });

        FirstQuestionFragment();

    }

    private void gotoMainActivity(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void gotoSignActivity(){

        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    public void FirstQuestionFragment(){

        _currentPage = 1;
        txv_stage.setText("First" + " " + "Stage");

        Question1Fragment fragment = new Question1Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void SecondQuestionFragment(){

        _currentPage = 2;
        txv_stage.setText("Second" + " " + "Stage");

        Question2Fragment fragment = new Question2Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void thirdQuestionFragment(){

        _currentPage = 2;
        txv_stage.setText("Second" + " " + "Stage");


        Question3Fragment fragment = new Question3Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void fourthQuestionFragment(){

        _currentPage = 3;
        txv_stage.setText("Third" + " " + "Stage");

        Question4Fragment fragment = new Question4Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void fifthQuestionFragment(){


        _currentPage = 4;
        txv_stage.setText("Fourth" + " " + "Stage");


        Question5Fragment fragment = new Question5Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void sixthQuestionFragment(){


        _currentPage = 5;
        txv_stage.setText("Fifth" + " " + "Stage");


        Question6Fragment fragment = new Question6Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void seventhQuestionFragment(){

        _currentPage = 7;
        txv_stage.setText("Seventh" + " " + "Stage");

        Question7Fragment fragment = new Question7Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void eighthQuestionFragment(){

        _currentPage = 6;
        txv_stage.setText("Sixth" + " " + "Stage");

        Question8Fragment fragment = new Question8Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }
}
