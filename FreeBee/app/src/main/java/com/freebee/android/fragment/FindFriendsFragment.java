package com.freebee.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.freebee.android.R;

import com.freebee.android.base.BaseFragment;
import com.freebee.android.main.FindFriendsActivity;

/**
 * Created by GoldRain on 10/12/2016.
 */

@SuppressLint("ValidFragment")
public class FindFriendsFragment extends BaseFragment implements View.OnClickListener {

    LinearLayout ui_lyt_find_ht,ui_lyt_facebook, ui_lyt_invit_ht;
    TextView ui_txvInvite;

    Context _context;

    public FindFriendsFragment(Context context) {
        this._context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_find_friends, container,false);

        ui_lyt_find_ht = (LinearLayout)view.findViewById(R.id.lyt_find_friend);
        ui_lyt_find_ht.setOnClickListener(this);

        ui_lyt_facebook = (LinearLayout)view.findViewById(R.id.lyt_from_facebook);
        ui_lyt_facebook.setOnClickListener(this);

        ui_lyt_invit_ht = (LinearLayout)view.findViewById(R.id.lyt_share);
        ui_lyt_invit_ht.setOnClickListener(this);

        ui_txvInvite = (TextView)view.findViewById(R.id.txv_invite_friends);
        ui_txvInvite.setOnClickListener(this);

        return view ;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.lyt_find_friend:
                _context.startActivity(new Intent(_context, FindFriendsActivity.class));
                break;
            case R.id.lyt_from_facebook:

                break;
            case R.id.lyt_share:
                Share("","");
                break;
            case R.id.txv_invite_friends:
                Share("","");
                break;
        }
    }

}
