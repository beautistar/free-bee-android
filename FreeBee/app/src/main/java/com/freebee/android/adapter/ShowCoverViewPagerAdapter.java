package com.freebee.android.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.freebee.android.R;
import com.freebee.android.main.ShowCoverActivity;

import java.util.ArrayList;

/**
 * Created by STS on 10/17/2016.
 */

public class ShowCoverViewPagerAdapter extends PagerAdapter {

    ShowCoverActivity _activity;
    LayoutInflater mLayoutInflater;

    ArrayList<Integer > upload_images = new ArrayList<>();

    public ShowCoverViewPagerAdapter(ShowCoverActivity activity, ArrayList<Integer> images) {

        this._activity = activity;
        this.upload_images = images;
        mLayoutInflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return upload_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_cover_image, container, false);

        ImageView ui_imv_cover = (ImageView)itemView.findViewById(R.id.imv_cover);
        ui_imv_cover.setImageResource(upload_images.get(position));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
