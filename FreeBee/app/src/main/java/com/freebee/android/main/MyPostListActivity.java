package com.freebee.android.main;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.adapter.MyPostListAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyPostListActivity extends CommonActivity {

    ArrayList<ProductEntity> allProducts = new ArrayList<>();
    MyPostListAdapter _adapter;
    ListView lst_my_post;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post_list);

        loadLayout();
    }

    private void loadLayout(){

        lst_my_post = (ListView)findViewById(R.id.lst_my_post);

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyPostListActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        getProductList();
    }

    private void getProductList(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_USERPROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseUserProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseResponseUserProfile(String response){

        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray list_of_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);

                for (int i = 0; i < list_of_products.length(); i++) {

                    JSONObject jsonObject = (JSONObject) list_of_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    allProducts.add(_list);
                }

                ArrayList<ProductEntity> productEntities = new ArrayList<>();

                for (int j = 0; j < allProducts.size(); j++){

                    productEntities.add(allProducts.get(allProducts.size() - j - 1));
                }

                _adapter = new MyPostListAdapter(this, productEntities);
                lst_my_post.setAdapter(_adapter);
            }

        } catch (JSONException e) {

            closeProgress();

            e.printStackTrace();
        }
    }
}

