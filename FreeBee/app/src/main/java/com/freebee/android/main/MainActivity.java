package com.freebee.android.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.freebee.android.FreeBeeApplication;

import com.freebee.android.R;
import com.freebee.android.adapter.GridViewAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;
import com.freebee.android.utils.RadiusImageView;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.freebee.android.commons.ReqConst.CODE_SUCCESS;
import static com.freebee.android.commons.ReqConst.RES_CODE;


public class MainActivity extends CommonActivity implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    UserEntity _user = new UserEntity();
    ArrayList<ProductEntity> _list_of_products = new ArrayList<>();
    ArrayList<ProductEntity> _like_of_products = new ArrayList<>();
    ArrayList<ProductEntity> _bought_of_products = new ArrayList<>();

    ImageView ui_imv_call_draw, imv_filter;
    EditText edt_search;

    TextView ui_txvName, ui_txvMyprofile ,txv_listed_num,txv_liked_num,txv_bought_num, txv_balance;
    RadiusImageView ui_imvPhoto;
    NavigationView ui_drawer_menu;
    DrawerLayout ui_drawerlayout;
    FrameLayout ui_fyt_sell;
    FloatingActionButton ui_fab_sell;

    LinearLayout ui_lyt_home, ui_lyt_post, ui_lyt_my_product, ui_lyt_setting, lyt_my_balance, ui_lyt_invite, ui_lyt_help , ui_lyt_bundle ;
    TextView txv_logout;
    LinearLayout ui_lyt_myProfile ;
    RelativeLayout ui_lyt_listed, ui_lyt_bought, ui_lyt_liked ;
    View headerView;

    GridViewAdapter adpter;
    GridView ui_gridview;

    //LatLng latLng = new LatLng(23.1,34.5);

    ArrayList<ProductEntity> _allProducts = new ArrayList<>();
    ArrayList<ProductEntity> searchResult = new ArrayList<>();
    int _currentPage = 1;

    SwipyRefreshLayout ui_RefreshLayout ;
    Boolean endpageChecked = true;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS,
            Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    //public static int MY_PEQUEST_CODE = 123;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchResult = (ArrayList<ProductEntity>) getIntent().getSerializableExtra(Constants.FILTER_RESULT);
        loadLayout();
        setupNavigationBar();

        checkAllPermission();

    }

    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }
/*==============================================================================*/

    @SuppressLint("InlinedApi")
    private void loadLayout() {


        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getBalance();
                showDrawer();
            }
        });

        edt_search = (EditText)findViewById(R.id.edt_search);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                String text = edt_search.getText().toString().toLowerCase(Locale.getDefault());
                adpter.searchProduct(text);

            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        imv_filter = (ImageView)findViewById(R.id.imv_filter);
        imv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gotoBundleActivity();
                gotoFilterActivity();
            }
        });

        ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        headerView = ui_drawer_menu.getHeaderView(0);

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        //navigation header my profile part
        ui_txvName = (TextView)headerView.findViewById(R.id.txv_name);
        ui_txvMyprofile = (TextView)headerView.findViewById(R.id.txv_myprofile);
        ui_imvPhoto = (RadiusImageView)headerView.findViewById(R.id.imv_photo);
        txv_listed_num = (TextView)headerView.findViewById(R.id.txv_listed_num);
        txv_liked_num = (TextView)headerView.findViewById(R.id.txv_liked_num);
        txv_bought_num = (TextView)headerView.findViewById(R.id.txv_bought_num);
        txv_balance = (TextView)headerView.findViewById(R.id.txv_balance);
        txv_balance.setText("$" + Commons.g_user.getBalance());
        ////////////////////////////////////////////////////////////

        ui_fab_sell = (FloatingActionButton)findViewById(R.id.fab_sell_stuff);
        ui_fab_sell.setOnClickListener(this);


        ui_lyt_home = (LinearLayout)headerView.findViewById(R.id.lyt_home);
        ui_lyt_home.setOnClickListener(this);


        ui_lyt_post = (LinearLayout)headerView.findViewById(R.id.lyt_post);
        ui_lyt_post.setOnClickListener(this);

        ui_lyt_my_product = (LinearLayout)headerView.findViewById(R.id.lyt_my_product);
        ui_lyt_my_product.setOnClickListener(this);

        ui_lyt_help = (LinearLayout)headerView.findViewById(R.id.lyt_help);
        ui_lyt_help.setOnClickListener(this);

        ui_lyt_invite = (LinearLayout)headerView.findViewById(R.id.lyt_invite);
        ui_lyt_invite.setOnClickListener(this);

        ui_lyt_setting = (LinearLayout)headerView.findViewById(R.id.lyt_setting);
        ui_lyt_setting.setOnClickListener(this);

        ui_lyt_myProfile = (LinearLayout)headerView.findViewById(R.id.lyt_myprofile);
        ui_lyt_myProfile.setOnClickListener(this);

        ui_lyt_bundle = (LinearLayout)headerView.findViewById(R.id.lyt_bundle);
        ui_lyt_bundle.setOnClickListener(this);

        lyt_my_balance = (LinearLayout)headerView.findViewById(R.id.lyt_my_balance);
        lyt_my_balance.setOnClickListener(this);

        ui_lyt_listed = (RelativeLayout)headerView.findViewById(R.id.lyt_listed);
        ui_lyt_listed.setOnClickListener(this);

        ui_lyt_liked = (RelativeLayout)headerView.findViewById(R.id.lyt_liked);
        ui_lyt_liked.setOnClickListener(this);

        ui_lyt_bought = (RelativeLayout)headerView.findViewById(R.id.lyt_bought);
        ui_lyt_bought.setOnClickListener(this);

        txv_logout = (TextView)headerView.findViewById(R.id.txv_logout);
        txv_logout.setOnClickListener(this);

        ui_RefreshLayout = (SwipyRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        ui_RefreshLayout.setOnRefreshListener(this);
        ui_RefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);

        ui_gridview = (GridView)findViewById(R.id.stageger_gridview);
        adpter = new GridViewAdapter(this);
        ui_gridview.setAdapter(adpter);

        ui_drawerlayout.closeDrawers();

        //_currentPage = Constants.current_page ;

        //  ui_gridview.setAdapter(adpter);

        if (searchResult != null && searchResult.size() != 0){

            if (searchResult.size() > 0){
                adpter.setProduct(searchResult);
                adpter.notifyDataSetChanged();
            }

        }else {

            ui_RefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    getProduct_list();
                }
            });
        }

    }
    private void updateProfile(){

        ui_txvName.setText(Commons.g_user.get_name());
        if (Commons.g_user.get_imageUrl().length() > 0)
            Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_menu_avatar).into(ui_imvPhoto);

        txv_listed_num.setText(String.valueOf(Commons.PRODUCT_LIST_COUNT));
        txv_liked_num.setText(String.valueOf(Commons.PRODUCT_LIKE_COUNT));
        txv_bought_num.setText(String.valueOf(Commons.PRODUCT_BOUGHT_COUNT));
    }

    private void getProduct_list(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_PRODUCTLIST;

        ui_RefreshLayout.setRefreshing(true);

        if (_currentPage == 0) _currentPage = 1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseProductList(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ui_RefreshLayout.setRefreshing(false);

                showAlertDialog(getString(R.string.error));
                closeProgress();
            }

        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("page", String.valueOf(_currentPage));
                    params.put("age", String.valueOf(Commons.g_user.getAge()));
                    params.put("gender", Commons.g_user.getGender());

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseProductList(String response){

        ui_RefreshLayout.setRefreshing(false);

        try {

            JSONObject object = new JSONObject(response);

            Log.d("==respons===main====", response);

            int result_code = object.getInt(RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray products = object.getJSONArray(ReqConst.RES_PRODUCTLIST);

                //_products.clear();

                for (int i = 0; i < products.length(); i++ ){

                    JSONObject jsonProduct = (JSONObject)products.get(i);

                    ProductEntity product = new ProductEntity();

                    product.set_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_ID));
                    product.set_name(jsonProduct.getString("product_name"));
                    product.set_imagUrl(jsonProduct.getString("product_image"));

                   // Log.d("==========photo=======", product.get_imagUrl());

                    _allProducts.add(product);
                }

                _currentPage++ ;

                adpter.setProduct(_allProducts);
                adpter.notifyDataSetChanged();

                UserProfile();

                //ui_gridview.setSelection(Constants.list_position)

            } else {

                ui_RefreshLayout.setRefreshing(false);
                endpageChecked = false ;

                //showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        getProduct_list();
    }

    private void getBalance(){

        String url = ReqConst.SERVER_URL + "get_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    int resultCode = json.getInt(RES_CODE);
                    if (resultCode == CODE_SUCCESS){

                        Commons.g_user.setBalance(json.getString("balance"));
                        txv_balance.setText("$" + Commons.g_user.getBalance());

                        Log.d("main balance", Commons.g_user.getBalance());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void UserProfile(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_USERPROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseUserProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                //showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    //showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseResponseUserProfile(String response){

        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(RES_CODE);

            JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _user = new UserEntity();

                _user.set_idx(userProfile.getInt(ReqConst.RES_USERID));
                _user.set_name(userProfile.getString(ReqConst.RES_USERNAME));
                _user.set_imageUrl(userProfile.getString(ReqConst.RES_USERIMAGE));
                _user.set_count_followers(userProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                _user.set_count_following(userProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                _user.set_isVerified(userProfile.getInt(ReqConst.RES_ISVERIFIED));

                _user.set_verify_facebook(userProfile.getString(ReqConst.RES_VERIFYFACEBOOK));
                _user.set_verify_google(userProfile.getString(ReqConst.RES_VERIFYGOOGLE));

                _user.set_phone(userProfile.getString(ReqConst.REQ_PHONE));
                _user.setAddress(userProfile.getString("address"));
                _user.setCity(userProfile.getString("city"));
                _user.setState(userProfile.getString("state"));
                _user.setZip_code(userProfile.getString("zip_code"));
                _user.setAge(userProfile.getInt("age"));
                _user.setGender(userProfile.getString("gender"));

                JSONArray list_of_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);

                for (int i = 0; i < list_of_products.length(); i++) {

                    Commons.PRODUCT_LIST_COUNT = list_of_products.length();

                    JSONObject jsonObject = (JSONObject) list_of_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    _list_of_products.add(_list);
                }

                JSONArray product_likes = userProfile.getJSONArray(ReqConst.RES_PRODUCTLIKES);

                for (int i = 0; i < product_likes.length(); i++){

                    Commons.PRODUCT_LIKE_COUNT = product_likes.length();

                    JSONObject jsonObject = (JSONObject) product_likes.get(i);

                    ProductEntity _like = new ProductEntity();

                    _like.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _like.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _like.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    _like_of_products.add(_like);
                }

                JSONArray product_bought = userProfile.getJSONArray(ReqConst.RES_PRODUCTBOUGHT);

                for (int i = 0; i < product_bought.length(); i++){

                    Commons.PRODUCT_BOUGHT_COUNT = product_bought.length();

                    JSONObject jsonObject = (JSONObject)product_bought.get(i);

                    ProductEntity _bought = new ProductEntity();

                    _bought.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _bought.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _bought.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    _bought_of_products.add(_bought);
                }

                Commons.g_user = _user;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateProfile();
                    }
                });

            }

        } catch (JSONException e) {

            closeProgress();

            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lyt_liked:
                gotoMyProfileActivity(R.id.lyt_liked);
                break;

            case R.id.lyt_listed:
                gotoMyProfileActivity(R.id.lyt_listed);
                break;

            case R.id.lyt_bought:
                gotoMyProfileActivity(R.id.lyt_bought);
                break;

            case R.id.lyt_home:

                //gotoHomeActivity();
                ui_drawerlayout.closeDrawers();
                _currentPage = 1;
                _allProducts.clear();
                searchResult = null;

                loadLayout();
                //finish();
                break;
            case R.id.lyt_post:
                gotoPostActivity();
                break;

            case R.id.lyt_my_product:
                gotoMyPostActivity();
                break;
            case R.id.lyt_bundle:
                gotoBundleActivity();
                break;

            case R.id.lyt_my_balance:
                gotoMyBalanceActivity();
                break;
            case R.id.lyt_invite:
                gotoInviteActivity();
                break;
            case R.id.lyt_setting:
                gotoSettingsActivity();
                break;
            case R.id.lyt_help:
                gotoHelpActivity();
                break;
            case R.id.fab_sell_stuff:
                gotoPostActivity();
                break;
            case R.id.lyt_myprofile:
                gotoMyProfileActivity(R.id.lyt_myprofile);
                break;

            case R.id.txv_logout:
                gotoLogout();
                break;
        }

        ui_drawerlayout.closeDrawers();

    }

    private void gotoFilterActivity(){

        startActivity(new Intent(this, FilterListActivity.class));
        finish();
    }

    private void gotoHomeActivity() {

        Intent intent = new Intent(this , MainActivity.class);
        startActivity(intent);
    }

    private void gotoPostActivity() {

        Intent intent =  new Intent( this , PostActivity.class);
        startActivity(intent);

        ui_drawerlayout.closeDrawers();
        finish();
    }

    private void gotoMyPostActivity(){

        startActivity(new Intent(this, MyPostListActivity.class));
        ui_drawerlayout.closeDrawers();
        finish();
    }

    private void gotoInviteActivity() {

        Share("", "");
    }

    private void gotoSettingsActivity() {

        Intent intent = new Intent(this , SettingActivity.class) ;
        startActivity(intent);
        finish();

    }

    private void gotoHelpActivity() {

        Intent intent = new Intent(this , HelpActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoSearchActivity() {

        Intent intent = new Intent(this , SearchActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoBundleActivity() {

        int position = 2;
        Intent intent = new Intent(this , MyBundleActivity.class);
        intent.putExtra(Constants.POSITION, position);
        startActivity(intent);
        finish();
    }

    private void gotoMyBalanceActivity(){

        startActivity(new Intent(MainActivity.this, MyBalanceActivity.class));
        finish();
    }

    private void gotoMyProfileActivity(int view_id) {

        switch (view_id){

            case R.id.lyt_myprofile:
                Intent intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent);
                finish();
                break;

            case R.id.lyt_listed:
                Intent intent1 = new Intent(this, UserProfileActivity.class);
                intent1.putExtra(Constants.MYPROFILE_POSITION, 0);
                intent1.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent1);
                finish();
                break;

            case R.id.lyt_liked:
                Intent intent2 = new Intent(this, UserProfileActivity.class);
                intent2.putExtra(Constants.MYPROFILE_POSITION, 1);
                intent2.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent2);
                finish();
                break;

            case R.id.lyt_bought:
                Intent intent3 = new Intent(this, UserProfileActivity.class);
                intent3.putExtra(Constants.MYPROFILE_POSITION, 2);
                intent3.putExtra(Constants.USERID, Commons.g_user.get_idx());
                startActivity(intent3);
                finish();
                break;
        }

    }

    public void gotoProduct(int productId){

        Intent intent = new Intent(this, ProductActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.PRODUCTID, productId);
        startActivity(intent);
        finish();
    }

    private void gotoLogout(){

        Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");
        Prefs.putString(Constants.TOKEN, "");

        registerToken();
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    private void registerToken(){

        if (Prefs.getString(Constants.TOKEN, "").length() == 0){
            return;
        }

        String url = ReqConst.SERVER_URL + "register_token";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));
                    params.put("token", Prefs.getString(Constants.TOKEN, ""));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    @Override
    public void onBackPressed() {

        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else onExit();
    }

    @Override
    protected void onResume() {
        ui_drawerlayout.closeDrawers();
        super.onResume();
    }
}

