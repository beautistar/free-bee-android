package com.freebee.android.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/28/2016.
 */

public class UserEntity implements Serializable {

    int _idx = 0 ;
    String _email = "";
    String _name = "";
    String _imageUrl = "";
    String _phone = "";
    String _about = "";

    int _isVerified = 0 ;

    int _count_followers = 0;
    int _count_following = 0;

    boolean _isFollowing = false ;
    boolean _isFollower = false ;
    String _is_followed = ""; // Y : yes- follow, N: no
    String _verify_facebook = "";
    String _verify_google = "";
    String _verify_phone = "";
    int _child = 0;
    int age = 0;
    String gender = "male";
    String address = "";
    String city = "";
    String state = "";
    String zip_code = "";
    String balance = "";

    ArrayList<ProductEntity> _listProducts = new ArrayList<>();
    ArrayList<ProductEntity> _likedProducts = new ArrayList<>();
    ArrayList<ProductEntity> _boughtProducts = new ArrayList<>();

    ArrayList<UserEntity> _followerList = new ArrayList<>();

    ArrayList<UserEntity> _followingList = new ArrayList<>();


    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_imageUrl() {
        return _imageUrl;
    }

    public void set_imageUrl(String _imageUrl) {
        this._imageUrl = _imageUrl;
    }

    public String get_about() {
        return _about;
    }

    public void set_about(String _about) {
        this._about = _about;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public int get_isVerified() {
        return _isVerified;
    }

    public void set_isVerified(int _isVerified) {
        this._isVerified = _isVerified;
    }

    public ArrayList<ProductEntity> get_listProducts() {
        return _listProducts;
    }

    public void set_listProducts(ArrayList<ProductEntity> _listProducts) {
        this._listProducts = _listProducts;
    }

    public ArrayList<ProductEntity> get_likedProducts() {
        return _likedProducts;
    }

    public void set_likedProducts(ArrayList<ProductEntity> _likedProducts) {
        this._likedProducts = _likedProducts;
    }

    public ArrayList<ProductEntity> get_boughtProducts() {
        return _boughtProducts;
    }

    public void set_boughtProducts(ArrayList<ProductEntity> _boughtProducts) {
        this._boughtProducts = _boughtProducts;
    }

    public ArrayList<UserEntity> get_followerList() {
        return _followerList;
    }

    public void set_followerList(ArrayList<UserEntity> _followerList) {
        this._followerList = _followerList;
    }

    public ArrayList<UserEntity> get_followingList() {
        return _followingList;
    }

    public void set_followingList(ArrayList<UserEntity> _followingList) {
        this._followingList = _followingList;
    }

    public String get_is_followed() {
        return _is_followed;
    }

    public void set_is_followed(String _is_followed) {
        this._is_followed = _is_followed;
    }

    public boolean is_isFollowing() {
        return _isFollowing;
    }

    public void set_isFollowing(boolean _isFollowing) {
        this._isFollowing = _isFollowing;
    }

    public boolean is_isFollower() {
        return _isFollower;
    }

    public void set_isFollower(boolean _isFollower) {
        this._isFollower = _isFollower;
    }

    public int get_count_followers() {return _count_followers;}

    public void set_count_followers(int _count_followers) {this._count_followers = _count_followers;}

    public int get_count_following() {return _count_following;}

    public void set_count_following(int _count_following) {this._count_following = _count_following;}

    public int getFollowerCount() {

        return _followerList.size();
    }

    public int getFolloingCount() {

        return _followingList.size();
    }

    public void addFollower(UserEntity user) {

        _followerList.add(user);
    }

    public void addFollowing(UserEntity user) {

        _followingList.add(user);
    }

    public void removeFollower(UserEntity user) {

        _followerList.remove(user);
    }

    public void removeFollowing(UserEntity user) {

        _followingList.remove(user);
    }

    public String get_verify_facebook() {
        return _verify_facebook;
    }

    public void set_verify_facebook(String _verify_facebook) {
        this._verify_facebook = _verify_facebook;
    }

    public String get_verify_google() {
        return _verify_google;
    }

    public void set_verify_google(String _verify_google) {
        this._verify_google = _verify_google;
    }

    public String get_verify_phone(){
        return _verify_phone;
    }

    public void set_verify_phone(String _verify_phone){
        this._verify_phone = _verify_phone;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public int get_child() {
        return _child;
    }

    public void set_child(int _child) {
        this._child = _child;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}


