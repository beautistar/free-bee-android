package com.freebee.android.adapter;

import com.freebee.android.R;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.FindFriendsActivity;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.utils.RadiusImageView;


import java.util.ArrayList;

/**
 * Created by ToSuccess on 10/18/2016.
 */

public class ListViewFindViewAdapter extends BaseAdapter{

    FindFriendsActivity activity;

    ArrayList<UserEntity> _searchFriends = new ArrayList<>();
    ArrayList<UserEntity> _allSearchFriends = new ArrayList<>();

    public ListViewFindViewAdapter(FindFriendsActivity activity) {

        this.activity = activity;
    }

    public void setFriendData(ArrayList<UserEntity> searchFriends){

        _allSearchFriends = searchFriends;
        _searchFriends.clear();
        _searchFriends.addAll(_allSearchFriends);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _searchFriends.size();
    }

    @Override
    public Object getItem(int position) {
        return _searchFriends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SearchHolder searchHolder;

        if (convertView == null){

            searchHolder = new SearchHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =  inflater.inflate(R.layout.item_findfriend_listview, parent,false);

            searchHolder.imv_photo = (RadiusImageView)convertView.findViewById(R.id.imv_photo);
            searchHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            searchHolder.imv_state = (ImageView)convertView.findViewById(R.id.imv_state);

            convertView.setTag(searchHolder);

        } else {

            searchHolder = (SearchHolder)convertView.getTag();
        }

        final UserEntity searchFriend = (UserEntity) _searchFriends.get(position);

        Glide.with(activity).load(searchFriend.get_imageUrl()).placeholder(R.drawable.user_no_img).into(searchHolder.imv_photo);
        searchHolder.txv_name.setText(searchFriend.get_name());

        if (searchFriend.get_is_followed().equals("Y")){

            searchHolder.imv_state.setSelected(true);

        } else searchHolder.imv_state.setSelected(false);

        searchHolder.imv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!searchFriend.get_is_followed().equals("Y")) {

                    activity.setFollow(searchFriend);

                } else {

                    activity.setUnFollow(searchFriend);
                }
            }
        });


        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (activity , UserProfileActivity.class);
                intent.putExtra(Constants.USER, searchFriend);
                activity.startActivity(intent);

            }
        });

        return convertView;
    }


    public class SearchHolder {

        RadiusImageView imv_photo;
        TextView txv_name;
        ImageView imv_state;
    }
}
