package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.freebee.android.R;
import com.bumptech.glide.Glide;

import com.freebee.android.main.MyBundleActivity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.utils.RadiusImageView;


import java.util.ArrayList;

/**
 * Created by GoldRain on 10/12/2016.
 */

public class ListViewFollowingAdapter extends BaseAdapter /*implements View.OnClickListener  */{

    ArrayList<UserEntity> _followings = new ArrayList<>();
    ArrayList<UserEntity> _allFollowing = new ArrayList<>();
    MyBundleActivity _activity;

    public ListViewFollowingAdapter(MyBundleActivity context) {

        this._activity = context;
    }

    public void setData(ArrayList<UserEntity> followings){


        _allFollowing = followings;
        _followings.clear();
        _followings.addAll(_allFollowing);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _followings.size();
    }

    @Override
    public Object getItem(int position) {
        return _followings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final UserHolder userHolder;

        if (convertView == null){

            userHolder = new UserHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =  inflater.inflate(R.layout.item_following_listview, parent,false);

            userHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name);
            userHolder.ui_imvPhoto = (RadiusImageView)convertView.findViewById(R.id.imv_photo);
            userHolder.imv_state = (ImageView)convertView.findViewById(R.id.imv_state);
            userHolder.lyt_list = (LinearLayout)convertView.findViewById(R.id.lyt_list);

            convertView.setTag(userHolder);

        } else {

            userHolder = (UserHolder)convertView.getTag();
        }

        final UserEntity user = (UserEntity) _followings.get(position);

        userHolder.ui_txvName.setText(user.get_name());
        Glide.with(_activity).load(user.get_imageUrl()).placeholder(R.drawable.user_no_img).into(userHolder.ui_imvPhoto);

   /*     if (user.get_is_followed().equals("Y")){

        } else userHolder.imv_state.setSelected(false);*/

        userHolder.imv_state.setSelected(true);


        userHolder.lyt_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.gotoUserProfile(user);

            }
        });

        return convertView;
    }

    private class UserHolder {

        RadiusImageView ui_imvPhoto ;
        TextView ui_txvName ;
        ImageView imv_state;
        LinearLayout lyt_list;

    }
}
