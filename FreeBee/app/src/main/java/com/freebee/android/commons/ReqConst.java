package com.freebee.android.commons;

import android.os.Handler;

import com.freebee.android.model.UserEntity;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class ReqConst {

    //public static final String SEVER_ADDR = "http://52.40.130.22";
    public static final String SEVER_ADDR = "http://free-bee.co";

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null ;
    public static String g_appVersion = "1.0";

    public static UserEntity g_newUser = null ;
    public static UserEntity g_user = null ;


    public static final String SERVER_URL = SEVER_ADDR + "/index.php/api/";

    //Request value

    public static final String REQ_PARAM_ID = "id";
    public static final String REQ_UPLOADIMAGE = "upload_image";
    public static final String REQ_REGISTER = "register";
    public static final String REQ_LOGIN = "login";
    public static final String REQ_EMAIL = "email";
    public static final String REQ_PASSWORD = "password";
    public static final String REQ_USERNAME = "username";
    public static final String REQ_PHONE = "phone";
    public static final String REQ_ADDRESS = "address";
    public static final String REQ_ZIP_CODE = "zip_code";
    public static final String REQ_AGE = "age";

    public static final String REQ_PARAM_FILE = "photo";
    public static final String REQ_PRODUCTLIST = "product_list";
    public static final String REQ_PRODUCTDETAIL = "product_detail";
    public static final String REQ_POSTAD = "post_ad";
    public static final String REQ_EDIT_PRODUCT = "edit_product";
    public static final String REQ_SEARCHPRODUCT = "search_product";
    public static final String REQ_USERFOLLOWING = "user_following";
    public static final String REQ_USERFOLLOWER = "user_follower";
    public static final String REQ_USERPROFILE = "user_profile";
    public static final String REQ_SEARCHFRIEND = "search_friend";
    public static final String REQ_REGISTERUSER = "register_user";
    public static final String REQ_UPDATEUSER = "update_user";
    public static final String REQ_FOLLOWFRIEND = "follow_friend";
    public static final String REQ_UNFOLLOW = "unfollow";
    public static final String REQ_LIKEPRODUCT = "like_product";
    public static final String REQ_PRODUCTNAME = "product_name";
    public static final String REQ_PRODUCTIMAGE = "product_iamage";
    public static final String REQ_PRODUCTDESCRIPTION = "product_description";
    public static final String REQ_PRODUCTPRICE = "product_price";
    public static final String REQ_CATEGORY = "category";
    public static final String REQ_LATITUDE = "latitude";
    public static final String REQ_LONGITUDE = "longitude";
    public static final String REQ_QUANTITY_AVAILABLE = "quantity_available";
    public static final String REQ_QUANTITY_PER_USER = "quantity_per_user";
    public static final String REQ_AGE_MIN = "age_min";
    public static final String REQ_AGE_MAX = "age_max";
    public static final String REQ_GENDER = "gender";
    public static final String REQ_HOBBY = "hobby";
    public static final String REQ_INDUSTRY = "industry";
    public static final String REQ_OCCUPATION = "occupation";
    public static final String REQ_ZIPCODE = "zipcode";
    public static final String REQ_CITY = "city";
    public static final String REQ_STATE = "state";
    public static final String REQ_COUNTRY = "country";
    public static final String REQ_FILTER_PRODUCT = "filter_product";
    public static final String RES_ADDRESS_LIST = "address_list";





    public static final String REQ_SEARCHTEXT = "searchtext";
    public static final String REQ_FRIENDID = "friendid";
    public static final String REQ_TYPE = "type";
    public static final String REQ_SOLDPRODUCT = "sold_product";
    public static final String REQ_VERIFYFACEBOOK = "verify_facebook";
    public static final String REQ_VERIFYGOOGLE = "verify_google";
    public static final String REQ_SOCIAL_SIGNUP = "social_signup";
    public static final String REQ_SOCIAL_LOGIN = "social_login";
    public static final String REQ_SOCIAL_ID = "social_id";
    public static final String REQ_PHOTO_URL = "photo_url";
    public static final String REQ_SOCIAL_TYPE = "social_type";
    public static final String REQ_ADD_CART_PRODUCT = "add_cart_product";
    public static final String REQ_REMOVE_CART_PRODUCT = "remove_cart";
    public static final String REQ_GET_CART = "get_cart_product";
    public static final String RES_PRODUCT_LIST = "product_list";
    public static final String REQ_COUNT = "count";
    public static final String REQ_DELETE_PRODUCT = "delete_product";
    public static final String REQ_CHECKOUT = "check_out";
    public static final String REQ_ADD_ADDRESS = "add_address";
    public static final String REQ_GET_ADDRESS = "get_address";


    //Response value

    public static final String RES_CODE = "result_code";
    public static final String RES_ERROR = "error";

    public static final String RES_ID = "id";
    public static final String RES_PRODUCTID = "productid";
    public static final String RES_PRODUCT_ID = "product_id";
    public static final String RES_PRODUCT_NAME = "product_name";
    public static final String RES_PRODUCT_IMAGE = "product_image";
    public static final String RES_PRODUCT_DESCRIPTION = "product_description";
    public static final String RES_CATEGORY = "category";
    public static final String RES_PRODUCTLATITUDE = "product_latitude";
    public static final String RES_PRODUCTLONGTITUDE = "product_longitude";

    public static final String RES_QUANTITY_AVAILABLE = "quantity_available";
    public static final String RES_QUANTITY_USER = "quantity_per_user";
    public static final String RES_AGE_MAX = "age_max";
    public static final String RES_AGE_MIN = "age_min";
    public static final String RES_GENDER = "gender";
    public static final String RES_HOBBY = "hobby";
    public static final String RES_INDUSTRY = "industry";
    public static final String RES_OCCUPATION = "occupation";
    public static final String RES_ZIPCODE = "zipcode";
    public static final String RES_CITY = "city";
    public static final String RES_STATE = "state";
    public static final String RES_COUNTRY = "country";
    public static final String RES_ISLIKED = "is_liked";
    public static final String RES_CART_PRODUCT = "cart_added";
    public static final String RES_PRODUCT_OWNER_NAME = "product_owner_name";
    public static final String RES_PRODUCT_OWNER_PHONE = "product_owner_phone";
    public static final String RES_PRODUCT_OWNER_IMAGE = "product_owner_image";
    public static final String RES_PRODUCT_OWNER_ID = "product_owner_id";
    public static final String RES_IS_SOLD = "is_sold";

    public static final String RES_PRODUCTPRICE = "product_price";
    public static final String RES_PRODUCTLIST = "product_list";
    public static final String RES_COUNTFOLLOWERS = "count_followers";
    public static final String RES_COUNTFOLLOWING = "count_following";
    public static final String RES_ISVERIFIED = "is_verified";
    public static final String RES_LISTOFPRODUCTS = "list_of_products";
    public static final String RES_PRODUCTLIKES = "product_likes";
    public static final String RES_PRODUCTBOUGHT = "product_bought";
    public static final String RES_PRODUCTDETAIL = "product_detail";
    public static final String RES_USERLIST = "user_list";
    public static final String RES_USERPROFILE = "user_profile";
    public static final String RES_USERID = "userid";
    public static final String RES_USERNAME = "username";
    public static final String RES_USERIMAGE = "userimage";
    public static final String RES_USERPHONE = "phone";
    public static final String RES_ABOUT= "about";
    public static final String RES_VERIFYFACEBOOK = "verify_facebook";
    public static final String RES_VERIFYGOOGLE = "verify_google";
    public static final String RES_SOLD_COUNT = "sold_count";
    public static final String REQ_REGISTER_USER_INFO = "register_userinfo";


    public static final String RES_USERINFOR = "user_info";


    public static final String RES_MSG = "message";
    public static final int CODE_SUCCESS = 0;
    public static final int CODE_EXIST_DEVICEID = 201;
    public static final int CODE_UPLOADFAIL = 202 ;
    public static final int CODE_EXISTEMAIL = 203 ;

    public static final int CODE_NOTEXISTEMAIL = 204;
    public static final int CODE_UNREGUSER = 102;
    public static final int CODE_WRONGPWD = 205;
    public static final int CODE_NOTEMAIL = 107;
    public static final int CODE_WRONGAUTH = 108;

}
