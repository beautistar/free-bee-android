package com.freebee.android.main;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.adapter.ViewPagerAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Constants;


import java.util.ArrayList;

public class DetailImageActivity extends CommonActivity implements View.OnClickListener{

    ViewPagerAdapter _mViewPagerAdapter ;
    ViewPager _viewPager ;

    private ArrayList<String> mImages = new ArrayList();


    TextView ui_txv_upload_img_cnt ;
    ImageView ui_imvBack, ui_imvDelete ;
    int _position = 0;

    @SuppressWarnings("SpellCheckingInspection")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image) ;


        mImages = getIntent().getStringArrayListExtra(Constants.IMAGES);

        _position = getIntent().getIntExtra(Constants.POSITION, 0);

        for (int i = 0 ; i < mImages.size(); i++){
            Log.d("Image url ==========>", mImages.get(i)) ;
        }


//        Log.d("=====position",String.valueOf(selectedPosition));

        String str = String.valueOf(_position + 1) + "/" + String.valueOf(mImages.size());
        CharSequence chr = str;

        ui_txv_upload_img_cnt = (TextView)findViewById(R.id.txv_upload_img_cnt);
        ui_txv_upload_img_cnt.setText(chr);


        initViewPager();
    }

    private void initViewPager() {

        ui_txv_upload_img_cnt = (TextView)findViewById(R.id.txv_upload_img_cnt);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvDelete = (ImageView)findViewById(R.id.imv_delete);
        ui_imvDelete.setOnClickListener(this);

        _mViewPagerAdapter = new ViewPagerAdapter(this, mImages);

        _viewPager = (ViewPager)findViewById(R.id.vip_upload_img);

        _viewPager.setAdapter(_mViewPagerAdapter);
        _viewPager.setCurrentItem(_position);

        Log.d("========",String.valueOf(_position));

        _viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {

                String str = String.valueOf(position+1) + "/" + String.valueOf(mImages.size());


                CharSequence chr = str;
                ui_txv_upload_img_cnt.setText(chr);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.imv_delete:
                break;
        }

    }

}

