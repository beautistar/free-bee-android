package com.freebee.android.adapter;

import android.app.Activity;

import com.bumptech.glide.Glide;
import com.freebee.android.R;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.freebee.android.main.DetailImageActivity;
import com.freebee.android.main.PostActivity;

import java.util.ArrayList;

/**
 * Created by GoldRain on 10/2/2016.
 */

public class RecyclerViewAdapterPost extends RecyclerView.Adapter<RecyclerViewAdapterPost.ImageHolder>{

    private ArrayList<String> _imageUrls = new ArrayList<>();
    private PostActivity _context;
    //private ArrayList<Integer> _images = new ArrayList<>();

    public RecyclerViewAdapterPost(PostActivity context ) {

        this._context = context ;
        //_images.add(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
    }
    @Override
    public ImageHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo_image, viewGroup  , false);
        ImageHolder viewHolder = new ImageHolder(view);

        return viewHolder;
    }

    public void setDatas (ArrayList<String> images) {

        _imageUrls = images ;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder, final int position) {

        String imageUrl = _imageUrls.get(position);
        Glide.with(_context).load(imageUrl).placeholder(new ColorDrawable(0xff5f99fb)).into(viewHolder.imvPhoto);

        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
        viewHolder.imvPhoto.setImageBitmap(bitmap);

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                if(_context instanceof PostActivity) {
//
//                    ((PostActivity) _context).gotoDetailActivity(_imageUrls, position);
//
//                }

                Intent intent = new Intent(_context , DetailImageActivity.class);
                intent.putStringArrayListExtra("images", _imageUrls);
                intent.putExtra("position", position);
                _context.startActivity(intent);

                Activity activity = (Activity)_context ;
                activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

            }
        });

        viewHolder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _context.removeImage(_imageUrls.get(position));
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {

        return (null!= _imageUrls ? _imageUrls.size() : 0);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto;
        ImageView imvDelete;

        public ImageHolder(View view) {
            super(view);
            imvPhoto = (ImageView) view.findViewById(R.id.imv_image);
            imvDelete = (ImageView)view.findViewById(R.id.imv_delete);
        }
    }
}
