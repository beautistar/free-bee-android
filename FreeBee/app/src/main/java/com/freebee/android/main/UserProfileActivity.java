package com.freebee.android.main;

import android.content.Intent;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import com.freebee.android.adapter.ViewPagerAdaper_UserProfile;
import com.freebee.android.base.CommonTabActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserProfileActivity extends CommonTabActivity implements View.OnClickListener {

    UserEntity _user = new UserEntity();
    ViewPagerAdaper_UserProfile adapter ;

    ArrayList<ProductEntity> _list_of_products = new ArrayList<>();
    ArrayList<ProductEntity> _like_of_products = new ArrayList<>();
    ArrayList<ProductEntity> _bought_of_products = new ArrayList<>();

    TabLayout tabLayout ;
    ViewPager viewPager ;
    int position = 0;
    int _userID = 0;

    ImageView ui_imvBack,imv_user, ui_imvUnfollow, ui_imvShare, imv_following, imv_followers, imv_userpfofile ;
    TextView txv_name, txv_follwing_num,txv_followers_num ;

    ImageView ui_imvFacebook, ui_imvGoogle, ui_imvPhone;

    CollapsingToolbarLayout collapsingToolbarLayout;
    /*NestedScrollView ui_nested_scrollview;*/
    Toolbar toolbar ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);


        position = getIntent().getIntExtra(Constants.MYPROFILE_POSITION, 0);
        _user = (UserEntity) getIntent().getSerializableExtra(Constants.USER);

        _userID = (int)getIntent().getIntExtra(Constants.USERID, 0);

        if (_user!= null){

            _userID = _user.get_idx();
        }

        Log.d("===userID======", String.valueOf(_userID));
        loadLayout();
    }

    private void loadLayout() {

        toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
//        collapsingToolbarLayout.setTitle("UserProfile");


        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvShare = (ImageView)findViewById(R.id.imv_share);
        ui_imvShare.setOnClickListener(this);

       /* ui_imvUnfollow = (ImageView)findViewById(R.id.imv_unfollow);
        ui_imvUnfollow.setOnClickListener(this);*/

        imv_userpfofile = (ImageView)findViewById(R.id.imv_editProfofile);
        imv_userpfofile.setOnClickListener(this);

        if (Commons.g_user.get_idx() == _userID){

            imv_userpfofile.setVisibility(View.VISIBLE);

        }else {

            imv_userpfofile.setVisibility(View.GONE);
        }

        imv_user = (ImageView)findViewById(R.id.imv_user);

        imv_following = (ImageView)findViewById(R.id.imv_following);
        imv_following.setOnClickListener(this);

        imv_followers = (ImageView)findViewById(R.id.imv_followers);
        imv_followers.setOnClickListener(this);

        txv_name = (TextView)findViewById(R.id.txv_name);
        txv_follwing_num = (TextView)findViewById(R.id.txv_following_num);
        txv_followers_num = (TextView)findViewById(R.id.txv_followers_num);
        //txv_description = (TextView)findViewById(R.id.txv_description);

        viewPager = (ViewPager)findViewById(R.id.viewpager);

        ui_imvFacebook = (ImageView)findViewById(R.id.imv_face);
        ui_imvGoogle = (ImageView)findViewById(R.id.imv_google);
        ui_imvPhone = (ImageView)findViewById(R.id.imv_phone);

        actionBarResponsive();

        UserProfile();
    }

    private void UserProfile(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_USERPROFILE;

        Log.d("=UserPRofile=URL===", url);
        Log.d("======MyId=====", String.valueOf(Commons.g_user.get_idx()));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseUserProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_USERID, String.valueOf(_userID));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseResponseUserProfile(String response){

        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _user = new UserEntity();

                _user.set_idx(userProfile.getInt(ReqConst.RES_USERID));
                _user.set_name(userProfile.getString(ReqConst.RES_USERNAME));
                _user.set_phone(userProfile.getString(ReqConst.RES_USERPHONE));
                _user.set_imageUrl(userProfile.getString(ReqConst.RES_USERIMAGE));
                _user.set_count_followers(userProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                _user.set_count_following(userProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                _user.set_isVerified(userProfile.getInt(ReqConst.RES_ISVERIFIED));
                _user.set_verify_facebook(userProfile.getString(ReqConst.RES_VERIFYFACEBOOK));
                _user.set_verify_google(userProfile.getString(ReqConst.RES_VERIFYGOOGLE));

                _user.set_phone(userProfile.getString(ReqConst.REQ_PHONE));
                _user.setAddress(userProfile.getString("address"));
                _user.setCity(userProfile.getString("city"));
                _user.setState(userProfile.getString("state"));
                _user.setZip_code(userProfile.getString("zip_code"));
                _user.setAge(userProfile.getInt("age"));
                _user.setGender(userProfile.getString("gender"));


                JSONArray list_of_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);

                for (int i = 0; i < list_of_products.length(); i++) {
                    JSONObject jsonObject = (JSONObject) list_of_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    _list_of_products.add(_list);
                }

                JSONArray product_likes = userProfile.getJSONArray(ReqConst.RES_PRODUCTLIKES);

                for (int i = 0; i < product_likes.length(); i++){

                    JSONObject jsonObject = (JSONObject) product_likes.get(i);

                    ProductEntity _like = new ProductEntity();

                    _like.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _like.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _like.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    _like_of_products.add(_like);
                }

                JSONArray product_bought = userProfile.getJSONArray(ReqConst.RES_PRODUCTBOUGHT);

                for (int i = 0; i < product_bought.length(); i++){

                    JSONObject jsonObject = (JSONObject)product_bought.get(i);

                    ProductEntity _bought = new ProductEntity();

                    _bought.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _bought.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _bought.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));
                }

                /////////////////Fragment Setting Adapter ////////////////////
                FragmentManager manager = getSupportFragmentManager();
                adapter = new ViewPagerAdaper_UserProfile(this,manager,_list_of_products, _like_of_products, _bought_of_products );

                viewPager.setAdapter(adapter);
                tabLayout = (TabLayout)findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(viewPager);

                viewPager.setCurrentItem(position);

                ///////////////////////////////////////////////////////////////////

                if (_user.get_idx() == Commons.g_user.get_idx()){

                    Commons.g_user = _user;
                }

                //_user = user;

                showUserProfile();
            }

        } catch (JSONException e) {

            closeProgress();

            e.printStackTrace();
        }
    }

    private void showUserProfile() {

        txv_name.setText(_user.get_name());
        Glide.with(this).load(_user.get_imageUrl()).placeholder(R.drawable.ic_logo).into(imv_user);

        /*if (_user.get_is_followed().equals("Y")) ui_imvUnfollow.setImageResource(R.drawable.ic_social_onboarding);
        else ui_imvUnfollow.setImageResource(R.drawable.ic_social_onboarding_un);*/

        txv_follwing_num.setText(String.valueOf(_user.get_count_following())+" "+ getString(R.string.following_));
        txv_followers_num.setText(String.valueOf(_user.get_count_followers())+" "+ getString(R.string.followers_));



        /*if (_user.get_verify_facebook().equals("Y")){

            ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_red);
        } else ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_negative);

        if (_user.get_verify_google().equals("Y")){
            ui_imvGoogle.setImageResource(R.drawable.ic_social_google_red);
        } else ui_imvGoogle.setImageResource(R.drawable.ic_social_google_negative);*/
    }

    private void setFollow(final UserEntity entity){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FOLLOWFRIEND;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseSetFollow(response, entity);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.REQ_FRIENDID, String.valueOf(_user.get_idx()));

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseResponseSetFollow(String respones, UserEntity entity){

        closeProgress();

        try {

            JSONObject object = new JSONObject(respones);
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _user.addFollower(entity);
                showToast("Success Follow");

                UserProfile();
            } else showToast("Failed");

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    private void setUnFollow(final UserEntity entity){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UNFOLLOW;
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseSetUnfollow(response, entity);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(_user.get_idx()));
                    params.put(ReqConst.REQ_FRIENDID, String.valueOf(entity.get_idx()));

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseResponseSetUnfollow(String response, UserEntity entity){

        try {

            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code ==  ReqConst.CODE_SUCCESS){

                _user.removeFollowing(entity);
                UserProfile();
                showToast("Success Unfollowing");

            } else showToast("Failed");

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    private void gotoEditActivity() {

        Intent intent = new Intent(this ,EditProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.imv_unfollow:

                if (!_user.get_is_followed().equals("Y")) {
                    setFollow(_user);

                } else setUnFollow(_user);

                break;

            case R.id.imv_share:
                Share("", "");
                break;

            case R.id.imv_editProfofile:
                gotoEditActivity();
                break;
        }
    }

    private void actionBarResponsive() {

        int actionBarHeight = Utils.getActionBarHeightPixel(this);
        int tabHeight = Utils.getTabHeight(this);
        if (actionBarHeight > 0) {
            toolbar.getLayoutParams().height = actionBarHeight + tabHeight;
            toolbar.requestLayout();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
