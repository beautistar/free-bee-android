package com.freebee.android.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.commons.Constants;
import com.freebee.android.main.QuestionsActivity;
import com.freebee.android.model.InterestsEntity;
import com.nex3z.flowlayout.FlowLayout;

import java.util.ArrayList;


public class Question1Fragment extends Fragment implements View.OnClickListener{

    QuestionsActivity activity;
    View view;
    TextView txv_choose_que, txv_selected_num, txv_selected;

    ArrayList<InterestsEntity> userInterests = new ArrayList<>();

    ArrayList<String> strSelected = new ArrayList<>();

    //ArrayList<InterestsEntity> userNew = new ArrayList<>();

    int _count = 0, selected_num = 0;;

    FlowLayout _fly_container;

    public Question1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question1, container, false);

        for (int i = 0; i < Constants.strInterests.size(); i++){

            InterestsEntity interestsEntity = new InterestsEntity();

            interestsEntity.set_id(i);
            interestsEntity.set_item(Constants.strInterests.get(i));
            interestsEntity.set_status(false);

            userInterests.add(interestsEntity);

        }

        loadLayout();

        return view;
    }



    private void loadLayout() {

        txv_choose_que = (TextView)view.findViewById(R.id.txv_choose_que);
        txv_choose_que.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooseList();
            }
        });

        _fly_container = (FlowLayout) view.findViewById(R.id.lyt_content);
        txv_selected_num = (TextView)view.findViewById(R.id.txv_selected_num);
        txv_selected = (TextView)view.findViewById(R.id.txv_selected);

        if (strSelected.size() ==0) {
            txv_selected_num.setVisibility(View.GONE);
            txv_selected.setVisibility(View.GONE);
        }

    }


    public void update_userInterests(){
        for (int i = 0; i<userInterests.size(); i++){
            userInterests.get(i).set_status(false);
        }
    }

    private void showChooseList(){

        _count = 0;
        update_userInterests();

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_interests);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        ImageView button = (ImageView) dialog.findViewById(R.id.imv_check);

        ImageView imv_cancel = (ImageView)dialog.findViewById(R.id.imv_cancel);

        TextView txv_selected_num = (TextView)dialog.findViewById(R.id.txv_selected_num);
        txv_selected_num.setText(String.valueOf(_count));

        //Constants.strInterests.remove(0);

        for(int i = 0 ; i<Constants.strInterests.size() ; i++){

            final TextView textView = new TextView(activity);
            textView.setId(i);
            textView.setText(Constants.strInterests.get(i));
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
            textView.setTextColor(getResources().getColor(R.color.black));

            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if((userInterests.get(view.getId()).is_status())) {
                        _count--;
                        userInterests.get(view.getId()).set_status(false);
                        textView.setBackgroundResource(R.drawable.item_unselect_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }
                    else {
                        _count++;

                        userInterests.get(view.getId()).set_status(true);

                        textView.setBackgroundResource(R.drawable.signup_round);
                        textView.setTextColor(getResources().getColor(R.color.white));

                        //strSelected.add(userNew.get(view.getId()).get_item());
                    }

                    txv_selected_num.setText(String.valueOf(_count));
                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //process selected items

                showSelectedItem(userInterests);

                dialog.dismiss();
            }
        });

        imv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showSelectedItem(ArrayList<InterestsEntity> userNew){

        _fly_container.removeAllViews();

        ArrayList<String> selectedtext = new ArrayList<>();
        for(int i = 0 ; i<userNew.size() ; i++){
            if(userNew.get(i).is_status()){
                selectedtext.add(userNew.get(i).get_item());
            }
        }
        for(int i=0; i<selectedtext.size(); i++){
            if (selectedtext.size() >0)
            {
                txv_selected_num.setVisibility(View.VISIBLE);
                view.findViewById(R.id.txv_selected).setVisibility(View.VISIBLE);

                selected_num = selectedtext.size();
            }

            txv_selected_num.setText(String.valueOf(selected_num));


            final TextView textView = new TextView(activity);
            textView.setId(i);
            textView.setText(selectedtext.get(i));
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            textView.setBackgroundResource(R.drawable.signup_round);
            textView.setTextColor(getResources().getColor(R.color.white));

            _fly_container.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selected_num--;
                    _fly_container.removeView(textView);

                    txv_selected_num.setText(String.valueOf(selected_num));

                }
            });
        }

    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity)context;
    }


}
