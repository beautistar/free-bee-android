package com.freebee.android.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.adapter.TransactionListAdapter;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.TransactionModel;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.gson.JsonArray;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static com.freebee.android.commons.ReqConst.CODE_SUCCESS;
import static com.freebee.android.commons.ReqConst.RES_CODE;

public class MyBalanceActivity extends BaseActivity implements View.OnClickListener{

    ImageView imv_back;
    TextView txv_balance, txv_add, txv_refund;
    EditText edt_amount;
    CardInputWidget card_stripe;
    float amount = 0.f;
    String balance = "";

    ListView lst_trans;
    TransactionListAdapter adapter;
    ArrayList<TransactionModel> arrayTran;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_balance);

        balance = String.valueOf(Commons.g_user.getBalance());
        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyBalanceActivity.this, MainActivity.class));
                finish();
            }
        });

        edt_amount = (EditText)findViewById(R.id.edt_amount);
        card_stripe = (CardInputWidget)findViewById(R.id.card_input_widget);

        txv_balance = (TextView)findViewById(R.id.txv_balance);

        if (balance.length() != 0) txv_balance.setText(balance);

        lst_trans = (ListView)findViewById(R.id.lst_transaction);
        adapter = new TransactionListAdapter(this);
        lst_trans.setAdapter(adapter);

        txv_add = (TextView)findViewById(R.id.txv_add);
        txv_add.setOnClickListener(this);
        txv_refund = (TextView)findViewById(R.id.txv_refund);
        txv_refund.setOnClickListener(this);

        getBalance();
        getTransaction();
    }

    private void getBalance(){

        String url = ReqConst.SERVER_URL + "get_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    int resultCode = json.getInt(RES_CODE);
                    if (resultCode == CODE_SUCCESS){

                        Commons.g_user.setBalance(json.getString("balance"));
                        balance = Commons.g_user.getBalance();
                        txv_balance.setText(balance);
                        Log.d("user balance==>", Commons.g_user.getBalance());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void getTransaction(){

        arrayTran = new ArrayList<>();

        showProgress();

        String url = ReqConst.SERVER_URL + "get_transaction";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                closeProgress();
                try {
                    JSONObject json = new JSONObject(response);
                    int result_code = json.getInt(RES_CODE);

                    if (result_code ==  CODE_SUCCESS){

                        JSONArray jsonArray = (JSONArray) json.getJSONArray("transaction_list");


                        for (int i = 0; i < jsonArray.length(); i++){

                            JSONObject jsonTran = (JSONObject) jsonArray.get(i);

                            TransactionModel trans = new TransactionModel();

                            trans.setId(jsonTran.getInt("id"));
                            trans.setUserid(jsonTran.getInt("userid"));
                            trans.setTransaction_id(jsonTran.getString("transaction_id"));
                            trans.setAmount(jsonTran.getString("amount"));
                            trans.setType(jsonTran.getString("type"));
                            trans.setCreated_at(jsonTran.getString("created_at"));

                            arrayTran.add(trans);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                adapter.setTransaction(arrayTran);
                            }
                        });
                    }
                    else showToast(getString(R.string.error));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void chargePayment(float _amount){

        Card card = card_stripe.getCard();

        if (card == null) {
            // Do not continue token creation.
            Toast.makeText(this,getString(R.string.no_existCard), Toast.LENGTH_SHORT).show();

        }else {

            Stripe stripe = new Stripe(this, getString(R.string.stripe_public_key));
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {

                            showProgress();

                            String url = ReqConst.SERVER_URL + "charge_payment";

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    closeProgress();

                                    try {
                                        JSONObject json = new JSONObject(response);
                                        int resultCode = json.getInt(RES_CODE);
                                        if (resultCode == CODE_SUCCESS){

                                            String transaction_id = json.getString("id");
                                            addBalance(transaction_id, _amount);
                                        }
                                        else if (resultCode == 204){

                                            String message = json.getString("message");
                                            showToast(message);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    showAlertDialog(getString(R.string.error));
                                    closeProgress();
                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<>();

                                    try {

                                        params.put("stripe_token", token.getId());
                                        params.put("email", Commons.g_user.get_email());
                                        params.put("amount", String.valueOf(Math.round(_amount * 100)));

                                    } catch (Exception e) {

                                        closeProgress();
                                        showAlertDialog(getString(R.string.error));
                                    }
                                    return params;
                                }
                            };

                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
                        }
                        public void onError(Exception error) {
                            // Show localized error message
                            Toast.makeText(MyBalanceActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }
                    }
            );

        }
    }

    private void addBalance(String transaction_id,float _amount){

        String url = ReqConst.SERVER_URL + "add_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                closeProgress();
                try {
                    Log.d("add balance", response);
                    JSONObject json = new JSONObject(response);
                    int result_code = json.getInt(RES_CODE);

                    if (result_code ==  CODE_SUCCESS){

                        Commons.g_user.setBalance(String.valueOf(Float.parseFloat(Commons.g_user.getBalance()) + _amount));
                        balance = Commons.g_user.getBalance();
                        txv_balance.setText(balance);
                        getTransaction();
                    }
                    else showToast(getString(R.string.error));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));
                    params.put("transaction_id", transaction_id);
                    params.put("amount", String.valueOf(_amount));
                    params.put("balance", String.valueOf(balance));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void refundPayment(float _amount){

        showProgress();

        String transaction_id = getTransId();

        String url = ReqConst.SERVER_URL + "refund_payment";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    int result_code = json.getInt(RES_CODE);

                    if (result_code ==  CODE_SUCCESS){
                        removeBalance(transaction_id, _amount);
                    }
                    else {
                        closeProgress();
                        if (result_code == 204)
                            showToast(json.getString("message"));

                    }

                } catch (JSONException e) {
                    closeProgress();
                    showToast(getString(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("transaction_id", transaction_id);
                    params.put("amount", String.valueOf(Math.round(_amount) * 100));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void removeBalance(String transaction_id, float amount){

        String url = ReqConst.SERVER_URL + "remove_balance";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                closeProgress();
                try {
                    JSONObject json = new JSONObject(response);
                    int result_code = json.getInt(RES_CODE);

                    if (result_code ==  CODE_SUCCESS){

                        Commons.g_user.setBalance(String.valueOf(Float.parseFloat(Commons.g_user.getBalance()) - amount));
                        balance = Commons.g_user.getBalance();
                        txv_balance.setText(balance);

                        getTransaction();
                    }
                    else showToast(getString(R.string.error));

                } catch (JSONException e) {
                    closeProgress();
                    e.printStackTrace();
                    showToast(getString(R.string.error));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));
                    params.put("transaction_id", transaction_id);
                    params.put("amount", String.valueOf(Math.round(amount)));
                    params.put("balance", balance);

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){

            case R.id.txv_add:

                if (edt_amount.getText().toString().length() == 0){

                    showToast("Enter amount");
                    return;

                }else {

                    amount = Float.parseFloat(edt_amount.getText().toString().trim());

                    chargePayment(amount);

                    /*if ((Float.parseFloat(balance) +  amount) > 20)

                    else
                        showToast("You need to charge more than " + "$" + (20 - Float.parseFloat(balance)));*/

                }
                break;

            case R.id.txv_refund:

                if (edt_amount.getText().toString().length() == 0){

                    showToast("Enter amount");
                    return;
                }
                else if(edt_amount.getText().toString().length() > 0) {

                    amount = Float.parseFloat(edt_amount.getText().toString().trim());

                    if (Float.parseFloat(Commons.g_user.getBalance()) < amount){

                        showToast("You can't refund more than $" + Commons.g_user.getBalance());
                        return;
                    }

                    else {

                        if (getTransId().length() > 0)
                            refundPayment(amount);
                    }

                }

                break;
        }
    }

    private String getTransId(){

        String tranID = "";

        if (arrayTran.size() !=0) {

            ArrayList<Float> amountArray = new ArrayList<>();
            ArrayList<String> idArray = new ArrayList<>();

            for (int i = 0; i <  arrayTran.size(); i++){

                if (idArray.contains(arrayTran.get(i).getTransaction_id())){

                    Float curamount = Float.parseFloat(arrayTran.get(i).getAmount());
                    int key = idArray.indexOf(arrayTran.get(i).getTransaction_id());

                    amountArray.set(key, amountArray.get(key) + curamount);
                }
                else {

                    amountArray.add(Float.parseFloat(arrayTran.get(i).getAmount()));
                    idArray.add(arrayTran.get(i).getTransaction_id());
                }
            }


            Float maxAmount = Collections.max(amountArray);

            if (amount > maxAmount){

                showToast("You cannot refund because amount is great than max amount.");
                return "";
            }
            else {

                tranID = idArray.get(amountArray.indexOf(maxAmount));
            }

            Log.d("tranID", tranID);

        }
        else showToast("You cannot refund because on no balance.");

        return tranID;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MyBalanceActivity.this, MainActivity.class));
        finish();
    }
}
