package com.freebee.android.main;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddAddressActivity extends BaseActivity {

    ImageView imv_back, imv_check;
    EditText edt_full_name, edt_address, edt_district, edt_city, edt_zipcode, edt_state;
    String full_name = "", address = "", district = "", city = "", zipcode = "", state = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddAddressActivity.this, GetAddressActivity.class));
                finish();
            }
        });

        imv_check = (ImageView)findViewById(R.id.imv_check);
        imv_check.setColorFilter(getResources().getColor(R.color.white));
        imv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValid())
                    addShipAddress();
            }
        });

        edt_full_name = (EditText)findViewById(R.id.edt_full_name);
        edt_address = (EditText)findViewById(R.id.edt_address);
        edt_district = (EditText)findViewById(R.id.edt_distric);
        edt_city = (EditText)findViewById(R.id.edt_city);
        edt_zipcode = (EditText)findViewById(R.id.edt_zipcode);
        edt_state = (EditText)findViewById(R.id.edt_state);
    }

    private void addShipAddress(){


        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADD_ADDRESS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseAddAddress(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put("name", full_name);
                    params.put("address", address);
                    /*params.put("district", district);*/
                    params.put("city", city);
                    params.put("state", state);
                    params.put("zipcode", zipcode);

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseAddAddress(String response){


        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                startActivity(new Intent(AddAddressActivity.this, GetAddressActivity.class));
                finish();
            }
            else {
                showToast(getString(R.string.error));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private boolean checkValid(){

        full_name = edt_full_name.getText().toString();
        address = edt_address.getText().toString();
        district = edt_district.getText().toString();
        city = edt_city.getText().toString();
        zipcode = edt_zipcode.getText().toString();
        state = edt_state.getText().toString();

        if (full_name.length() == 0){

            showToast("Enter full name");
            return false;
        }
        else if (address.length() == 0){

            showToast("Enter address");
            return false;
        }
        /*else if (district.length() == 0){
            showToast("Enter district");
            return false;
        }*/
        else if (city.length() == 0){
            showToast("Enter city");
            return false;
        }
        else if (zipcode.length() == 0){

            showToast("Enter zipcode");
            return false;
        }
        else if (state.length() == 0){
            showToast("Enter state");
            return false;
        }

        return true;
    }


}
