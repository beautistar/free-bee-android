package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.model.CityModel;

import java.util.ArrayList;

public class CitiesSelectListAdapter extends BaseAdapter {

    //private String[] data;
    ArrayList<String> data = new ArrayList<>();
    private LayoutInflater inflater;

    public CitiesSelectListAdapter(Context context, /*String[] array*/ ArrayList<String> array){

        data = array;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_choose_cities, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.city = (TextView) convertView.findViewById(R.id.item_name);

            convertView.setTag(viewHolder);
        }
        else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.city.setText(data.get(position));

        return convertView;
    }
    private class ViewHolder{
        TextView city;
    }

}
