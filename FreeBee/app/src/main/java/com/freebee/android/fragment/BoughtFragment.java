package com.freebee.android.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.freebee.android.R;


import com.freebee.android.adapter.RecyclerViewBoughtAdapter;
import com.freebee.android.base.BaseFragment;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;

import java.util.ArrayList;

/**
 * Created by GoldRain on 10/8/2016.
 */

@SuppressLint("ValidFragment")
public class BoughtFragment extends BaseFragment {

    UserProfileActivity _activity ;
    ArrayList<ProductEntity> _bought = new ArrayList<>();

    RecyclerViewBoughtAdapter _adapter;
    RecyclerView ui_gdv;

    public BoughtFragment (UserProfileActivity context, ArrayList<ProductEntity> bought){

        this._activity = context ;
        _bought.addAll(bought);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d("=========bought=====", String.valueOf(_bought));

        View view = inflater.inflate(R.layout.fragment_bought , container, false);

        ui_gdv = (RecyclerView) view.findViewById(R.id.gdv_bought);
        ui_gdv.setLayoutManager(new GridLayoutManager(_activity, 2));
        ui_gdv.setHasFixedSize(true);
        ui_gdv.setItemAnimator(new DefaultItemAnimator());
        _adapter = new RecyclerViewBoughtAdapter(_activity, _bought);

        ui_gdv.setAdapter(_adapter);


        return view ;
    }
}
