package com.freebee.android.adapter;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.freebee.android.R;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;


import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/6/2017.
 */

public class RecyclerViewLikeAdapter extends RecyclerView.Adapter<RecyclerViewLikeAdapter.LikesHolder> {

    UserProfileActivity _activity;
    ArrayList<ProductEntity> _allLikes = new ArrayList<>();

    public RecyclerViewLikeAdapter(UserProfileActivity activity, ArrayList<ProductEntity> likes){

        this._activity = activity;
        _allLikes = likes;
    }

    @Override
    public LikesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_likes_gdv, parent, false);

        return new LikesHolder(view);
    }

    @Override
    public void onBindViewHolder(LikesHolder holder, int position) {

        final ProductEntity like = _allLikes.get(position);

        holder.txvName.setText(like.get_name());

        Glide.with(_activity).load(like.get_imagUrl()).placeholder(R.drawable.place_holder_pattern).into(holder.imvImage);

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               Intent intent = new Intent(_activity, ProductActivity.class);
               intent.putExtra(Constants.PRODUCTID, like.get_id());
               _activity.startActivity(intent);
               _activity.finish();
           }
       });

    }


    @Override
    public int getItemCount() {
        return _allLikes.size();
    }

    public class LikesHolder extends RecyclerView.ViewHolder{

        public RoundedImageView imvImage;
        public TextView txvName;

        public LikesHolder(View view) {
            super(view);

            txvName = (TextView) view.findViewById(R.id.txv_name);
            imvImage = (RoundedImageView)view.findViewById(R.id.imv_product);
        }
    }
}
