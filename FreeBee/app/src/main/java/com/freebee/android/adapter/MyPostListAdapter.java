package com.freebee.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.main.MyPostListActivity;
import com.freebee.android.main.PostActivity;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.model.ProductEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyPostListAdapter extends BaseAdapter {

    MyPostListActivity activity;
    ArrayList<ProductEntity> arrayProducts = new ArrayList<>();

    public MyPostListAdapter(MyPostListActivity _activity, ArrayList<ProductEntity> entities){

        this.activity = _activity;
        this.arrayProducts = entities;
    }

    @Override
    public int getCount() {
        return arrayProducts.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyPostListHolder holder;

        if (convertView == null){

            holder = new MyPostListHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_my_post_list, parent, false);

            holder.imv_product = (ImageView)convertView.findViewById(R.id.imv_product);
            holder.imv_edit = (ImageView)convertView.findViewById(R.id.imv_edit);
            holder.imv_delete = (ImageView)convertView.findViewById(R.id.imv_delete);
            holder.txv_product_name = (TextView)convertView.findViewById(R.id.txv_product_name);

            convertView.setTag(holder);
        }else {
            holder = (MyPostListHolder)convertView.getTag();
        }

        ProductEntity product = (ProductEntity)arrayProducts.get(position);
        if (product.get_imagUrl().length() > 0)
            Glide.with(activity).load(product.get_imagUrl()).into(holder.imv_product);
        holder.txv_product_name.setText(product.get_name());


        holder.imv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.showProgress();

                String url = ReqConst.SERVER_URL + ReqConst.REQ_DELETE_PRODUCT;
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        activity.closeProgress();

                        try {
                            JSONObject object = new JSONObject(response);

                            int result_code = object.getInt(ReqConst.RES_CODE);

                            if (result_code == ReqConst.CODE_SUCCESS){

                                arrayProducts.remove(product);
                                Commons.g_user.set_listProducts(arrayProducts);
                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activity.closeProgress();
                        activity.showAlertDialog(activity.getString(R.string.error));
                    }

                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();

                        try {
                            params.put(ReqConst.RES_PRODUCT_ID, String.valueOf(product.get_id()));

                        } catch (Exception e) {

                            activity.closeProgress();
                            activity.showAlertDialog(activity.getString(R.string.error));
                        }

                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
            }
        });

        holder.imv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.PRODUCT = product;
                Intent intent = new Intent(activity, PostActivity.class);
                intent.putExtra(Constants.PRODUCT_EDIT, true);
                activity.startActivity(intent);
                activity.finish();
            }
        });

        return convertView;
    }

    public class MyPostListHolder{

        ImageView imv_product, imv_edit, imv_delete;
        TextView txv_product_name;
    }
}
