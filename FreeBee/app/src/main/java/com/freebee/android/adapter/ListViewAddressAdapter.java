package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freebee.android.R;
import com.freebee.android.commons.Commons;
import com.freebee.android.main.GetAddressActivity;
import com.freebee.android.model.ShipAddressModel;

import java.util.ArrayList;

public class ListViewAddressAdapter extends BaseAdapter {

    ArrayList<ShipAddressModel> shipAddress = new ArrayList<>();

    Context context;

    public ListViewAddressAdapter(Context context){

        this.context = context;
    }

    public void setAddress(ArrayList<ShipAddressModel> models){

        shipAddress = models; }

    @Override
    public int getCount() {
        return shipAddress.size();
    }

    @Override
    public Object getItem(int position) {
        return shipAddress.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_ship_address, parent, false);

            viewHolder.name = (TextView)convertView.findViewById(R.id.txv_name);
            viewHolder.address = (TextView)convertView.findViewById(R.id.txv_address);
            viewHolder.city = (TextView)convertView.findViewById(R.id.txv_city);
            viewHolder.state = (TextView)convertView.findViewById(R.id.txv_state);
            viewHolder.imv_checked = (ImageView)convertView.findViewById(R.id.imv_checked);

            convertView.setTag(viewHolder);
        }
        else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        ShipAddressModel shipAdd = (ShipAddressModel) shipAddress.get(position);
        viewHolder.name.setText(shipAdd.getName());
        viewHolder.address.setText(shipAdd.getAddress());
        viewHolder.city.setText(shipAdd.getCity() + ", " + shipAdd.getZipcode());
        viewHolder.state.setText(shipAdd.getState());
        viewHolder.imv_checked.setSelected(shipAdd.isChecked());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < shipAddress.size(); i++){

                    if (i == position){

                        viewHolder.imv_checked.setSelected(true);
                        shipAdd.setChecked(true);

                        Commons.shippingAddress = shipAdd;

                    } else {

                        shipAddress.get(i).setChecked(false);

                    }
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public class ViewHolder{

        TextView name, address, city, state;
        ImageView imv_checked;
    }

}
