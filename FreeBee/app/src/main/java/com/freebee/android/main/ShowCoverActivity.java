package com.freebee.android.main;

import androidx.viewpager.widget.ViewPager;
import com.freebee.android.R;
import android.os.Bundle;
import android.view.View;


import com.freebee.android.adapter.ShowCoverViewPagerAdapter;
import com.freebee.android.base.CommonActivity;

import java.util.ArrayList;

public class ShowCoverActivity extends CommonActivity implements View.OnClickListener {

    ArrayList<Integer> imgs = new ArrayList<>();

    ShowCoverViewPagerAdapter adapter;
    ViewPager ui_vp_cover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cover);

        loadData();
        loadlayout();
    }

    private void loadData() {

        imgs.add(R.drawable.ic_ps4);
        imgs.add(R.drawable.other_empty_pic_2);
    }

    private void loadlayout() {

        adapter = new ShowCoverViewPagerAdapter(this, imgs );
        ui_vp_cover = (ViewPager)findViewById(R.id.vp_cover);
        ui_vp_cover.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

    }
}
