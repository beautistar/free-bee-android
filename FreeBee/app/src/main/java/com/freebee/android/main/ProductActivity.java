package com.freebee.android.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import com.freebee.android.adapter.ProductViewPagerAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.utils.RadiusImageView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductActivity extends CommonActivity implements View.OnClickListener , Serializable/*,OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks */{

    ProductViewPagerAdapter adapter;

    ProductEntity _product = new ProductEntity();

    ArrayList<String> _productImages = new ArrayList<>();
    ViewPager ui_vp;

    ImageView ui_imvFacebook, ui_imvGoogle, ui_imvPhone , ui_imvLike, ui_imvSold , ui_imvback, ui_share, ui_sms, imv_bundle ;
    RadiusImageView imv_photo;
    //FrameLayout ui_fytMap ;

    GoogleMap mMap;
    GoogleApiClient mGoogleApiClient ;
    Location mCurrentLocation;
    //ImageView ui_imvMap, ui_imvScope;

    TextView txv_name, txv_productState, txv_productName, txv_description, /*txv_price,*/ txv_seeprofile, txv_qty, txv_claimed, txv_type;

    Double X = 0.0;
    Double Y = 0.0 ;

    int productId ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        productId = (int) getIntent().getSerializableExtra(Constants.PRODUCTID);
        loadLayout();

    }

    /*
    private void GoogleMapLoad() {

        X = getIntent().getDoubleExtra(Constants.CURRENT_MYLATITUDE_X, 42.873807);
        Y = getIntent().getDoubleExtra(Constants.CURRENT_MYLONGITUDE_Y , 129.535743);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }*/

    private void loadLayout() {

        ui_imvFacebook = (ImageView)findViewById(R.id.imv_facebook);
        ui_imvGoogle =  (ImageView)findViewById(R.id.imv_google);
        ui_imvPhone = (ImageView)findViewById(R.id.imv_phone);
        ui_imvPhone.setOnClickListener(this);


        ui_imvback = (ImageView)findViewById(R.id.imv_back);
        ui_imvback.setOnClickListener(this);

        ui_imvLike = (ImageView)findViewById(R.id.imv_like);
        ui_imvLike.setOnClickListener(this);

        ui_imvSold = (ImageView)findViewById(R.id.imv_sold);
        ui_imvSold.setOnClickListener(this);

        ui_share = (ImageView)findViewById(R.id.imv_share);
        ui_share.setOnClickListener(this);

        ui_sms = (ImageView) findViewById(R.id.imv_sms);
        ui_sms.setOnClickListener(this);

        imv_bundle = (ImageView)findViewById(R.id.imv_bundle);
        imv_bundle.setOnClickListener(this);

        txv_name = (TextView) findViewById(R.id.txv_name);

        txv_seeprofile = (TextView) findViewById(R.id.txv_seeprofile);
        txv_seeprofile.setOnClickListener(this);

        txv_productState = (TextView)findViewById(R.id.txv_productState);
        txv_productName = (TextView)findViewById(R.id.txv_productName);
        txv_description = (TextView)findViewById(R.id.txv_description);
        //txv_price = (TextView)findViewById(R.id.txv_price);

        txv_qty = (TextView)findViewById(R.id.txv_qty);
        txv_claimed = (TextView)findViewById(R.id.txv_claimed);
        txv_type = (TextView)findViewById(R.id.txv_type);

        imv_photo = (RadiusImageView) findViewById(R.id.imv_photo);


        //ui_imvScope = (ImageView)findViewById(R.id.imv_scope);
        //ui_imvScope.setOnClickListener(this);

        //ui_fytMap = (FrameLayout)findViewById(R.id.flt_map);
        //ui_fytMap.setOnClickListener(this);

        adapter = new ProductViewPagerAdapter(this);

        ui_vp = (ViewPager)findViewById(R.id.vp);
        ui_vp.setAdapter(adapter);

        productDetails();

    }
    private void productDetails(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_PRODUCTDETAIL;

        showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseProductDetails(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                showAlertDialog(getString(R.string.error));
                closeProgress();
            }

        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCT_ID, String.valueOf(productId));

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseProductDetails(String respons) {


        try {

            closeProgress();

            JSONObject object = new JSONObject(respons);

            //Log.d("======respons=======", respons);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {

                JSONObject jsonProduct = object.getJSONObject(ReqConst.RES_PRODUCTDETAIL);

                ProductEntity product = new ProductEntity();

                product.set_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_ID));
                product.set_name(jsonProduct.getString(ReqConst.RES_PRODUCT_NAME));
                product.set_description(jsonProduct.getString(ReqConst.RES_PRODUCT_DESCRIPTION));
                //product.set_price(jsonProduct.getInt(ReqConst.RES_PRODUCTPRICE));
                product.set_category(jsonProduct.getString(ReqConst.RES_CATEGORY));
                product.set_latitude(jsonProduct.getDouble(ReqConst.RES_PRODUCTLATITUDE));
                product.set_longtitude(jsonProduct.getDouble(ReqConst.RES_PRODUCTLONGTITUDE));
                product.set_quantity_available(jsonProduct.getInt(ReqConst.RES_QUANTITY_AVAILABLE));
                product.set_quantity_per_user(jsonProduct.getInt(ReqConst.RES_QUANTITY_USER));
                product.set_age_max(jsonProduct.getInt(ReqConst.RES_AGE_MAX));
                product.set_age_min(jsonProduct.getInt(ReqConst.RES_AGE_MIN));
                product.set_gender(jsonProduct.getString(ReqConst.RES_GENDER));
                product.set_hobby(jsonProduct.getString(ReqConst.RES_HOBBY));
                product.set_industry(jsonProduct.getString(ReqConst.RES_INDUSTRY));
                product.set_occupation(jsonProduct.getString(ReqConst.RES_OCCUPATION));
                product.set_zipcode(jsonProduct.getString(ReqConst.RES_ZIPCODE));
                product.set_city(jsonProduct.getString(ReqConst.RES_CITY));
                product.set_state(jsonProduct.getString(ReqConst.RES_STATE));
                product.set_country(jsonProduct.getString(ReqConst.RES_COUNTRY));
                product.set_like_product(jsonProduct.getString(ReqConst.RES_ISLIKED));
                product.set_cart_product(jsonProduct.getString(ReqConst.RES_CART_PRODUCT));
                product.set_ownerName(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_NAME));
                product.set_owner_imageUrl(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_IMAGE));
                product.set_owner_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_OWNER_ID));
                product.set_is_sold(jsonProduct.getString(ReqConst.RES_IS_SOLD));
                product.set_verify_facebook(jsonProduct.getString(ReqConst.RES_VERIFYFACEBOOK));
                product.set_owner_phone(jsonProduct.getString(ReqConst.RES_PRODUCT_OWNER_PHONE));
                product.set_verify_google(jsonProduct.getString(ReqConst.RES_VERIFYGOOGLE));
                product.set_sold_count(jsonProduct.getInt(ReqConst.RES_SOLD_COUNT));
                product.setType(jsonProduct.getString(ReqConst.REQ_TYPE));

                JSONArray productImages = jsonProduct.getJSONArray(ReqConst.RES_PRODUCT_IMAGE);

                _productImages = new ArrayList<>();

                for (int i = 0; i < productImages.length();i++){

                    String jsonImage = (String) productImages.get(i);
                    _productImages.add(jsonImage);
                }
                product.set_productImages(_productImages);

                /*Log.d("==========photo=======", product.get_imagUrl());*/

                _product = product;

                showProductDetails();

                //GoogleMapLoad();

            } else {

                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProductDetails() {

        //txv_price.setText(String.valueOf(_product.get_price())+" " + "USD" );

        if (_product.get_ownerName().toString().length() != 0) txv_name.setText(_product.get_ownerName());
        else txv_name.setText("name");

        Glide.with(this).load(_product.get_owner_imageUrl()).placeholder(R.drawable.ic_avatar).into(imv_photo);
        txv_description.setText(_product.get_description());

        if (_product.get_is_sold().equals("Y")){

            txv_productState.setText("Premium");
            ui_imvSold.setImageResource(R.drawable.ic_premium_member);
            txv_productState.setTextColor(getResources().getColor(R.color.splash_bg
            ));

        }else {
            txv_productState.setText("Basic");
            ui_imvSold.setImageResource(R.drawable.ic_basic_member);
            txv_productState.setTextColor(getResources().getColor(R.color.black));
        }

        if (_product.get_like_product().equals("L")){

            ui_imvLike.setImageResource(R.drawable.ic_product_details_favorite_red);

        } else {

            ui_imvLike.setImageResource(R.drawable.ic_product_details_favorite);
        }

        if (_product.get_cart_product().equals("Y")){
            imv_bundle.setImageResource(R.drawable.ic_cart_fill);
        } else imv_bundle.setImageResource(R.drawable.ic_cart);

        txv_productName.setText(_product.get_name());
        txv_qty.setText(String.valueOf(_product.get_quantity_per_user() - _product.get_sold_count()));
        txv_claimed.setText(String.valueOf(_product.get_sold_count()));
        txv_type.setText(_product.getType());

        X = _product.get_latitude();
        Y = _product.get_longtitude();

        //verified user

       /* if (_product.get_verify_facebook().equals("Y")){

            ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_red);
        } else ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_negative);

        if (_product.get_verify_google().equals("Y")){
            ui_imvGoogle.setImageResource(R.drawable.ic_social_google_red);

        } else ui_imvGoogle.setImageResource(R.drawable.ic_social_google_negative);*/

        adapter.setData(_productImages);
        adapter.notifyDataSetChanged();

    }

    private void likeProduct(){

        String _like = "";
        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LIKEPRODUCT;

         _like = "";
        if (_product.get_like_product().equals("L")){
            _like = "U";
        } else _like = "L";

        String final_like = _like;

        Log.d("===response=Like=", url + " " +  final_like);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLikeProduct(response);
                Log.d("===response=Like=", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCTID, String.valueOf(_product.get_id()));
                    params.put(ReqConst.REQ_TYPE, final_like);

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseLikeProduct(String response){

        closeProgress();

        Log.d("===response=Like=", response);

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                productDetails();
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }
    }

    private void soldProduct(){

        String IS_SOLD = "";

        if (_product.get_is_sold().equals("N")){
            IS_SOLD = "Y";
        } else IS_SOLD = "N";

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SOLDPRODUCT;
        String finalIS_SOLD = IS_SOLD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSoldProduct(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCT_ID, String.valueOf(_product.get_id()));
                    params.put(ReqConst.REQ_TYPE, finalIS_SOLD);

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseSoldProduct(String response){

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            Log.d("reuslt+++++sold====>", response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                productDetails();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng latLng=new LatLng(X,Y);

        //  mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(X,Y),10.0f));
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        mMap.addMarker(new MarkerOptions().position(latLng).title("You are here!").snippet("Consider yourself located"));


        //mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled( false );

    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
       // mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {

        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    */


    private void addCartProduct(int _count){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ADD_CART_PRODUCT;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseAddCart(response, _count);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.REQ_COUNT, String.valueOf(_count));
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCTID, String.valueOf(_product.get_id()));

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseAddCart(String response, int _count){

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _product.set_cart_product("Y");
                imv_bundle.setImageResource(R.drawable.ic_cart_fill);
                txv_claimed.setText(String.valueOf(_product.get_sold_count() + _count));
                _product.set_sold_count(_product.get_sold_count() + _count);
                txv_qty.setText(String.valueOf(_product.get_quantity_per_user() - _product.get_sold_count()));
                showToast("Added this product to Cart");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.imv_facebook:

                break;

            case R.id.imv_share:
                gotoShare();
                break;
            case R.id.imv_sms:

                if (Commons.g_user.get_idx() != _product.get_owner_id()) {
                    gotoSMS();
                } else {
                    showAlertDialog("It is your own product");
                }
                break;

            case R.id.txv_seeprofile:

                if (Commons.g_user.get_idx() == _product.get_owner_id()){
                    showToast("It's just your product!");
                    return;
                }

                Intent intent1 = new Intent(this, UserProfileActivity.class);
                intent1.putExtra(Constants.USERID, _product.get_owner_id());
                startActivity(intent1);
                break;

            case R.id.imv_scope:

                Intent intent = new Intent(this , ProductMapActivity.class);
                intent.putExtra(Constants.CURRENT_MYLATITUDE_X, X);
                intent.putExtra(Constants.CURRENT_MYLONGITUDE_Y, Y);
                startActivity(intent);
                //finish();
                break;

            case R.id.imv_like:

                if (_product.get_owner_id() != Commons.g_user.get_idx())
                    likeProduct();

                break;

            case R.id.imv_sold:

                if (Commons.g_user.get_idx() == _product.get_owner_id())
                soldProduct();

                break;

            case R.id.imv_phone:

                if (Commons.g_user.get_idx() == _product.get_owner_id()) {

                    showAlertDialog("It is your product");
                } else {

                    callPhone();
                }

                break;

            case R.id.imv_bundle:

                if (_product.get_cart_product().equals("Y")){
                    showAlertDialog("You already added this product.");
                    return;
                }
                else selectQTY();
                break;
        }

    }

    private void selectQTY(){

        final int limit_product = _product.get_quantity_per_user()/* - _product.get_sold_count()*/;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_cart_qty);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_qty_limit = (TextView)dialog.findViewById(R.id.txv_qty_limit);
        EditText edt_claim = (EditText)dialog.findViewById(R.id.edt_claim);
        Button btn_add_cart = (Button)dialog.findViewById(R.id.btn_add_cart);
        txv_qty_limit.setText("You can claim only " + limit_product +" quantities.");

        btn_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.parseInt(edt_claim.getText().toString()) > limit_product)

                    showToast("Over quantities to add to cart");

                else{
                    int _count = Integer.parseInt(edt_claim.getText().toString());
                    addCartProduct(_count);

                }

                dialog.dismiss();
            }

        });

        dialog.show();
    }

    void gotoShare() {

        try{
            //making bitmap image
            URL url = new URL(_productImages.get(0).toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);

//            View mView = findViewById(R.id.imv_content);
////            mView = mView.getRootView(); //full screen image
//            mView.setDrawingCacheEnabled(true);
//            Bitmap bitmap = mView.getDrawingCache();

            Intent intent = new Intent(Intent.ACTION_SEND);

            intent.setType("image/*");
            Bitmap bitmap1 = bitmap;
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap1, "null", null);
            Uri imageUri = Uri.parse(path);
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.putExtra(Intent.EXTRA_SUBJECT, "FreeBee product");
            intent.putExtra(Intent.EXTRA_TEXT, "Name : " + _product.get_name() + "\n\n" + "Seller Phone : " +  _product.get_owner_phone() + "\n\n" + "Price : " +  _product.get_price());
            startActivity(intent);

        }catch (android.content.ActivityNotFoundException e) {
            e.printStackTrace();
        }catch (java.lang.NullPointerException e){
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callPhone() {

        Uri number = Uri.parse("tel:"+ _product.get_owner_phone());
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    private void gotoSMS() {

        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+_product.get_owner_phone()));
        intentsms.putExtra("sms_body", "text of massege");
        startActivity(intentsms);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProductActivity.this, MainActivity.class));
        finish();
    }
}