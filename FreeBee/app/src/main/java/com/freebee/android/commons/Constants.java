package com.freebee.android.commons;


import com.freebee.android.model.FaqEntity;
import com.freebee.android.model.ProductEntity;

import java.util.ArrayList;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int SPLASH_TIME = 1000;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final String USER = "user";

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;



    /*public static final String[] CATEGORIES1 = {String.valueOf(R.string.electronics),String.valueOf(R.string.fashion_accessories),String.valueOf(R.string.sport),
            String.valueOf(R.string.home_garden) ,String.valueOf(R.string.film_media),String.valueOf(R.string.baby_child),String.valueOf(R.string.gamming),String.valueOf(R.string.vehicles),String.valueOf(R.string.other)};
*/
    //public static final String[] CATEGORIES = {"Electronics","Fashion and Accessories","Sport &amp; Hobbies","Home and Garden","Film, Media and books","Baby and Child","Gamming","Vehicles","Other"};

    //public static final String[] CATEGORIES = res.getStringArray(R.array.Categories);

    public static final String PRODUCTID = "productid";

    public static final String KEY_LOGOUT = "logout";
    public static final String MYPROFILE_POSITION = "myprofile_position";
    public static final String CURRENT_MYLATITUDE_X = "current_mylocation_x";
    public static final String CURRENT_MYLONGITUDE_Y = "current_mylocation_y";
    public static final String IMAGES = "images";
    public static final String POSITION = "position";
    public static final String FILTER_RESULT = "search_result";
    public static final String USERID = "userid";
    public static final String PRODUCT_EDIT = "product_edit";
    public static ProductEntity PRODUCT = new ProductEntity();
    public static final String PREMIUM = "premium";
    public static final String REGULAR = "regular";

    public static String TOKEN = "token";


    public static final int MY_PEQUEST_CODE = 104 ;

    public static final String FOLLOWING = "following";
    public static final int FOLLOWING_CODE = 200 ;

    public static int current_page = 1; //current post page :
    public static int list_position = 0; //current post item position:

    public static ArrayList<String> strInterests = new ArrayList<>();
    public static ArrayList<String> consFaqTitle = new ArrayList<>();
    public static ArrayList<String> consFaqContent = new ArrayList<>();
    public static String myBalance = "";

}
