package com.freebee.android.main;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.freebee.android.R;

import com.freebee.android.base.CommonActivity;

import java.io.InputStream;

public class TermsAndConditionActivity extends CommonActivity implements View.OnClickListener {

    TextView ui_txvTerms, txv_helpTitle;
    ImageView ui_imvBack;

    String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);

        title = getIntent().getStringExtra("Title");

        loadLayout();

    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        txv_helpTitle = (TextView) findViewById(R.id.txv_helpTitle);
        ui_txvTerms = (TextView)findViewById(R.id.txv_terms_of_use);

        String result;
        try {
            Resources res = getResources();
            InputStream in_s = null;
            if (title.equals("FAQs")) {

                in_s = res.openRawResource(R.raw.faqs);
                txv_helpTitle.setText("FAQs");
            } else {

                in_s = res.openRawResource(R.raw.terms);
                txv_helpTitle.setText("Terms and Conditions");
            }


            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        ui_txvTerms.setText(result.toString());


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back:

                finish();
                overridePendingTransition(R.anim.right_in , R.anim.left_out);
                break;
        }

    }
}
