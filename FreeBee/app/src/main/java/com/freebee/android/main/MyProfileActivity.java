package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.freebee.android.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.freebee.android.FreeBeeApplication;

import com.freebee.android.adapter.ViewPagerAdapter_MyProfile;
import com.freebee.android.base.CommonActivity;

import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.UserEntity;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyProfileActivity extends CommonActivity implements View.OnClickListener{

    ViewPagerAdapter_MyProfile adapter;

    TabLayout tabLayout ;
    ViewPager viewPager ;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    NestedScrollView ui_nested_scrollview;
    ImageView ui_imv_cover ,ui_imvBack, ui_imvShare , ui_imvEdit,imv_following,imv_followers ;
    TextView txv_name, txv_follwing_num,txv_followers_num ,txv_description;

    int position = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        position = getIntent().getIntExtra(Constants.MYPROFILE_POSITION, 0);

        loadLayout();
    }

    private void loadLayout() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
//        collapsingToolbarLayout.setTitle("MyProfile");

        FragmentManager manager = getSupportFragmentManager();
        adapter =new  ViewPagerAdapter_MyProfile(this, manager);
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        //viewPager.setVerticalScrollbarPosition(position);
        viewPager.setCurrentItem(position);

        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        ui_nested_scrollview = (NestedScrollView)findViewById(R.id.nested_scrollview);
        ui_nested_scrollview.setFillViewport(true);

        ui_imv_cover = (ImageView)findViewById(R.id.imv_cover);
        ui_imv_cover.setOnClickListener(this);
//
        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvShare = (ImageView)findViewById(R.id.imv_share);
        ui_imvShare.setOnClickListener(this);

        ui_imvEdit = (ImageView)findViewById(R.id.imv_edit);
        ui_imvEdit.setOnClickListener(this);

        imv_following = (ImageView)findViewById(R.id.imv_following);
        imv_following.setOnClickListener(this);

        imv_followers = (ImageView)findViewById(R.id.imv_followers);
        imv_followers.setOnClickListener(this);

        txv_name = (TextView)findViewById(R.id.txv_name);
        txv_follwing_num = (TextView)findViewById(R.id.txv_follwing_num);
        txv_followers_num = (TextView)findViewById(R.id.txv_followers_num);
        txv_description = (TextView)findViewById(R.id.txv_description);

        showMyProfile();
    }

    private void MyProfile(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_USERPROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseMyProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseMyProfile(String response){

        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(response);

            JSONObject myProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity user = new UserEntity();

                user.set_idx(myProfile.getInt(ReqConst.RES_USERID));
                user.set_name(myProfile.getString(ReqConst.RES_USERNAME));
                user.set_imageUrl(myProfile.getString(ReqConst.RES_USERIMAGE));
                user.set_count_followers(myProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                user.set_count_following(myProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                user.set_isVerified(myProfile.getInt(ReqConst.RES_ISVERIFIED));

                user.set_phone(myProfile.getString(ReqConst.REQ_PHONE));
                user.setAddress(myProfile.getString("address"));
                user.setCity(myProfile.getString("city"));
                user.setState(myProfile.getString("state"));
                user.setZip_code(myProfile.getString("zip_code"));
                user.setAge(myProfile.getInt("age"));
                user.setGender(myProfile.getString("gender"));

                Commons.g_user = user ;

                showMyProfile();
            }

        } catch (JSONException e) {
            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void showMyProfile(){

        txv_name.setText(Commons.g_user.get_name());
        Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_menu_avatar).into(ui_imv_cover);

        txv_follwing_num.setText(String.valueOf(Commons.g_user.get_count_following())+" "+"Following");
        txv_followers_num.setText(String.valueOf(Commons.g_user.get_count_followers())+" "+"Followers");
        /*txv_description.setText(Commons.g_user.get_description);*/
    }

    private void gotoShowCoverActivity() {

        Intent intent = new Intent(this ,ShowCoverActivity.class);
        startActivity(intent);
    }
    private void gotoEditActivity() {

        Intent intent = new Intent(this ,EditProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.imv_share:
                Share("", "");
                break;

            case R.id.imv_cover:
                //gotoShowCoverActivity();
                break;

            case R.id.imv_edit:
                gotoEditActivity();
                finish();

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
