
package com.freebee.android.main;

import android.content.res.Resources;
import android.graphics.Point;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.freebee.android.R;
import com.freebee.android.adapter.ListViewFaqsAdapter;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Constants;
import com.freebee.android.model.FaqEntity;

import java.util.ArrayList;

public class FAQsActivity extends CommonActivity {

    ImageView imv_back;
    ExpandableListView lst_faqs;
    ListViewFaqsAdapter _adapter;
    ArrayList<FaqEntity> faqEntities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);

        faqEntities.clear();

        for (int i = 0; i < Constants.consFaqTitle.size(); i++){

            FaqEntity faqEntity = new FaqEntity();

            faqEntity.set_id(i);
            faqEntity.set_title(Constants.consFaqTitle.get(i));
            faqEntity.set_content(Constants.consFaqContent.get(i));

            faqEntities.add(faqEntity);
        }

        loadLayout();
    }

    private void loadLayout() {

        lst_faqs = (ExpandableListView)findViewById(R.id.lst_faqs);
        _adapter = new ListViewFaqsAdapter(this, faqEntities);
        lst_faqs.setAdapter(_adapter);
        //lst_faqs.setIndicatorBounds(0,0);

        final int[] prevExpandPosition = {-1};
        lst_faqs.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (prevExpandPosition[0] >= 0 && prevExpandPosition[0] != groupPosition) {
                    lst_faqs.collapseGroup(prevExpandPosition[0]);
                }
                prevExpandPosition[0] = groupPosition;
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                40, r.getDisplayMetrics());
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            lst_faqs.setIndicatorBounds(width - px, width);
        } else {
            lst_faqs.setIndicatorBoundsRelative(width - px, width);
        }

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        faqEntities = new ArrayList<>();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        faqEntities.clear();
    }
}
