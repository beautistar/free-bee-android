package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FilterListActivity extends CommonActivity implements View.OnClickListener {

    ImageView imv_back;
    LinearLayout lyt_office, lyt_golf,lyt_caps, lyt_stress, lyt_trade, lyt_personal, lyt_automotive, lyt_tools, lyt_calendars, lyt_home_kitchen, lyt_food;
    TextView txv_filter;
    boolean[] selectedStatus = new boolean[11];
    String[] categoriesArray = null;
    ArrayList<ProductEntity> allFilter = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_list);

        categoriesArray = getResources().getStringArray(R.array.Categories);
        for (int i = 0; i < categoriesArray.length; i++) selectedStatus[i] = false;

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView) findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoHomeActivity();

            }
        });

        lyt_office = (LinearLayout)findViewById(R.id.lyt_office);
        lyt_office.setOnClickListener(this);

        lyt_golf = (LinearLayout)findViewById(R.id.lyt_golf);
        lyt_golf.setOnClickListener(this);

        lyt_caps = (LinearLayout)findViewById(R.id.lyt_caps);
        lyt_caps.setOnClickListener(this);

        lyt_stress = (LinearLayout)findViewById(R.id.lyt_stress);
        lyt_stress.setOnClickListener(this);

        lyt_trade = (LinearLayout)findViewById(R.id.lyt_trade);
        lyt_trade.setOnClickListener(this);

        lyt_personal = (LinearLayout)findViewById(R.id.lyt_personal);
        lyt_personal.setOnClickListener(this);

        lyt_automotive = (LinearLayout)findViewById(R.id.lyt_automotive);
        lyt_automotive.setOnClickListener(this);

        lyt_tools = (LinearLayout)findViewById(R.id.lyt_tools);
        lyt_tools.setOnClickListener(this);

        lyt_calendars = (LinearLayout)findViewById(R.id.lyt_calendars);
        lyt_calendars.setOnClickListener(this);

        lyt_home_kitchen = (LinearLayout)findViewById(R.id.lyt_home_kitchen);
        lyt_home_kitchen.setOnClickListener(this);

        lyt_food = (LinearLayout)findViewById(R.id.lyt_food);
        lyt_food.setOnClickListener(this);

        txv_filter = (TextView)findViewById(R.id.txv_filter);
        txv_filter.setOnClickListener(this);

    }

    private void gotoHomeActivity(){

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lyt_office:
                view.setSelected(!lyt_office.isSelected());
                //lyt_office.setEnabled(true);
                //selectedStatus[0] = lyt_office.isSelected();
                Array.setBoolean(selectedStatus, 0, lyt_office.isSelected());
                break;

            case R.id.lyt_golf:
                view.setSelected(!lyt_golf.isSelected());
                Array.setBoolean(selectedStatus, 1, lyt_golf.isSelected());
                break;

            case R.id.lyt_caps:
                view.setSelected(!lyt_caps.isSelected());
                Array.setBoolean(selectedStatus, 2, lyt_caps.isSelected());
                break;

            case R.id.lyt_stress:
                view.setSelected(!lyt_stress.isSelected());
                Array.setBoolean(selectedStatus, 3, lyt_stress.isSelected());
                break;

            case R.id.lyt_trade:
                lyt_trade.setSelected(!lyt_trade.isSelected());
                Array.setBoolean(selectedStatus, 4, lyt_trade.isSelected());
                break;

            case R.id.lyt_personal:
                lyt_personal.setSelected(!lyt_personal.isSelected());
                Array.setBoolean(selectedStatus, 5, lyt_personal.isSelected());
                break;

            case R.id.lyt_automotive:
                lyt_automotive.setSelected(!lyt_automotive.isSelected());
                Array.setBoolean(selectedStatus, 6, lyt_automotive.isSelected());
                break;

            case R.id.lyt_tools:
                view.setSelected(!lyt_tools.isSelected());
                Array.setBoolean(selectedStatus, 7, lyt_tools.isSelected());
                break;

            case R.id.lyt_calendars:
                view.setSelected(!lyt_calendars.isSelected());
                Array.setBoolean(selectedStatus, 8, lyt_calendars.isSelected());
                break;

            case R.id.lyt_home_kitchen:
                view.setSelected(!lyt_home_kitchen.isSelected());
                Array.setBoolean(selectedStatus, 9, lyt_home_kitchen.isSelected());
                break;

            case R.id.lyt_food:
                view.setSelected(!lyt_food.isSelected());
                Array.setBoolean(selectedStatus, 10, lyt_food.isSelected());
                break;

            case R.id.txv_filter:
                if (getFilterList().length() > 0)
                    processFilter();
                else showAlertDialog("Please select at least a category.");
                break;
        }
    }

    private void processFilter(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_FILTER_PRODUCT ;

        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response==search===", String.valueOf(response));
                parseResponseSearch(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();}
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.REQ_CATEGORY, getFilterList());
                    params.put(ReqConst.REQ_AGE, String.valueOf(Commons.g_user.getAge()));
                    params.put(ReqConst.REQ_GENDER, Commons.g_user.getGender());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseResponseSearch(String response){

        Log.d("Filter response===>", response);

        closeProgress();

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            allFilter.clear();

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray Products = object.getJSONArray(ReqConst.RES_PRODUCTLIST);

                for (int i = 0; i < Products.length(); i++){

                    JSONObject jsonProduct = (JSONObject)Products.get(i);

                    ProductEntity product = new ProductEntity();

                    product.set_id(jsonProduct.getInt(ReqConst.RES_PRODUCT_ID));
                    product.set_name(jsonProduct.getString(ReqConst.RES_PRODUCT_NAME));
                    product.set_imagUrl(jsonProduct.getString(ReqConst.RES_PRODUCT_IMAGE));

                    allFilter.add(product);
                }

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constants.FILTER_RESULT, allFilter);
                startActivity(intent);
                finish();

            }else if (result_code == 1){

                showAlertDialog("There is no any result.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getFilterList(){

        String filterList = "";

        for (int i = 0; i < categoriesArray.length; i++){

            if (selectedStatus[i]){

                if (filterList.length() > 0)
                    filterList += ";" + categoriesArray[i];
                else filterList +=categoriesArray[i];
            }
        }

        return filterList;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
