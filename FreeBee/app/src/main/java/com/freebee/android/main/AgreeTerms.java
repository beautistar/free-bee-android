package com.freebee.android.main;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freebee.android.R;

public class AgreeTerms extends AppCompatActivity implements View.OnClickListener{

    ImageView ui_imvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agree_terms);
        loadLayout();
    }

    private void loadLayout() {

        TextView textView = (TextView) findViewById(R.id.terms_content);
        textView.setMovementMethod(new ScrollingMovementMethod());
        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                finish();
                break;
        }
    }
}
