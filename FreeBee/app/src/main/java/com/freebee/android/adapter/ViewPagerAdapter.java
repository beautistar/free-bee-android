package com.freebee.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.freebee.android.R;
import com.bumptech.glide.Glide;


import java.util.ArrayList;

/**
 * Created by GoldRain on 10/6/2016.
 */

public class ViewPagerAdapter extends PagerAdapter {

    ArrayList<String> upload_image = new ArrayList<>();
    Context _context ;
    LayoutInflater mLayoutInflater;

    public ViewPagerAdapter(Context context, ArrayList<String> images) {

        this._context = context ;
        mLayoutInflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.upload_image = images ;
    }

    @Override
    public int getCount() {
        return upload_image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==((FrameLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.pager_item ,container , false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.item_img);

        Glide.with(_context).load(upload_image.get(position)).placeholder(R.drawable.ic_logo).into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Activity activity = (Activity)_context;
//                _activity.finish();
                activity.overridePendingTransition(R.anim.left_in, R.anim.right_out);
            }
        });

        String imageUrl = upload_image.get(position);
        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);

        imageView.setImageBitmap(bitmap);
        container.addView(itemView);
        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
