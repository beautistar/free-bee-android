package com.freebee.android.adapter;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.freebee.android.R;

import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/5/2017.
 */

public class RecyclerViewAdapterList extends RecyclerView.Adapter<RecyclerViewAdapterList.ListHolder> {

    UserProfileActivity _activity;

    ArrayList<ProductEntity> _allProduct = new ArrayList<>();

    public RecyclerViewAdapterList(UserProfileActivity activity, ArrayList<ProductEntity> products ){

        _activity = activity;
        _allProduct = products;
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_gdv, parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(ListHolder holder, int position) {

        final ProductEntity productEntity = _allProduct.get(position);

        holder.txvName.setText(productEntity.get_name());

        Glide.with(_activity).load(productEntity.get_imagUrl()).placeholder(R.drawable.place_holder_pattern).into(holder.imvImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_activity, ProductActivity.class);
                intent.putExtra(Constants.PRODUCTID, productEntity.get_id());
                _activity.startActivity(intent);
                _activity.finish();

            }
        });

    }

    @Override
    public int getItemCount() {
        return _allProduct.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        public RoundedImageView imvImage;
        public TextView txvName;

        public ListHolder(View view) {
            super(view);

            txvName = (TextView) view.findViewById(R.id.txv_name);
            imvImage = (RoundedImageView)view.findViewById(R.id.imv_product);
        }
    }
}
