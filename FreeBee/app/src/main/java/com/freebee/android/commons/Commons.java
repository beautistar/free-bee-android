package com.freebee.android.commons;

import android.os.Handler;

import com.freebee.android.base.CommonActivity;
import com.freebee.android.main.MainActivity;
import com.freebee.android.main.PostActivity;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.ShipAddressModel;
import com.freebee.android.model.UserEntity;

import java.util.ArrayList;


/**
 * Created by GoldRain on 9/25/2016.
 */
public class Commons {

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";

    //public static UserEntity g_newUser = null;
    public  static UserEntity g_user = null;
    public static ProductActivity g_productActivity = null;

    public static  int PRODUCT_LIST_COUNT = 0;
    public static  int PRODUCT_LIKE_COUNT = 0;
    public static  int PRODUCT_BOUGHT_COUNT = 0;

    public static CommonActivity g_currentActivity = null;
    public static PostActivity g_postActivity = null;
    public static ShipAddressModel shippingAddress = null;
    public static ArrayList<ProductEntity> orderProducts = null;

}
