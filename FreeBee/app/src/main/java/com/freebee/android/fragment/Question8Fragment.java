package com.freebee.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;

import java.util.ArrayList;

public class Question8Fragment extends Fragment implements View.OnClickListener {

    QuestionsActivity activity;
    View view;

    TextView txv_yes, txv_no, txv_0_3m, txv_3_6m, txv_6_12m, txv_1_2y, txv_others;

    ArrayList<TextView> arraySelling = new ArrayList<>();
    LinearLayout lyt_selling;;

    public Question8Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question8, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {


        txv_yes = (TextView)view.findViewById(R.id.txv_yes);
        txv_yes.setOnClickListener(this);
        txv_yes.setVisibility(View.GONE);

        txv_no = (TextView)view.findViewById(R.id.txv_no);
        txv_no.setOnClickListener(this);
        txv_no.setVisibility(View.GONE);

        txv_0_3m = (TextView)view.findViewById(R.id.txv_0_3m);
        txv_0_3m.setOnClickListener(this);
        arraySelling.add(txv_0_3m);

        txv_3_6m = (TextView)view.findViewById(R.id.txv_3_6m);
        txv_3_6m.setOnClickListener(this);
        arraySelling.add(txv_3_6m);

        txv_6_12m = (TextView)view.findViewById(R.id.txv_6_12m);
        txv_6_12m.setOnClickListener(this);
        arraySelling.add(txv_6_12m);

        txv_1_2y = (TextView)view.findViewById(R.id.txv_1_2y);
        txv_1_2y.setOnClickListener(this);
        arraySelling.add(txv_1_2y);

        txv_others = (TextView)view.findViewById(R.id.txv_other);
        txv_others.setOnClickListener(this);
        arraySelling.add(txv_others);

        lyt_selling = (LinearLayout)view.findViewById(R.id.lyt_selling);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity) context;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_yes:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));

                break;

            case R.id.txv_no:

                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));

                break;

            case R.id.txv_0_3m:

                selectSellingTerms(1);

                break;

            case R.id.txv_3_6m:
                selectSellingTerms(2);
                break;

            case R.id.txv_6_12m:

                selectSellingTerms(3);
                break;

            case R.id.txv_1_2y:
                selectSellingTerms(4);
                break;

            case R.id.txv_other:
                selectSellingTerms(5);
                break;

        }
    }


    private void selectSellingTerms(int terms){


        arraySelling.get(terms-1).setBackgroundResource(R.drawable.signup_round);
        arraySelling.get(terms-1).setTextColor(getResources().getColor(R.color.white));

        for (int i = 0; i < arraySelling.size() ; i++){

            if (i == (terms - 1)) continue;
            arraySelling.get(i).setBackgroundResource(R.drawable.item_unselect_fillrect);
            arraySelling.get(i).setTextColor(getResources().getColor(R.color.black));
        }

    }

    private void setBackground(int a, int b, int e, int f){

        txv_yes.setBackgroundResource(a);
        txv_no.setBackgroundResource(b);

        txv_yes.setTextColor(e);
        txv_no.setTextColor(f);

    }
}
