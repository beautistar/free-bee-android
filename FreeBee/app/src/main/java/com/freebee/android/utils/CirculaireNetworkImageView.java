package com.freebee.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.freebee.android.R;
import com.android.volley.toolbox.NetworkImageView;



/**
 * Created by GoldRain on 9/25/2016.
 */
public class CirculaireNetworkImageView extends NetworkImageView {

    private int borderWidth;
    private int canvasSize;
    private Bitmap image;
    private Paint paint;
    private Paint paintBorder;

    public CirculaireNetworkImageView(final Context context) {
        this(context, null);
    }

    public CirculaireNetworkImageView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.actionButtonStyle);
    }

    public CirculaireNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        paint = new Paint();
        paint.setAntiAlias(true);

        paintBorder = new Paint();
        paintBorder.setAntiAlias(true);
    }

    public void setBorderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
        this.requestLayout();
        this.invalidate();
    }

    public void setBorderColor(int borderColor) {
        if (paintBorder != null)
            paintBorder.setColor(borderColor);
        this.invalidate();
    }

    public void addShadow() {
        setLayerType(LAYER_TYPE_SOFTWARE, paintBorder);
        paintBorder.setShadowLayer(4.0f, 0.0f, 2.0f, Color.BLACK);
    }





}
