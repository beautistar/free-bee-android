package com.freebee.android.base;


import android.content.Intent;
import androidx.fragment.app.Fragment;

/**
 * Created by GoldRain on 9/26/2016.
 */

public class BaseFragment extends Fragment {

    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }
    public void showToast (String strMsg){

        _context.showToast(strMsg);
    }
    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

    public void Share(String subject, String text){


        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT , text);
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e){
            e.printStackTrace();
        } catch (java.lang.NullPointerException e){
            e.printStackTrace();
        }
    }
}
