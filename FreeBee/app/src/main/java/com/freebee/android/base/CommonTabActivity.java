package com.freebee.android.base;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;

import com.freebee.android.commons.Commons;


/**
 * Created by GoldRain on 9/25/2016.
 */
public class CommonTabActivity extends CommonActivity {

    protected TextView ui_txvUnread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Commons.g_currentActivity = this;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK){

            Commons.g_isAppRunning = false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
