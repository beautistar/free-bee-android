package com.freebee.android.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;


import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;

import java.io.ByteArrayOutputStream;

public class InviteFriendsActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imgShare ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        loadLayout();

    }

    private void loadLayout() {
        gotoShareActivity("Share" , "");
    }

    public void gotoShareActivity(String subject, String text){

        try{
            View mView = findViewById(R.id.imv_content);
//            mView = mView.getRootView(); //full screen image
            mView.setDrawingCacheEnabled(true);
            Bitmap bitmap = mView.getDrawingCache();

            Intent intent = new Intent(Intent.ACTION_SEND);

            intent.setType("image/jpeg");
            Bitmap bitmap1 = bitmap;
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap1, "null", null);
            Uri imageUri = Uri.parse(path);
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(intent);

        }catch (android.content.ActivityNotFoundException e){
            e.printStackTrace();
        }catch (java.lang.NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

    }
}
