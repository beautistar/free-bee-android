package com.freebee.android.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;

public class Question6Fragment extends Fragment implements View.OnClickListener {

    QuestionsActivity activity;
    View view;

    TextView txv_own, txv_rent;

    String own = "Do you plan on selling in the next year?", rent = "Do you plan on buying a home in the next year?";

    public Question6Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_question6, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_own = (TextView)view.findViewById(R.id.txv_own);
        txv_own.setOnClickListener(this);

        txv_rent = (TextView)view.findViewById(R.id.txv_rent);
        txv_rent.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()){

            case R.id.txv_own:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));

                showAlert(own);

                break;

            case R.id.txv_rent:

                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));
                showAlert(rent);
                break;

        }

    }

    private void showAlert(String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        alertDialog.dismiss();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b = alertDialog.getButton((DialogInterface.BUTTON_POSITIVE));
                b.setTextColor(getResources().getColor(R.color.alertdialog));
            }
        });

        alertDialog.show();
    }

    private void setBackground(int a, int b, int e, int f){

        txv_own.setBackgroundResource(a);
        txv_rent.setBackgroundResource(b);

        txv_own.setTextColor(e);
        txv_rent.setTextColor(f);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity) context;
    }


}
