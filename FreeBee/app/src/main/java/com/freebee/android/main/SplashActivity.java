package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.freebee.android.R;

import com.freebee.android.adapter.ViewPagerAdapterTutorial;
import com.freebee.android.base.CommonActivity;

import com.freebee.android.commons.Constants;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pixplicity.easyprefs.library.Prefs;

import me.relex.circleindicator.CircleIndicator;


public class SplashActivity extends CommonActivity {

    String android_id = "";
    int _idx = 0 ;

    Boolean _isFirstUser = true;

    ViewPagerAdapterTutorial _adapterTutorial;
    ViewPager vpg_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        _isFirstUser = Preference.getInstance().getValue(this, PrefConst.PREFKEY_FIRSTUSER,true);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {

                String newToken = instanceIdResult.getToken();
                Log.d("NEW_TOKEN", newToken);
                Prefs.putString(Constants.TOKEN, newToken);
            }
        });

        loadSplash();

    }

    private void loadSplash() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (_isFirstUser){

                    firstTutorial();

                }
                else {

                    userRegister();

                }
            }
        }, Constants.SPLASH_TIME);
    }

    public void userRegister(){

        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }


    private void firstTutorial(){


        FragmentManager manager = getSupportFragmentManager();
        _adapterTutorial = new ViewPagerAdapterTutorial(manager, this);
        vpg_container = (ViewPager)findViewById(R.id.vpg_container);
        vpg_container.setAdapter(_adapterTutorial);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(vpg_container);
    }
}
