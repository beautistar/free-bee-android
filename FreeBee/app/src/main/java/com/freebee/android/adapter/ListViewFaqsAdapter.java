package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.FAQsActivity;
import com.freebee.android.model.FaqEntity;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListViewFaqsAdapter extends BaseExpandableListAdapter {

    FAQsActivity activity;
    ArrayList<FaqEntity> faqEntityArrayList = new ArrayList<>();

    public ListViewFaqsAdapter(FAQsActivity _activity, ArrayList<FaqEntity> faqEntities){

        faqEntityArrayList.clear();

        this.activity = _activity;
        this.faqEntityArrayList = faqEntities;

    }

    @Override
    public int getGroupCount() {
        return faqEntityArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return faqEntityArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return faqEntityArrayList.get(childPosition).get_title();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        FaqHolder faqHolder;

        if (convertView == null){

            faqHolder = new FaqHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_faq, null);

            faqHolder.txv_number = (TextView)convertView.findViewById(R.id.txv_nuber);
            faqHolder.txv_title = (TextView)convertView.findViewById(R.id.txv_title);
            faqHolder.lyt_item = (LinearLayout)convertView.findViewById(R.id.lyt_item);

            convertView.setTag(faqHolder);

        } else {

            faqHolder = (FaqHolder)convertView.getTag();
        }

        FaqEntity faqEntity = (FaqEntity)faqEntityArrayList.get(groupPosition);
        faqHolder.txv_number.setText(String.valueOf(groupPosition + 1));
        faqHolder.txv_title.setText(faqEntity.get_title());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        FaqChildHolder faqChild;

        if (convertView == null){

            faqChild = new FaqChildHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_faq_child,null);

            faqChild.txv_content = (TextView)convertView.findViewById(R.id.txv_content);

            convertView.setTag(faqChild);

        } else {

            faqChild = (FaqChildHolder) convertView.getTag();
        }

        FaqEntity faqEntity = (FaqEntity)faqEntityArrayList.get(groupPosition);
        faqChild.txv_content.setText(faqEntity.get_content());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public class FaqHolder {

        TextView txv_number, txv_title;
        LinearLayout lyt_item;
    }

    public class FaqChildHolder{

        TextView txv_content;
    }

    /*FAQsActivity activity;
    ArrayList<FaqEntity> faqEntityArrayList = new ArrayList<>();

    public ListViewFaqsAdapter(FAQsActivity _activity, ArrayList<FaqEntity> faqEntities){

        this.activity = _activity;
        faqEntityArrayList = faqEntities;

    }
    @Override
    public int getCount() {
        return faqEntityArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return faqEntityArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FaqHolder faqHolder;

        if (convertView == null){

            faqHolder = new FaqHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_faq, parent, false);

            faqHolder.txv_number = (TextView)convertView.findViewById(R.id.txv_nuber);
            faqHolder.txv_title = (TextView)convertView.findViewById(R.id.txv_title);
            faqHolder.lyt_item = (LinearLayout)convertView.findViewById(R.id.lyt_item);

            convertView.setTag(faqHolder);

        } else {
            faqHolder = (FaqHolder)convertView.getTag();
        }

        FaqEntity faqEntity = (FaqEntity)faqEntityArrayList.get(position);
        faqHolder.txv_number.setText(String.valueOf(position + 1));
        faqHolder.txv_title.setText(faqEntity.get_title());

        faqHolder.lyt_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return convertView;
    }

    public class FaqHolder {

        TextView txv_number, txv_title;
        LinearLayout lyt_item;
    }*/
}
