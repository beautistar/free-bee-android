package com.freebee.android.model;

import java.io.Serializable;

public class InterestsEntity implements Serializable {

    int _id = 0;
    String _item = "";
    boolean _status = false;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_item() {
        return _item;
    }

    public void set_item(String _item) {
        this._item = _item;
    }

    public boolean is_status() {
        return _status;
    }

    public void set_status(boolean _status) {
        this._status = _status;
    }
}
