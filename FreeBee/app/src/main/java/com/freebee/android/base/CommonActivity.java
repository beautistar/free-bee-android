package com.freebee.android.base;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import com.freebee.android.commons.Commons;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class CommonActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private String getLauncherClassName(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(getPackageName());

        List<ResolveInfo> resolveInfosList = getPackageManager().queryIntentActivities(intent,0);
        if (resolveInfosList != null && resolveInfosList.size()>0){

        }

        return null;
    }


    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Commons.g_isAppPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Commons.g_isAppPaused = false;
        Commons.g_isAppRunning = true;
    }


}
