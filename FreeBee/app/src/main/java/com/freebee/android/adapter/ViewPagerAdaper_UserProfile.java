package com.freebee.android.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.freebee.android.R;
import com.freebee.android.fragment.BoughtFragment;
import com.freebee.android.fragment.LikedFragment;
import com.freebee.android.fragment.ListedFragment;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;

import java.util.ArrayList;


/**
 * Created by ToSuccess on 10/19/2016.
 */

public class ViewPagerAdaper_UserProfile extends FragmentStatePagerAdapter {

    Context _context;
    UserProfileActivity _activity;
    ArrayList<ProductEntity> _list = new ArrayList<>();
    ArrayList<ProductEntity> _like = new ArrayList<>();
    ArrayList<ProductEntity> _bought = new ArrayList<>();

    public ViewPagerAdaper_UserProfile (UserProfileActivity context, FragmentManager fm, ArrayList<ProductEntity> list, ArrayList<ProductEntity> like, ArrayList<ProductEntity> bought){

        super(fm);
        this._activity = context ;

        _list.addAll(list);
        _like.addAll(like);
        _bought.addAll(bought);
    }

    @Override
    public int getItemPosition(Object object)
    {

        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag = null ;

        switch (position){

            case 0:

                frag = new ListedFragment(_activity , _list);
                break;

            case 1:
                frag = new LikedFragment(_activity,_like);

                break;

            case 2:
                frag = new BoughtFragment(_activity,_bought);

                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title=_activity.getString(R.string.listed_pro);
                break;
            case 1:
                title=_activity.getString(R.string.liked_pro);
                break;
            case 2:
                title=_activity.getString(R.string.claimed_pro);
                break;
        }

        return title;
    }

}
