package com.freebee.android.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends CommonActivity implements View.OnClickListener {


    ArrayList<ProductEntity> _searchProducts = new ArrayList<>();
    ImageView ui_imvBack;

    LinearLayout ui_lytElec, ui_lytCars, ui_lytSprots, ui_lytHome,
                ui_lytMovies, ui_lytFashion, ui_lytBaby, ui_lytOther ;

    EditText edt_from, edt_to ;
    String category ="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        loadLayout();

    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_lytElec = (LinearLayout) findViewById(R.id.lyt_electronics);
        ui_lytElec.setOnClickListener(this);

        ui_lytCars = (LinearLayout) findViewById(R.id.lyt_cars_);
        ui_lytCars.setOnClickListener(this);

        ui_lytSprots = (LinearLayout) findViewById(R.id.lyt_sports);
        ui_lytSprots.setOnClickListener(this);

        ui_lytHome = (LinearLayout) findViewById(R.id.lyt_home_garden);   //  lyt_home : !!  can change:
        ui_lytHome.setOnClickListener(this);

        ui_lytMovies = (LinearLayout) findViewById(R.id.lyt_movies);
        ui_lytMovies.setOnClickListener(this);

        ui_lytFashion = (LinearLayout) findViewById(R.id.lyt_fashion);
        ui_lytFashion.setOnClickListener(this);

        ui_lytBaby = (LinearLayout) findViewById(R.id.lyt_baby);
        ui_lytBaby.setOnClickListener(this);

        ui_lytOther = (LinearLayout) findViewById(R.id.lyt_other);
        ui_lytOther.setOnClickListener(this);

        TextView ui_txvSearch = (TextView) findViewById(R.id.txv_search);
        ui_txvSearch.setOnClickListener(this);

        edt_from = (EditText)findViewById(R.id.edt_from);
        edt_to = (EditText)findViewById(R.id.edt_to);
    }

    private void searchProduct(){

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCHPRODUCT ;

        if(ui_lytElec.isSelected() == true){
            category = "1";}

        if(ui_lytCars.isSelected() == true ){

            if(category.length()== 0 || category == null){
                category = "2";
            }else category += ",2";}

        if(ui_lytSprots.isSelected()==true){

            if(category.length()==0 || category ==null){
                category = "3";
            }else category += ",3";}

        if(ui_lytHome.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="4";
            }else category +=",4";}

        if(ui_lytMovies.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="5";
            }else category +=",5";}

        if(ui_lytFashion.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="6";
            }else category +=",6";}

        if(ui_lytBaby.isSelected()==true){

            if(category.length()==0 || category ==null){
                category ="7";
            }else category +=",7";}

        if(ui_lytOther.isSelected() == true){

            if(category.length()==0 || category ==null){
                category ="8";
            }else category +=",8";}

        Log.d("string++++++++++", category);

        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response==search===", String.valueOf(response));
                parseResponseSearch(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();}
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("category", category);
                    params.put("price_from", edt_from.getText().toString());
                    params.put("price_to",edt_to.getText().toString());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseResponseSearch(String response){

        closeProgress();

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray Products = object.getJSONArray(ReqConst.RES_PRODUCTLIST);

                for (int i = 0; i < Products.length(); i++){

                    JSONObject jsonProduct = (JSONObject)Products.get(i);

                    ProductEntity product = new ProductEntity();

                    product.set_id(jsonProduct.getInt("product_id"));
                    product.set_name(jsonProduct.getString("product_name"));
                    product.set_imagUrl(jsonProduct.getString("product_image"));

                    // Log.d("==========photo=======", product.get_imagUrl());

                    _searchProducts.add(product);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        gotoMainActivity(_searchProducts);
    }

    private void gotoMainActivity(ArrayList<ProductEntity> _searchProducts){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.FILTER_RESULT, _searchProducts);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.lyt_electronics:

                view.setSelected(!ui_lytElec.isSelected());
                ui_lytElec.setEnabled(true);
                break;

            case R.id.lyt_cars_:
                view.setSelected(!ui_lytCars.isSelected());
                ui_lytCars.setEnabled(true);
                break;

            case R.id.lyt_sports:

                view.setSelected(!ui_lytSprots.isSelected());
                ui_lytSprots.setEnabled(true);
                break;

            case R.id.lyt_home_garden:

                view.setSelected(!ui_lytHome.isSelected());
                ui_lytHome.setEnabled(true);
                break;

            case R.id.lyt_movies:

                view.setSelected(!ui_lytMovies.isSelected());
                ui_lytMovies.setEnabled(true);
                break;

            case R.id.lyt_fashion:

                view.setSelected(!ui_lytFashion.isSelected());
                ui_lytFashion.setEnabled(true);
                break;

            case R.id.lyt_baby:

                view.setSelected(!ui_lytBaby.isSelected());
                ui_lytBaby.setEnabled(true);

                break;

            case R.id.lyt_other:

                view.setSelected(!ui_lytOther.isSelected());
                ui_lytOther.setEnabled(true);

                break;

            case R.id.imv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.txv_search:

                searchProduct();
                break;
        }

    }

}



