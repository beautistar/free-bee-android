package com.freebee.android.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Button;
import android.widget.Toast;

import com.akexorcist.localizationactivity.LocalizationDelegate;
import com.akexorcist.localizationactivity.OnLocaleChangedListener;

import com.freebee.android.commons.Constants;
import com.freebee.android.R;

import java.util.Locale;


/**
 * Created by GoldRain on 9/29/2016.
 */

public abstract class BaseActivity extends AppCompatActivity implements Handler.Callback, OnLocaleChangedListener {

    private LocalizationDelegate localizationDelegate = new LocalizationDelegate(this);

    public Context _context = null;

    public Handler _handler = null;

    private ProgressDialog _progressDlg;

    private Vibrator _vibrator;

    public DrawerLayout ui_drawerlayout ;

    public boolean _isEndFlag;    // dobule click back button to kill the app

    public static final int BACK_TWO_CLICK_DELAY_TIME = 3000 ; //ms



    public Runnable _exitRunner = new Runnable() {
        @Override
        public void run() {
            _isEndFlag = false ;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        localizationDelegate.addOnLocaleChengedListener(this);
        localizationDelegate.onCreate(savedInstanceState);

        setDefaultLanguage("en");
        super.onCreate(savedInstanceState);

        _context = this;

        _vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);

        Constants.strInterests.clear();
        Constants.consFaqContent.clear();
        Constants.consFaqTitle.clear();
        //interests
        Constants.strInterests.add("Hockey");
        Constants.strInterests.add("Football");
        Constants.strInterests.add("Baseball");
        Constants.strInterests.add("basketball");
        Constants.strInterests.add("billiards");
        Constants.strInterests.add("track and field");
        Constants.strInterests.add("sailing");
        Constants.strInterests.add("running");
        Constants.strInterests.add("hiking");
        Constants.strInterests.add("biking");
        Constants.strInterests.add("marathons");
        Constants.strInterests.add("triathlons");
        Constants.strInterests.add("kayaking");
        Constants.strInterests.add("canoeing");
        Constants.strInterests.add("boating");
        Constants.strInterests.add("fishing");
        Constants.strInterests.add("bowling");
        Constants.strInterests.add("tennis");
        Constants.strInterests.add("cricket");
        Constants.strInterests.add("volleyball");
        Constants.strInterests.add("archery");
        Constants.strInterests.add("reading");
        Constants.strInterests.add("writing");
        Constants.strInterests.add("drawing");
        Constants.strInterests.add("listening to music");
        Constants.strInterests.add("skateboarding");
        Constants.strInterests.add("soccer");
        Constants.strInterests.add("dancing");
        Constants.strInterests.add("skydiving");
        Constants.strInterests.add("flying");
        Constants.strInterests.add("traveling");
        Constants.strInterests.add("car racing");
        Constants.strInterests.add("motorcycle riding");
        Constants.strInterests.add("Computers");
        Constants.strInterests.add("technology");
        Constants.strInterests.add("investing");
        Constants.strInterests.add("acting");
        Constants.strInterests.add("baking");
        Constants.strInterests.add("blogging");
        Constants.strInterests.add("board games");
        Constants.strInterests.add("video gaming");
        Constants.strInterests.add("programming");
        Constants.strInterests.add("martial arts");
        Constants.strInterests.add("collecting");
        Constants.strInterests.add("couponing");
        Constants.strInterests.add("crafts");
        Constants.strInterests.add("electronics");
        Constants.strInterests.add("embroidery");
        Constants.strInterests.add("fantasy sports");
        Constants.strInterests.add("fashion");
        Constants.strInterests.add("carpentry");
        Constants.strInterests.add("genealogy");
        Constants.strInterests.add("graphic design");
        Constants.strInterests.add("home improvement");
        Constants.strInterests.add("ice skating");
        Constants.strInterests.add("karaoke");
        Constants.strInterests.add("knitting");
        Constants.strInterests.add("painting");
        Constants.strInterests.add("photography");
        Constants.strInterests.add("musical instruments");
        Constants.strInterests.add("pottery");
        Constants.strInterests.add("puzzles");
        Constants.strInterests.add("scrapbooking");
        Constants.strInterests.add("sculpting");
        Constants.strInterests.add("sewing");
        Constants.strInterests.add("social media");
        Constants.strInterests.add("watching television");
        Constants.strInterests.add("watching movies");
        Constants.strInterests.add("weight lifting");
        Constants.strInterests.add("wine");
        Constants.strInterests.add("beer");
        Constants.strInterests.add("yoga");
        Constants.strInterests.add("climbing");
        Constants.strInterests.add("surfing");
        Constants.strInterests.add("swimming");

        //Faq title

        Constants.consFaqTitle.add("What is Free-Bee?");
        Constants.consFaqTitle.add("How does Free-Bee allow companies to put their promotional products into the hands of a targeted audience?");
        Constants.consFaqTitle.add("What are the benefits to advertisers who use Free-Bee?");
        Constants.consFaqTitle.add("What does it cost?");
        Constants.consFaqTitle.add("How do my promotional products get from point A to point B?");
        Constants.consFaqTitle.add("Promotional products tend to be cheap throw away free-bees. Why would anyone want this stuff?");
        Constants.consFaqTitle.add("Can I control the quantity each claimer can receive for each product listing?");
        Constants.consFaqTitle.add("Why would I want to pay to send promotional products to people who I don’t know?");
        Constants.consFaqTitle.add("What is an example of how this service can provide a return for me?");
        Constants.consFaqTitle.add("As a Free-Bee claimer, what am I allowed to claim for free?");
        Constants.consFaqTitle.add("How often can I claim a bundle?");
        Constants.consFaqTitle.add("I don’t have any promotional products to give away. How can I obtain them?");
        Constants.consFaqTitle.add("How are free-bees categorized as standard or promotional?");
        Constants.consFaqTitle.add("Can Advertisers and Claimers communicate?");
        Constants.consFaqTitle.add("Can Advertisers control who receives their free-bees?");

        //Faq content

        Constants.consFaqContent.add("Free-Bee is a first of its kind advertising platform that allows companies to direct their promotional products to the right people using various targeting metrics.");
        Constants.consFaqContent.add("Free-Bee “claimers” must fill out a detailed questionnaire at signup. This allows Free-Bee to identify many metrics about them including their geographical location, age, gender, workplace, occupation, hobbies, interests and activities they participate in.");
        Constants.consFaqContent.add("Typically, a company will meet with potential clients or end users at networking events, conventions and one-on-one meet and greets. Now for the first time, companies are able to easily and accurately put their promotional products into the hands of the people who are most valuable without ever having to go through conventional channels. Many Free-Bee users have excess inventory of promotional products and no way to give them away. With Free-Bee these goods can remain valuable by broadening brand recognition to new audiences all over the world or in your immediate vicinity.");
        Constants.consFaqContent.add("“Claimers” can claim bundles of promotional products at no cost. Advertisers are charged a small fee per item “claimed.” The charge for standard free-bees such as pens, small calendars, keychains, small bags is $0.50 and the charge for premium free-bees such as flashlights, mugs, bottles, electronic devices, and apparel is $2.00.");
        Constants.consFaqContent.add("Advertisers must ship their promotional products in bulk to the Free-Bee warehouse after free-bees are listed on the platform and prior to listing activation. Once “claimers” claim their free-bees and finalize their bundles Free-Bee will ship the bundles via USPS as no cost to advertisers or claimers.");
        Constants.consFaqContent.add("In the old days promotional free-bees were undesirable, cheaply made throwaways of little value. Times have changes. Now literally thousands of cool and useful free-bees are manufactured around the world in tremendously large quantities. People are always excited to receive free things which are useful because they are paying for these goods every day.");
        Constants.consFaqContent.add("Yes. Advertisers set limits on how many of a given item a claimer can receive. For example, if you want each claimer to receive only one of your mugs you can set that limitation in the product listing page.");
        Constants.consFaqContent.add("By putting your branded free-bees into the hands of valuable, targeted audiences you will increase brand recognition cheaply using an entirely untapped advertising channel. Conventional digital advertising platforms are oversaturated. Viewers are inundated with hundreds of ads daily causing them to suffer from what’s called banner blindness. The effect of this is constantly decreasing effectiveness of traditional digital advertisements. Advertisers can overcome this by getting their branded goods physically into the hands of a targeted audience. Moreover, where a single display ad is seen once very briefly and then disappears, a physical item can remain visible and of use to audiences for weeks, months and even years.");
        Constants.consFaqContent.add("Imagine you make insoles for running shoes. You are currently expanding your physical retail presence to a particular city in which you previously had none. Now imagine, the value of placing a promotional item, let’s say a headband with your name, logo and slogan on it, into the hands of a runner who lives in that city. What is the value of that runner viewing and selecting your product, unpacking it and wearing it whenever he or she goes for a run? We think a lot.");
        Constants.consFaqContent.add("You will claim goods in what Free-Bee calls bundles. A bundle consists of no more than 10 freebees which can be any mixture of standard and premium free-bees.");
        Constants.consFaqContent.add("Claimers are limited to one bundle per month to ensure advertisers get great value and are not diluted by claimers being inundated with free goods, and to reduce abusive practices of the platform.");
        Constants.consFaqContent.add("Free-Bee has a list of cooperating promotional product manufacturers who offer special rates to Free-Bee users. Each manufacturer provides their own unique line of promotional products. Please see the list of cooperating manufacturers below. By using cooperating manufacturers, you will be able to have your products shipped directly to the Free-Bee warehouse, which will provide even more cost saving to you.");
        Constants.consFaqContent.add("Please consult our product category list to determine how your free-bees will be categorized. Free-Bee will ultimately decide how products are categorized.");
        Constants.consFaqContent.add("Yes. Free-Bee is rolling out a new communication tool which will allow advertisers to communicate with claimers who have claimed or wish to claim at least one of the advertiser’s free-bees. This tool will be beneficial in various situations including when advertisers want to vet claimers of high value promotional free-bees (Generally over $20 in value/item). Likewise, claimers are able to communicate with advertisers when they would like to discuss potential business relationships or to ask for more information about the advertiser.");
        Constants.consFaqContent.add("Yes. For low value free-bees (Generally below $20/item but typically under $5 production cost) and high value items, advertisers should assign targeting metrics which are available in the product listing page. For High Value free-bees, Advertisers will have the opportunity to vet claimers before allowing a claim to be processed. Advertisers are required to respond to a claimer in this situation within one hour or the claimer will be informed to move on.");
    }

    protected void onResume(){
        super.onResume();
        localizationDelegate.onResume();
    }

    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)
                _vibrator.cancel();
        } catch (Exception e) {
        }
        _vibrator = null;

        super.onDestroy();
    }


    public void showProgress(boolean cancelable) {

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg
                .setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();

    }

    public void showProgress() {
        showProgress(false);
    }

    public void closeProgress() {

        if(_progressDlg == null) {
            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }

    public void showAlertDialog(String msg) {

        final AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b = alertDialog.getButton((DialogInterface.BUTTON_POSITIVE));
                b.setTextColor(getResources().getColor(R.color.splash_bg));
            }
        });

        alertDialog.show();
    }

    /**
     * show toast
     * @return
     */

    public void showToast (String toast_string){

        Toast.makeText(_context,toast_string, Toast.LENGTH_SHORT).show();
    }

    public void vibrate(){

        if (_vibrator != null)
            _vibrator.vibrate(500);
    }


    @Override
    public boolean handleMessage(Message msg) {

        switch (msg.what){
            default:
                break;
        }
        return false;
    }

    public void setupNavigationBar() {

        DrawerLayout ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open , R.string.drawer_close);

        ui_drawerlayout.setDrawerListener(drawerToggle);
    }

    public void showDrawer() {

        DrawerLayout ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    public void onExit() {

        if (_isEndFlag == false) {

            Toast.makeText(this, getString(R.string.str_back_one_more_end),
                    Toast.LENGTH_SHORT).show();
            _isEndFlag = true;

            _handler.postDelayed(_exitRunner, BACK_TWO_CLICK_DELAY_TIME);

        } else if (_isEndFlag == true) {


            finish();
        }
    }

    public void Share(String subject, String text) {

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT , text);
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e){
            e.printStackTrace();
        } catch (java.lang.NullPointerException e){
            e.printStackTrace();
        }
    }

    public final void setLanguage(String language) {
        localizationDelegate.setLanguage(language);
    }

    public final void setLanguage(Locale locale) {
        localizationDelegate.setLanguage(locale);
    }

    public final void setDefaultLanguage(String language) {
        localizationDelegate.setDefaultLanguage(language);
    }

    public final void setDefaultLanguage(Locale locale) {
        localizationDelegate.setDefaultLanguage(locale);
    }

    public final String getLanguage() {
        return localizationDelegate.getLanguage();
    }

    public final Locale getLocale() {
        return localizationDelegate.getLocale();
    }

    // Just override method locale change event
    @Override
    public void onBeforeLocaleChanged() { }

    @Override
    public void onAfterLocaleChanged() { }

}
