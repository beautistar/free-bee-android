package com.freebee.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.main.MyBundleActivity;
import com.freebee.android.main.ProductActivity;
import com.freebee.android.model.ProductEntity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BundleListAdapter extends BaseAdapter {

    MyBundleActivity context;
    ArrayList<ProductEntity> allProduct = new ArrayList<>();

    public BundleListAdapter(MyBundleActivity context){
        this.context = context;
    }

    public BundleListAdapter(MyBundleActivity context, ArrayList<ProductEntity> products){

        this.context = context;
        this.allProduct = products;
    }

    public void addAllCart(MyBundleActivity activity, ArrayList<ProductEntity> allCart){

        this.context = activity;

        allProduct.clear();
        this.allProduct = allCart;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allProduct.size();
    }

    @Override
    public Object getItem(int position) {
        return allProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BundleHolder holder;
        if (convertView ==  null){

            holder = new BundleHolder();
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_bundle_list, parent, false);

            holder.imv_product = (ImageView)convertView.findViewById(R.id.imv_product);
            holder.imv_remove = (ImageView)convertView.findViewById(R.id.imv_remove);
            holder.imv_details = (ImageView)convertView.findViewById(R.id.imv_details);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_quantity = (TextView)convertView.findViewById(R.id.txv_quantity);
            holder.txv_type = (TextView)convertView.findViewById(R.id.txv_type);

            convertView.setTag(holder);
        }
        else {
            holder = (BundleHolder)convertView.getTag();
        }

        ProductEntity product = (ProductEntity)allProduct.get(position);
        if (product.get_imagUrl().length() > 0)
            Glide.with(context).load(product.get_imagUrl()).into(holder.imv_product);
        holder.txv_name.setText(product.get_name());
        holder.txv_quantity.setText("Quantity" + " " + product.get_cart_count());
        holder.txv_type.setText(product.getType());

        holder.imv_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra(Constants.PRODUCTID, product.get_id());
                context.startActivity(intent);
                context.finish();

            }
        });

        holder.imv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.confirmRemoveCart(product);
                //removeCart(product);
            }
        });

        return convertView;
    }

    public void removeCart(ProductEntity product){

        context.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REMOVE_CART_PRODUCT;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                context.closeProgress();

                try {
                    JSONObject object = new JSONObject(response);

                    int result_code = object.getInt(ReqConst.RES_CODE);

                    if (result_code == ReqConst.CODE_SUCCESS){

                        allProduct.remove(product);
                        notifyDataSetChanged();
                        context.showToast("Removed from cart.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                context.closeProgress();
                context.showAlertDialog(context.getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.RES_PRODUCTID, String.valueOf(product.get_id()));
                    params.put(ReqConst.REQ_COUNT, String.valueOf(product.get_cart_count()));

                } catch (Exception e) {

                    context.closeProgress();
                    context.showAlertDialog(context.getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    public class BundleHolder{

        ImageView imv_product, imv_remove, imv_details;
        TextView txv_name, txv_quantity, txv_type;
    }
}
