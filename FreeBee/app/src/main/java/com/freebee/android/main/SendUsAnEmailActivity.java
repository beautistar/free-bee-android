package com.freebee.android.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.freebee.android.R;
import com.freebee.android.base.CommonActivity;

public class SendUsAnEmailActivity extends CommonActivity implements View.OnClickListener {

    ImageView  ui_imvBack ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_us_an_email);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.imv_back:
                finish();
                overridePendingTransition
                        (R.anim.right_in, R.anim.left_out);
                break;

        }
    }

}
