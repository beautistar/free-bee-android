package com.freebee.android.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.freebee.android.R;


import com.freebee.android.adapter.RecyclerViewLikeAdapter;
import com.freebee.android.base.BaseFragment;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;

import java.util.ArrayList;

/**
 * Created by GoldRain on 10/8/2016.
 */

@SuppressLint("ValidFragment")
public class LikedFragment extends BaseFragment {

    UserProfileActivity _activity ;
    ArrayList<ProductEntity> _likes = new ArrayList<>();
    RecyclerViewLikeAdapter _adapter;
    RecyclerView ui_gdv;

    public LikedFragment (UserProfileActivity context, ArrayList<ProductEntity> like) {

        this._activity = context ;
        _likes.addAll(like);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d("======likes======", String.valueOf(_likes));

        View view = inflater.inflate(R.layout.fragment_liked, container, false) ;

        ui_gdv = (RecyclerView)view.findViewById(R.id.gdv_liked);
        ui_gdv.setLayoutManager(new GridLayoutManager(_activity,2));
        ui_gdv.setHasFixedSize(true);
        ui_gdv.setItemAnimator(new DefaultItemAnimator());
        _adapter = new RecyclerViewLikeAdapter(_activity, _likes);

        ui_gdv.setAdapter(_adapter);

        return view;
    }
}
