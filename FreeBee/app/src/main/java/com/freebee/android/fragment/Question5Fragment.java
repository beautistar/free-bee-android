package com.freebee.android.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.adapter.CitiesSelectListAdapter;
import com.freebee.android.main.QuestionsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Question5Fragment extends Fragment implements View.OnClickListener {

    QuestionsActivity activity;
    View view;

    TextView txv_industries, txv_types;

    public String[] INDUSTRY_CATEGORIES = null;
    public String[] OCCUPATION_CATEGORIES = null;
    int[] strIndustryIds = {R.string.select_Industry, R.string.home } ;
    int[] strOccupationIds = {R.string.select_occupation, R.string.home } ;
    int[] _selecttIdx = {-1 ,-1};

    int category_id = 0;

    HashMap<String, ArrayList<String>> industryList = new HashMap<String,ArrayList<String>>();
    ArrayList<String> industryArray = new ArrayList<>();

    public Question5Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question5, container, false);

        INDUSTRY_CATEGORIES =  getResources().getStringArray(R.array.Industries);
        OCCUPATION_CATEGORIES =  getResources().getStringArray(R.array.Occupation);

        getOccupation();
        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_industries = (TextView) view.findViewById(R.id.txv_industries);
        txv_industries.setOnClickListener(this);

        txv_types = (TextView)view.findViewById(R.id.txv_types);
        txv_types.setOnClickListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showIndustriesDialog() {

        ArrayList<String> industries = new ArrayList<>();
        for (String key : industryList.keySet().stream().sorted().collect(Collectors.toList())){
            industries.add(key);
        }

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(activity, industries);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    /*result.add(categoriesArray[checkedItems.keyAt(i)]);*/

                    result.add(industries.get(checkedItems.keyAt(i)));
                }

                industryArray.addAll(result);

                txv_industries.setText(TextUtils.join(", ", result));
                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public void showTypesDialog() {

        ArrayList<String> occupations = new ArrayList<>();

        if (industryList.size() > 0){
            for (int i = 0; i < mergeOccupation(industryArray).size(); i++){
                occupations.add(mergeOccupation(industryArray).get(i));
            }
        }

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_cities);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ListView lst_category = (ListView)dialog.findViewById(R.id.lst_cities);
        CitiesSelectListAdapter adapter = new CitiesSelectListAdapter(activity, occupations);

        lst_category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lst_category.setAdapter(adapter);

        Button btn_select = (Button)dialog.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> result = new ArrayList<>();
                SparseBooleanArray checkedItems = lst_category.getCheckedItemPositions();

                for (int i = 0; i < checkedItems.size(); i++){

                    /*result.add(categoriesArray[checkedItems.keyAt(i)]);*/

                    result.add(occupations.get(checkedItems.keyAt(i)));
                }

                txv_types.setText(TextUtils.join(", ", result));
                dialog.dismiss();
            }
        });

        Button btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private ArrayList<String> mergeOccupation(List<String> industries) {
        ArrayList<String> occupations = new ArrayList<>();
        for (String occupation : industries) {
            // TOTO:: Check the null point exception
            try {
                occupations.addAll(industryList.get(occupation));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return occupations;
    }


    public String loadJSONOccupation() {

        String json = null;
        try {
            InputStream is = activity.getApplication().getAssets().open("occupation.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            for (int i = 0; i < json.length(); i++){

            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void getOccupation(){

        try {
            JSONObject firstObject = new JSONObject(loadJSONOccupation());
            Iterator iterator = firstObject.keys();
            while ( iterator.hasNext()) {

                String key = (String)iterator.next();

                JSONArray cityArr = firstObject.getJSONArray(key);
                ArrayList cities = new ArrayList();

                for (int i = 0 ; i < cityArr.length(); i++) {

                    String city = cityArr.getString(i);
                    cities.add(city);
                }

                industryList.put(key, cities);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_industries:
                showIndustriesDialog();
                break;

            case R.id.txv_types:
                showTypesDialog();;
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity) context;

    }
}
