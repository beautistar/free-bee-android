package com.freebee.android.main;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.adapter.ListOrderConfirmAdapter;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderConfirmActivity extends BaseActivity {

    ImageView imv_back;
    TextView txv_user_name, txv_name, txv_address, txv_city, txv_state, txv_complete_order;
    ListView lst_order;
    ListOrderConfirmAdapter _adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirm);

        loadLayout();
    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderConfirmActivity.this, GetAddressActivity.class));
                finish();
            }
        });
        lst_order = (ListView)findViewById(R.id.lst_order);
        _adapter = new ListOrderConfirmAdapter(getApplicationContext(), Commons.orderProducts);
        lst_order.setAdapter(_adapter);

        txv_user_name = (TextView)findViewById(R.id.txv_user_name);
        txv_user_name.setText(Commons.g_user.get_name());
        txv_name = (TextView)findViewById(R.id.txv_name);
        txv_name.setText(Commons.shippingAddress.getName());
        txv_address = (TextView)findViewById(R.id.txv_address);
        txv_address.setText(Commons.shippingAddress.getAddress());
        txv_city = (TextView)findViewById(R.id.txv_city);
        txv_city.setText(Commons.shippingAddress.getCity() + ", " + Commons.shippingAddress.getZipcode());
        txv_state = (TextView)findViewById(R.id.txv_state);
        txv_state.setText(Commons.shippingAddress.getState());

        txv_complete_order = (TextView)findViewById(R.id.txv_complete_order);
        txv_complete_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkOut();
            }
        });

    }

    private void checkOut(){

        ArrayList<ProductEntity> order = Commons.orderProducts;
        showProgress();
        Map<String, String> params1 = new HashMap<>();

        JSONArray array = new JSONArray();

        for(int i=0;i<order.size();i++){
            JSONObject obj=new JSONObject();
            try {
                obj.put("product_id",order.get(i).get_id());
                obj.put("count",order.get(i).get_cart_count());
                obj.put("type", order.get(i).getType());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            array.put(obj);
        }

        params1.put("order_list", array.toString());
        params1.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
        params1.put("address_id", String.valueOf(Commons.shippingAddress.getId()));

        String url = ReqConst.SERVER_URL + ReqConst.REQ_CHECKOUT;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params1), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                parseCheckOutResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(request,url);

    }

    private void parseCheckOutResponse(JSONObject response){

        closeProgress();

        try {

            int resultCode = response.getInt(ReqConst.RES_CODE);

            if (resultCode == ReqConst.CODE_SUCCESS){

                startActivity(new Intent(OrderConfirmActivity.this, MyBundleActivity.class));
                finish();
            }
            else if (resultCode == 209){

                String resultMsg = response.getString(ReqConst.RES_MSG);
                showToast(resultMsg);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
