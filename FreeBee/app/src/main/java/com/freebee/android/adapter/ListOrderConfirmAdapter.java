package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.ShipAddressModel;

import java.util.ArrayList;

public class ListOrderConfirmAdapter extends BaseAdapter {

    ArrayList<ProductEntity> orderProduct = new ArrayList<>();
    Context context;


    public ListOrderConfirmAdapter(Context context, ArrayList<ProductEntity> models){

        this.context = context;
        orderProduct = models;
    }

    @Override
    public int getCount() {
        return orderProduct.size();
    }

    @Override
    public Object getItem(int position) {
        return orderProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_order_confirm_list, parent, false);

            viewHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            viewHolder.txv_advertisers = (TextView)convertView.findViewById(R.id.txv_advertisers);
            viewHolder.txv_quantity = (TextView)convertView.findViewById(R.id.txv_quantity);
            viewHolder.txv_type = (TextView)convertView.findViewById(R.id.txv_type);

            convertView.setTag(viewHolder);
        }
        else {

            viewHolder = (ViewHolder)convertView.getTag();
        }

        ProductEntity order = (ProductEntity) orderProduct.get(position);

        viewHolder.txv_name.setText(order.get_name());
        viewHolder.txv_advertisers.setText(order.get_ownerName());
        viewHolder.txv_quantity.setText(String.valueOf(order.get_cart_count()));
        viewHolder.txv_type.setText(order.getType());

        return convertView;
    }

    private class ViewHolder{

        TextView txv_name, txv_advertisers, txv_quantity, txv_type;
    }
}
