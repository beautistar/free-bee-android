package com.freebee.android.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.freebee.android.R;


/**
 * Created by GoldRain on 10/8/2016.
 */

public class ViewPagerAdapter_MyProfile extends FragmentStatePagerAdapter {

    Context _context;

    public ViewPagerAdapter_MyProfile (Context context , FragmentManager fm) {
        //public ViewPagerAdapter_MyProfile (Context context , FragmentManager fm) {

        super(fm);

        this._context = context;

    }

    @Override
    public int getItemPosition(Object object) {return super.getItemPosition(object);}

    @Override
    public Fragment getItem(int position) {

        Fragment frag = null ;
        switch (position){

           /* case 0:

                frag = new ListedFragment(_context);

                break;

            case 1:
                frag = new LikedFragment(_context);

                break;

            case 2:
                frag = new BoughtFragment(_context);

                break;*/
        }
        return frag;
    }

    @Override
    public int getCount() {return 3;}

    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title= _context.getString(R.string.listed_pro);
                break;
            case 1:
                title= _context.getString(R.string.liked_pro);
                break;
            case 2:
                title=_context.getString(R.string.claimed_pro);
                break;
        }

        return title;
    }

 }
