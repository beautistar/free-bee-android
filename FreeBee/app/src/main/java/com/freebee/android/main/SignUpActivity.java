package com.freebee.android.main;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ProductEntity;
import com.freebee.android.model.UserEntity;
import com.freebee.android.preference.PrefConst;
import com.freebee.android.preference.Preference;
import com.freebee.android.utils.BitmapUtils;
import com.freebee.android.utils.CircleImageView;
import com.freebee.android.utils.MultiPartRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pixplicity.easyprefs.library.Prefs;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.freebee.android.commons.ReqConst.CODE_SUCCESS;

public class SignUpActivity extends CommonActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {


    EditText ui_edtEmail, ui_edtPassword, ui_edtName, ui_phone, edt_address, edt_city, edt_state, edt_zipCode, edt_age, edt_gender;
    TextView txv_signup, txv_agree;
    ImageView ui_imvBack, imv_upload, imv_next, imv_fb, imv_google;
    CircleImageView imv_avatar;
    CheckBox checkbox;
    int _idx = 0;

    Uri _imageCaptureUri;
    String _photoPath;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    public static CallbackManager callbackManager;
    public static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    private String FEmail, Fname,Firstname, Lastname,Fid,Fphone,Image_url;
    String photourl = "";

    AppEventsLogger logger;
    boolean fromSocial = false; //false: normal, true: social
    int[] strIds = {R.string.select_categories, R.string.home } ;

    String[] userInfoAlert = {"Please input your phone number.", "Please input your address.", "Please input your city.",
            "Please input your state.", "Please input your zip code.","Please input your age.", "Please input your gender."};

    String phone = "", address = "", city = "", state = "", zip_code = "", age = "", gender = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Commons.g_user = new UserEntity();
        GENDER_CATEGORIES =  getResources().getStringArray(R.array.Gender);
        FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        loadLyout();
    }

    private void loadLyout() {

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPassword = (EditText)findViewById(R.id.edt_password);
        ui_edtName = (EditText)findViewById(R.id.edt_name);
        ui_phone = (EditText) findViewById(R.id.edt_phone);
        edt_address = (EditText)findViewById(R.id.edt_address);
        edt_city = (EditText)findViewById(R.id.edt_city);
        edt_state = (EditText)findViewById(R.id.edt_state);
        edt_zipCode = (EditText)findViewById(R.id.edt_zipCode);
        edt_age = (EditText)findViewById(R.id.edt_age);
        edt_gender = (EditText)findViewById(R.id.edt_gender);

        txv_signup = (TextView) findViewById(R.id.txv_signup);
        txv_signup.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setColorFilter(ContextCompat.getColor(this, R.color.black_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
        ui_imvBack.setOnClickListener(this);

        imv_avatar = (CircleImageView) findViewById(R.id.imv_avatar);
        imv_avatar.setOnClickListener(this);

        imv_upload =(ImageView) findViewById(R.id.imv_upload);
        imv_upload.setOnClickListener(this);

        imv_next = (ImageView)findViewById(R.id.imv_next);
        imv_next.setOnClickListener(this);

        imv_fb = (ImageView)findViewById(R.id.imv_fb);
        imv_fb.setOnClickListener(this);

        imv_google = (ImageView)findViewById(R.id.imv_google);
        imv_google.setOnClickListener(this);

        /*checkbox = (CheckBox)findViewById(R.id.checkBox1);
        txv_agree = (TextView)findViewById(R.id.txv_agree_terms);
        txv_agree.setClickable(true);
        txv_agree.setOnClickListener(this);
        txv_agree.setMovementMethod(LinkMovementMethod.getInstance());*/

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container_singup);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtName.getWindowToken(),0);
                return false;
            }
        });
    }

    private void gotoSignup(){

        fromSocial = false;
        try{
            showProgress();

            File file = null;
            file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.REQ_EMAIL, ui_edtEmail.getText().toString());
            params.put(ReqConst.REQ_PASSWORD, ui_edtPassword.getText().toString());
            params.put(ReqConst.REQ_USERNAME, ui_edtName.getText().toString());
            params.put(ReqConst.REQ_PHONE, ui_phone.getText().toString());
            params.put(ReqConst.REQ_ADDRESS, edt_address.getText().toString());
            params.put(ReqConst.REQ_CITY, edt_city.getText().toString());
            params.put(ReqConst.REQ_STATE, edt_state.getText().toString());
            params.put(ReqConst.REQ_ZIP_CODE, edt_zipCode.getText().toString());
            params.put(ReqConst.REQ_AGE, edt_age.getText().toString());
            params.put(ReqConst.REQ_GENDER, edt_gender.getText().toString());

            String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTER;

            MultiPartRequest multiPartRequest = new MultiPartRequest(url, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (isFinishing()) {
                        closeProgress();
                        showAlertDialog(error.getMessage());
                    }
                }
            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    parseSignUp(response);
                }
            }, file, ReqConst.REQ_PARAM_FILE, params);

            multiPartRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            FreeBeeApplication.getInstance().addToRequestQueue(multiPartRequest, url);

        }catch (Exception e){
            closeProgress();
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }
    }

    /*private void parseSignUp(String response){

        Log.d("Respnse===>", response);

        try {
            JSONObject object = new JSONObject(response);
            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _idx = object.getInt(ReqConst.RES_ID);
                Commons.g_user.set_idx(_idx);

                loginSuccess();

            }else if (result_code == ReqConst.CODE_EXISTEMAIL){

                closeProgress();
                showAlertDialog(getString(R.string.exist_email));
            }

        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

    }*/

    private void gotoQuestionActivity(){

        Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, ui_edtEmail.getText().toString().trim());
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, ui_edtPassword.getText().toString().trim());

        //setResult(RESULT_OK);
        closeProgress();
        startActivity(new Intent(this, QuestionsActivity.class));
        finish();
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();

                } else if (item == 1){
                    doTakeGallery();

                } else return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        _photoPath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Crop.REQUEST_CROP: {

                if (resultCode == RESULT_OK){
                    try {

                        File outFile = BitmapUtils.getOutputMediaFile(this, "temp.png");

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                        Bitmap returnedBitmap = bitmap;

                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                bitmap.recycle();
                                bitmap = null;
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                bitmap.recycle();
                                bitmap = null;
                                break;

                            default:
                                break;
                        }

                        Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, returnedBitmap.getWidth(), returnedBitmap.getHeight(), true);
                        File newFile = BitmapUtils.getOutputMediaFile(this, System.currentTimeMillis() + ".png");
                        BitmapUtils.saveOutput(newFile, w_bmpSizeLimited);
                        _photoPath = newFile.getAbsolutePath();
                        Log.d("photopath===", _photoPath);
                       // imv_avatar(_photoPath);

                        if (_photoPath.length() > 0){
                            imv_avatar.setImageBitmap(w_bmpSizeLimited);
                            imv_upload.setVisibility(View.GONE);
                        }else imv_upload.setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }


            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                    beginCrop(_imageCaptureUri);
                }
                break;

            case Constants.PICK_FROM_CAMERA:
            {
                if (resultCode == RESULT_OK){
                    try {
                        String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                        Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);
                        String w_strFilePath = "";
                        String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                        if (w_strLimitedImageFilePath != null) {
                            w_strFilePath = w_strLimitedImageFilePath;
                        }

                        _photoPath = w_strFilePath;
                        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
                        //  _photoPath= BitmapUtils.getSizeLimitedImageFilePath(_photoPath);
                        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

                        beginCrop(_imageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                break;
            }

            case RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;

        }

    }
    private void beginCrop(Uri source) {
        // setToOther(true);
        Uri destination = Uri.fromFile(BitmapUtils.getOutputMediaFile(this, "temp.png"));//
        // Crop.of(source, destination).asSquare().start(this);
        Crop.of(source, destination).withMaxSize(840, 1024).start(this);
    }

    public boolean checkUserInfoValid(int [] userInfo){

        for (int i = 0; i < userInfo.length; i++){

            if (userInfo[i] == 0){
                showAlertDialog(userInfoAlert[i]);
                return false;
            }
        }

        return true;
    }
    public boolean checkValid() {

        if (ui_edtEmail.getText().length() == 0) {
            showAlertDialog(getString(R.string.input_email));
            return false;

        } else if (ui_edtPassword.getText().length() == 0) {
            showAlertDialog(getString(R.string.input_password));
            return false;

        } else if (ui_edtName.getText().length() == 0){

            showAlertDialog(getString(R.string.input_name));
            return false;
        } else if (edt_address.getText().toString().length() == 0){

            showAlertDialog("Please input your address.");
            return false;
        }else if (edt_state.getText().length() == 0){

            showAlertDialog("Please input your state.");
            return false;
        } else if (edt_city.getText().length() == 0){

            showAlertDialog("Please input your city.");
            return false;
        } else if (edt_zipCode.getText().length() == 0){

            showAlertDialog("Please input your zip code.");
            return false;
        } else if (edt_age.getText().toString().length() == 0){

            showAlertDialog("Please input your age.");
            return false;
        } else if (edt_gender.getText().toString().length() == 0){

            showAlertDialog("Please input your gender.");
            return false;
        } else if (ui_phone.getText().toString().length() == 0){

            showAlertDialog("Please input your phone number.");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                startActivity(new Intent(this, SignInActivity.class));
                finish();
                break;

            case R.id.imv_upload:

            case R.id.imv_avatar:

                if (!hasPermissions(this, PERMISSIONS)){

                    ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.MY_PEQUEST_CODE);
                }else {
                    selectPhoto();}

                break;

            case R.id.imv_next:

                if (checkValid())
                    gotoSignup();

                break;

            case R.id.imv_fb:

                facebookVerify();
                break;

            case R.id.imv_google:
                googleVerify();
                break;

            /*case R.id.txv_agree_terms:
                checkbox.setChecked(true);
                gotoAgreeTermActivity();
                break;*/
        }

    }

    private void gotoAgreeTermActivity() {

        Intent intent = new Intent(this, AgreeTerms.class);
        overridePendingTransition(0,0);
        startActivity(intent);
    }

    /*==================== CARMERA Permission========================================*/
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    private void googleVerify() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }



    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {

        Log.d("handSignResult ==> ", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            GoogleLogin(acct);

            Log.d("potoURL=========", String.valueOf(acct.getPhotoUrl()));
            Log.d("Email===>", acct.getEmail());
            Log.d("Full name===>", acct.getDisplayName());
            Log.d("ID==>" , acct.getId());


        } else {

        }
    }

    /*===============================Google+ Login Start ===============================*/

    private void GoogleLogin(GoogleSignInAccount result){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SOCIAL_SIGNUP;

        fromSocial = true;

        String photo_url = "";
        if (result.getPhotoUrl() != null && !result.getPhotoUrl().equals("null"))
            photo_url = result.getPhotoUrl().getPath();

        String finalPhoto_url = photo_url;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSignUp(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    //params.put(ReqConst.RES_USERID,String.valueOf(Commons.g_user.get_idx()));

                    params.put(ReqConst.REQ_USERNAME, result.getDisplayName());
                    params.put(ReqConst.REQ_EMAIL, result.getEmail());
                    params.put(ReqConst.REQ_SOCIAL_ID, String.valueOf(result.getId()));
                    params.put(ReqConst.REQ_PHOTO_URL, finalPhoto_url);
                    params.put(ReqConst.REQ_SOCIAL_TYPE, "google");

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);  // url or "tag"
    }


    /******************************FB LOGIN**************************/

    private void facebookVerify(){

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.v("LoginActivity Response ", response.toString());

                        try {
                            Fname = object.getString("name");
                            Fid = object.getString("id");

                            FEmail = object.getString("email");
                            if (FEmail.length() == 0){
                                FEmail = Fid + "@facebook.com";
                            }


                            //Image_url = "http://graph.facebook.com/(Id)/picture?type=large";

                            //Fphone = object.getString("phone_number");

                            Log.d("Email = ", " " + FEmail);
                            //Log.d("phoneNumber = ", " " + Fphone);
                            //Log.d("Gender======",Gender);

                            photourl = "http://graph.facebook.com/"+Fid+"/picture?type=large";
                            photourl = URLEncoder.encode(photourl);

                            facebookLogin();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

                Log.d("facebook error", error.toString());

            }

        });
    }

    public void facebookLogin(){

        showProgress();

        fromSocial = true;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SOCIAL_SIGNUP;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));

                parseSignUp(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    //params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
                    params.put(ReqConst.REQ_USERNAME, Fname);
                    params.put(ReqConst.REQ_EMAIL, FEmail);
                    params.put(ReqConst.REQ_SOCIAL_ID, String.valueOf(Fid));
                    params.put(ReqConst.REQ_PHOTO_URL, photourl);
                    params.put(ReqConst.REQ_SOCIAL_TYPE, "facebook");

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseSignUp(String response){

        Log.d("=google+==response==", response);

        try {

            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONObject userProfile = object.getJSONObject(ReqConst.RES_USERPROFILE);

                UserEntity _user = new UserEntity();

                _user.set_idx(userProfile.getInt(ReqConst.RES_USERID));
                _user.set_name(userProfile.getString(ReqConst.RES_USERNAME));
                _user.set_imageUrl(userProfile.getString(ReqConst.RES_USERIMAGE));
                _user.set_count_followers(userProfile.getInt(ReqConst.RES_COUNTFOLLOWERS));
                _user.set_count_following(userProfile.getInt(ReqConst.RES_COUNTFOLLOWING));
                _user.set_isVerified(userProfile.getInt(ReqConst.RES_ISVERIFIED));
                _user.set_verify_facebook(userProfile.getString(ReqConst.RES_VERIFYFACEBOOK));
                _user.set_verify_google(userProfile.getString(ReqConst.RES_VERIFYGOOGLE));

                _user.set_phone(userProfile.getString(ReqConst.REQ_PHONE));
                _user.setAddress(userProfile.getString("address"));
                _user.setCity(userProfile.getString("city"));
                _user.setState(userProfile.getString("state"));
                _user.setZip_code(userProfile.getString("zip_code"));
                _user.setAge(userProfile.getInt("age"));
                _user.setGender(userProfile.getString("gender"));

                JSONArray jsonArray_products = userProfile.getJSONArray(ReqConst.RES_LISTOFPRODUCTS);
                ArrayList<ProductEntity> list_of_product = new ArrayList<>();

                for (int i = 0; i < jsonArray_products.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray_products.get(i);

                    ProductEntity _list = new ProductEntity();

                    _list.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _list.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _list.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_product.add(_list);
                }

                JSONArray product_likes = userProfile.getJSONArray(ReqConst.RES_PRODUCTLIKES);
                ArrayList<ProductEntity> list_of_like = new ArrayList<>();

                for (int i = 0; i < product_likes.length(); i++){

                    JSONObject jsonObject = (JSONObject) product_likes.get(i);

                    ProductEntity _like = new ProductEntity();

                    _like.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _like.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _like.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_like.add(_like);
                }

                JSONArray product_bought = userProfile.getJSONArray(ReqConst.RES_PRODUCTBOUGHT);
                ArrayList<ProductEntity> list_of_bought = new ArrayList<>();

                for (int i = 0; i < product_bought.length(); i++){

                    JSONObject jsonObject = (JSONObject)product_bought.get(i);

                    ProductEntity _bought = new ProductEntity();

                    _bought.set_id(jsonObject.getInt(ReqConst.RES_PRODUCT_ID));
                    _bought.set_name(jsonObject.getString(ReqConst.RES_PRODUCT_NAME));
                    _bought.set_imagUrl(jsonObject.getString(ReqConst.RES_PRODUCT_IMAGE));

                    list_of_bought.add(_bought);
                }

                Commons.g_user = _user;
                Commons.g_user.set_listProducts(list_of_product);
                Commons.g_user.set_boughtProducts(list_of_bought);
                Commons.g_user.set_likedProducts(list_of_like);

                if (fromSocial){

                    gotoRegisterUserInfo();

                }else
                    gotoQuestionActivity();

            } else if (result_code == ReqConst.CODE_EXISTEMAIL){

                showAlertDialog(object.getString(ReqConst.RES_ERROR));
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }
    }

    private void gotoRegisterUserInfo(){

        int[] strings = new int[7];

        final String[] _gender = {""};
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pick_register_userinfo);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        EditText edt_phone = (EditText)dialog.findViewById(R.id.edt_phone);
        phone = edt_phone.getText().toString();
        EditText edt_address = (EditText)dialog.findViewById(R.id.edt_address);
        address = edt_address.getText().toString();
        EditText edt_city = (EditText)dialog.findViewById(R.id.edt_city);
        city = edt_city.getText().toString();
        EditText edt_state = (EditText)dialog.findViewById(R.id.edt_state);
        state = edt_state.getText().toString();
        EditText edt_zip_code = (EditText)dialog.findViewById(R.id.edt_zipCode);
        zip_code = edt_zip_code.getText().toString();
        EditText edt_age = (EditText)dialog.findViewById(R.id.edt_age);
        age = edt_age.getText().toString();
        //EditText edt_gender = (EditText)dialog.findViewById(R.id.edt_gender);

        final TextView txv_gender = (TextView) dialog.findViewById(R.id.txv_gender);
        txv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chooseGender(0);

                String[][] itemsArray = { GENDER_CATEGORIES} ;
                final String[]  items = itemsArray[0];

                final TextView txv = txv_gender;

                AlertDialog.Builder builder = new AlertDialog.Builder(dialog.getContext());
                builder.setTitle(strIds[0]);
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int item) {

                        txv.setText(items[item]);
                        _gender[0] = items[item];
                        gender = txv.getText().toString();

               /* _selecttIdx[idx] = item;
                category_id = item;*/

                    }
                });

                AlertDialog _alert = builder.create();

                _alert.show();
            }
        });



        Button btn_save = (Button)dialog.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strings[0] = phone.length();
                strings[1] = address.length();
                strings[2] = city.length();
                strings[3] = state.length();
                strings[4] = zip_code.length();
                strings[5] = age.length();
                strings[6] = gender.length();

                if(checkUserInfoValid(strings)){

                    String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTER_USER_INFO;

                    Log.d("=======URL==========", url);

                    showProgress();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            parseRegisterUserInfo(response);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            showAlertDialog(getString(R.string.error));
                            closeProgress();
                        }

                    })  {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            try {
                                params.put(ReqConst.REQ_PARAM_ID, String.valueOf(Commons.g_user.get_idx()));
                                params.put(ReqConst.REQ_PHONE, phone);
                                params.put(ReqConst.REQ_ADDRESS, address);
                                params.put(ReqConst.REQ_CITY, city);
                                params.put(ReqConst.REQ_STATE, state);
                                params.put(ReqConst.REQ_ZIP_CODE, zip_code);
                                params.put(ReqConst.REQ_AGE, age);
                                params.put(ReqConst.REQ_GENDER, gender);

                            } catch (Exception e) {}
                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
                    dialog.dismiss();
                }


            }
        });

        dialog.show();
    }


    private void parseRegisterUserInfo(String response){

        try {

            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                fromSocial = false;

                Commons.g_user.set_phone(phone);
                Commons.g_user.setAddress(address);
                Commons.g_user.setCity(city);
                Commons.g_user.setState(state);
                Commons.g_user.setZip_code(zip_code);
                Commons.g_user.setAge(Integer.parseInt(age));
                Commons.g_user.setGender(gender);
                registerToken();
                gotoQuestionActivity();
                finish();

            } else {

                showAlertDialog(getString(R.string.error));
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }

    }

    private void registerToken(){

        String url = ReqConst.SERVER_URL + "register_token";
        if (Prefs.getString(Constants.TOKEN, "").length() == 0){
            return;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("userid", String.valueOf(Commons.g_user.get_idx()));
                    params.put("token", Prefs.getString(Constants.TOKEN, ""));

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);
    }
    public String[] GENDER_CATEGORIES = null;

    /*private void chooseGender(int idx){

        String[][] itemsArray = { GENDER_CATEGORIES} ;
        final String[]  items = itemsArray[idx];

        final TextView txv = txv_gender;

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(strIds[idx]);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int item) {

                txv.setText(items[item]);
                gender = items[item];

               *//* _selecttIdx[idx] = item;
                category_id = item;*//*

            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();

        alert.show();
    }*/

    private void parseVerifyFacebookResponse(String response){

        try {

            Log.d("=facebook===response", response);
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity user = new UserEntity();
                JSONObject jsonUser = object.getJSONObject(ReqConst.RES_USERPROFILE);

                user.set_idx(jsonUser.getInt(ReqConst.RES_USERID));
                user.set_name(jsonUser.getString(ReqConst.RES_USERNAME));

                Commons.g_user.set_verify_facebook("Y");

            }
        } catch (JSONException e) {

            closeProgress();
            showAlertDialog("Network Error.Please try again.");
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        onExit();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
