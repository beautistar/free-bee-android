package com.freebee.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;

import java.util.ArrayList;

public class Question4Fragment extends Fragment implements View.OnClickListener {

    QuestionsActivity activity;
    View view;

    TextView txv_yes, txv_no, txv_dog, txv_cat, txv_bird, txv_fish, txv_others;
    LinearLayout lyt_pet;

    ArrayList<TextView> arrayPetView = new ArrayList<>();

    boolean dog = false, cat = false, bird = false, fish = false, other = false;

    ArrayList<Integer> selectedItems = new ArrayList<Integer>();

    public Question4Fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_question4, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        lyt_pet = (LinearLayout)view.findViewById(R.id.lyt_pets);

        txv_yes = (TextView)view.findViewById(R.id.txv_yes);
        txv_yes.setOnClickListener(this);

        txv_no = (TextView)view.findViewById(R.id.txv_no);
        txv_no.setOnClickListener(this);

        txv_dog = (TextView)view.findViewById(R.id.txv_dog);
        txv_dog.setOnClickListener(this);
        arrayPetView.add(txv_dog);
        txv_cat = (TextView)view.findViewById(R.id.txv_cat);
        txv_cat.setOnClickListener(this);
        arrayPetView.add(txv_cat);
        txv_bird = (TextView)view.findViewById(R.id.txv_bird);
        txv_bird.setOnClickListener(this);
        arrayPetView.add(txv_bird);
        txv_fish = (TextView)view.findViewById(R.id.txv_fish);
        txv_fish.setOnClickListener(this);
        arrayPetView.add(txv_fish);
        txv_others = (TextView)view.findViewById(R.id.txv_others);
        txv_others.setOnClickListener(this);
        arrayPetView.add(txv_others);

        lyt_pet.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_yes:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));
                lyt_pet.setVisibility(View.VISIBLE);

                break;

            case R.id.txv_no:

                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));
                lyt_pet.setVisibility(View.GONE);

                break;

            case R.id.txv_dog:

                if (dog) dog = false;
                else dog = true;
                selectPet(0, dog);

                break;

            case R.id.txv_cat:
                if (cat) cat = false;
                else cat = true;
                selectPet(1, cat);
                break;

            case R.id.txv_bird:
                if (bird) bird = false;
                else bird = true;
                selectPet(2, bird);
                break;

            case R.id.txv_fish:
                if (fish) fish = false;
                else fish = true;
                selectPet(3, fish);
                break;

            case R.id.txv_others:
                if (other) other = false;
                else other = true;
                selectPet(4, other);
                break;

        }
    }

    private void setBackground(int a, int b, int e, int f){

        txv_yes.setBackgroundResource(a);
        txv_no.setBackgroundResource(b);

        txv_yes.setTextColor(e);
        txv_no.setTextColor(f);

    }

    private void selectPet(int num, boolean click){

        if (click){

            arrayPetView.get(num).setBackgroundResource(R.drawable.signup_round);
            arrayPetView.get(num).setTextColor(getResources().getColor(R.color.white));
        }
        else {
            arrayPetView.get(num).setBackgroundResource(R.drawable.item_unselect_fillrect);
            arrayPetView.get(num).setTextColor(getResources().getColor(R.color.black));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (QuestionsActivity)context;

    }
}
