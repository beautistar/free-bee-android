package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;
import com.freebee.android.model.UserEntity;

import java.util.ArrayList;

public class GridViewChildAdapter extends BaseAdapter {

    QuestionsActivity _activity;
    ArrayList<UserEntity> _user = new ArrayList<>();

    int _child =0;

    int selectedYes = 0;
    int selectedNo = 0;

    public GridViewChildAdapter(QuestionsActivity activity, int child){

        this._activity = activity;
        this._child = child;

    }


    @Override
    public int getCount() {
        return _child;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ChildHolder childHolder;

        if (view == null){

            childHolder = new ChildHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_child_gridview, viewGroup, false);

            childHolder.txv_child = (TextView)view.findViewById(R.id.txv_child);
            childHolder.txv_male = (TextView)view.findViewById(R.id.txv_male);
            childHolder.txv_female = (TextView)view.findViewById(R.id.txv_female);
            childHolder.edt_age = (EditText) view.findViewById(R.id.edt_age);

            view.setTag(childHolder);
        }

        else {
            childHolder = (ChildHolder) view.getTag();
        }


        childHolder.txv_child.setText(String.valueOf(i+1));

        childHolder.txv_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                childHolder.setBackground(R.drawable.signup_round, R.drawable.item_unselect_fillrect,
                        view.getResources().getColor(R.color.white), view.getResources().getColor(R.color.black));

            }
        });

        childHolder.txv_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    childHolder.setBackground(R.drawable.item_unselect_fillrect, R.drawable.signup_round,view.getResources().getColor(R.color.black),
                            view.getResources().getColor(R.color.white));

            }
        });

        return view;
    }

    public class ChildHolder{

        TextView txv_child, txv_male, txv_female;
        EditText edt_age;

        private void setBackground(int a, int b, int e, int f){

            txv_male.setBackgroundResource(a);
            txv_female.setBackgroundResource(b);
            txv_male.setTextColor(e);
            txv_female.setTextColor(f);
        }
    }


}
