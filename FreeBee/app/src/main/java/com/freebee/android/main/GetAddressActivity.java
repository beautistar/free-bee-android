package com.freebee.android.main;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.R;
import com.freebee.android.adapter.ListViewAddressAdapter;
import com.freebee.android.base.BaseActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.model.ShipAddressModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetAddressActivity extends BaseActivity {

    ListView lst_address;
    ListViewAddressAdapter _adapter;
    ImageView imv_back;
    TextView txv_add, txv_select_address;

    ArrayList<ShipAddressModel> shipAddress = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_address);

        loadLayout();
        getAddressList();
    }

    private void loadLayout() {

        lst_address = (ListView)findViewById(R.id.lst_address);
        _adapter = new ListViewAddressAdapter(getApplicationContext());


        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetAddressActivity.this, MyBundleActivity.class));
                finish();
            }
        });

        txv_add = (TextView)findViewById(R.id.txv_add);
        txv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GetAddressActivity.this, AddAddressActivity.class));

            }
        });

        txv_select_address = (TextView)findViewById(R.id.txv_select_address);
        txv_select_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Commons.shippingAddress == null){

                    showToast("Select Shipping Address");
                }else {

                    startActivity(new Intent(GetAddressActivity.this, OrderConfirmActivity.class));
                    finish();
                }

            }
        });



    }

    private void getAddressList(){


        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_ADDRESS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseGetAddress(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {
                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void parseGetAddress(String response){


        closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray jsonArray = object.getJSONArray(ReqConst.RES_ADDRESS_LIST);

                for (int i = 0; i < jsonArray.length(); i++){

                    JSONObject shipJson =  (JSONObject)jsonArray.get(i);

                    ShipAddressModel ship = new ShipAddressModel();

                    ship.setId(shipJson.getInt(ReqConst.RES_ID));
                    ship.setUserid(shipJson.getInt(ReqConst.RES_USERID));
                    ship.setName(shipJson.getString("name"));
                    ship.setAddress(shipJson.getString("address"));
                    ship.setCity(shipJson.getString("city"));
                    ship.setState(shipJson.getString("state"));
                    ship.setZipcode(shipJson.getString("zipcode"));

                    shipAddress.add(ship);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        _adapter.setAddress(shipAddress);
                        lst_address.setAdapter(_adapter);
                    }
                });

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
