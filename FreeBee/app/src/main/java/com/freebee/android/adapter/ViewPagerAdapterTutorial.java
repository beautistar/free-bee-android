package com.freebee.android.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.freebee.android.fragment.FirstTutirialFragment;
import com.freebee.android.main.SplashActivity;

public class ViewPagerAdapterTutorial extends FragmentStatePagerAdapter {

    SplashActivity _context;

    public ViewPagerAdapterTutorial(FragmentManager fm , SplashActivity context) {
        super(fm);
        this._context = context;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){

            case 0:
                fragment = new FirstTutirialFragment(_context, position);
                break;

            case 1:
                fragment = new FirstTutirialFragment(_context,position);
                break;

            case 2:
                fragment = new FirstTutirialFragment(_context,position);
                break;

        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
