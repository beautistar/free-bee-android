package com.freebee.android.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.QuestionsActivity;

import java.util.ArrayList;

public class Question2Fragment extends Fragment implements View.OnClickListener{

    QuestionsActivity activity;
    View view;

    TextView txv_yes, txv_no,txv_married, txv_divorced, txv_single, txv_dating, txv_noRes;
    ArrayList<TextView> arrayStatusView = new ArrayList<>();
    LinearLayout lyt_status;


    public Question2Fragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =inflater.inflate(R.layout.fragment_question2, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_yes = (TextView)view.findViewById(R.id.txv_yes);
        txv_yes.setOnClickListener(this);
        txv_no = (TextView)view.findViewById(R.id.txv_no);
        txv_no.setOnClickListener(this);
        txv_yes.setVisibility(View.GONE);
        txv_no.setVisibility(View.GONE);

        txv_married = (TextView)view.findViewById(R.id.txv_married);
        txv_married.setOnClickListener(this);
        arrayStatusView.add(txv_married);

        txv_single = (TextView)view.findViewById(R.id.txv_single);
        txv_single.setOnClickListener(this);
        arrayStatusView.add(txv_single);

        txv_divorced = (TextView)view.findViewById(R.id.txv_divorced);
        txv_divorced.setOnClickListener(this);
        arrayStatusView.add(txv_divorced);

        txv_dating = (TextView)view.findViewById(R.id.txv_dating);
        txv_dating.setOnClickListener(this);
        arrayStatusView.add(txv_dating);

        txv_noRes = (TextView)view.findViewById(R.id.txv_noRes);
        txv_noRes.setOnClickListener(this);
        arrayStatusView.add(txv_noRes);

        lyt_status = (LinearLayout)view.findViewById(R.id.lyt_status);
        //lyt_status.setVisibility(View.GONE);
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_yes:

                setBackground(R.drawable.circle, R.drawable.circle_white, getResources().getColor(R.color.white), getResources().getColor(R.color.black));
                //lyt_status.setVisibility(View.VISIBLE);
               break;

            case R.id.txv_no:
                setBackground(R.drawable.circle_white, R.drawable.circle, getResources().getColor(R.color.black), getResources().getColor(R.color.white));
                //lyt_status.setVisibility(View.GONE);
                break;

            case R.id.txv_married:

                selectStatus(1);

                break;

            case R.id.txv_single:
                selectStatus(2);
                break;

            case R.id.txv_divorced:

                selectStatus(3);
                break;

            case R.id.txv_dating:
                selectStatus(4);
                break;

            case R.id.txv_noRes:
                selectStatus(5);
                break;
        }

    }

    private void setBackground(int a, int b, int e, int f){

        txv_yes.setBackgroundResource(a);
        txv_no.setBackgroundResource(b);
        txv_yes.setTextColor(e);
        txv_no.setTextColor(f);

    }

    private void selectStatus(int num){


        arrayStatusView.get(num-1).setBackgroundResource(R.drawable.signup_round);
        arrayStatusView.get(num-1).setTextColor(getResources().getColor(R.color.white));

        for (int i = 0; i < arrayStatusView.size() ; i++){

            if (i == (num - 1)) continue;
            arrayStatusView.get(i).setBackgroundResource(R.drawable.item_unselect_fillrect);
            arrayStatusView.get(i).setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        activity = (QuestionsActivity)context;
    }
}

