package com.freebee.android.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.freebee.android.R;
import com.freebee.android.FreeBeeApplication;
import com.freebee.android.base.CommonActivity;
import com.freebee.android.commons.Commons;
import com.freebee.android.commons.Constants;
import com.freebee.android.commons.ReqConst;
import com.freebee.android.utils.BitmapUtils;
import com.freebee.android.utils.MultiPartRequest;
import com.freebee.android.utils.RadiusImageView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends CommonActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    ImageView ui_imvBack, ui_imvFacebook, ui_imvGoogle, ui_imvPhone, ui_txvPhicture ;
    TextView ui_txvPicture ,ui_txvUpdate, ui_txvFacebook, ui_txvGoogle, ui_txvPhone, ui_txvFacebookLink,ui_txvGoogleLink, ui_txvPhoneLink ;
    EditText ui_edtName, ui_edtAbout;
    LinearLayout ui_lytFacebook, ui_lytGoogle, ui_lytPhone;
    RelativeLayout lyt_photo;

    Uri _imageCaptureUri;
    String _photoPath;

    RadiusImageView ui_imvPhoto;
    private int _idx = 0 ;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    public static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    public static CallbackManager callbackManager;

    private String FEmail, Name,Firstname, Lastname,Id,Gender,Image_url;

    String photourl;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        loadLayout();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        try
        {

            PackageInfo info = getPackageManager().getPackageInfo("com.freebee", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                Log.i("KeyHash::", Base64.encodeToString(md.digest(), Base64.DEFAULT));//will give developer key hash
                //Toast.makeText(getApplicationContext(), Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show(); //will give app key hash or release key hash
            }

        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {}

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        checkAllPermission();
    }

    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*==================== CARMERA Permission========================================*/
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
      /*////////////////////////////////////////////////////////////////*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvFacebook = (ImageView)findViewById(R.id.imv_facebook);
        ui_txvFacebook = (TextView)findViewById(R.id.txv_facebook);
        ui_txvFacebookLink = (TextView)findViewById(R.id.txv_facebookLink);

        ui_imvGoogle = (ImageView)findViewById(R.id.imv_google);
        ui_txvGoogle = (TextView)findViewById(R.id.txv_google);
        ui_txvGoogleLink = (TextView)findViewById(R.id.txv_googleLink);

        ui_imvPhone = (ImageView)findViewById(R.id.imv_phone);
        ui_txvPhone = (TextView)findViewById(R.id.txv_phone);
        ui_txvPhoneLink = (TextView)findViewById(R.id.txv_phoneLink);

        ui_edtAbout = (EditText)findViewById(R.id.edt_about);

        ui_edtName = (EditText)findViewById(R.id.edt_name);
        ui_txvPhicture = (ImageView)findViewById(R.id.txv_change_picture);
        ui_txvPhicture.setOnClickListener(this);

        ui_imvPhoto = (RadiusImageView) findViewById(R.id.imv_photo);

        ui_txvUpdate = (TextView)findViewById(R.id.txv_update);
        ui_txvUpdate.setOnClickListener(this);

        ui_lytFacebook = (LinearLayout)findViewById(R.id.lyt_facebook);
        ui_lytFacebook.setOnClickListener(this);

        ui_lytGoogle = (LinearLayout)findViewById(R.id.lyt_google);
        ui_lytGoogle.setOnClickListener(this);

        ui_lytPhone = (LinearLayout)findViewById(R.id.lyt_phone);
        ui_lytPhone.setOnClickListener(this);

        lyt_photo = (RelativeLayout)findViewById(R.id.lyt_photo);
        lyt_photo.setOnClickListener(this);

        setEditPage();
    }

    private void setEditPage(){

        if (Commons.g_user.get_imageUrl().length() != 0)
        Glide.with(this).load(Commons.g_user.get_imageUrl()).placeholder(R.drawable.ic_avatar).into(ui_imvPhoto);
        if (Commons.g_user.get_name().length() != 0) ui_edtName.setText(Commons.g_user.get_name());
        if (Commons.g_user.get_about().length() != 0) ui_edtAbout.setText(Commons.g_user.get_about());

        if (Commons.g_user.get_verify_facebook().equals("Y")){

            //ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_red);
            ui_txvFacebook.setTextColor(getResources().getColor(R.color.splash_bg));
            ui_txvFacebookLink.setTextColor(getResources().getColor(R.color.white));
            ui_txvFacebookLink.setText(getString(R.string.linked));

        } else {

            //ui_imvFacebook.setImageResource(R.drawable.ic_social_facebook_negative);
            ui_txvFacebook.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvFacebookLink.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvFacebookLink.setText(getString(R.string.link));
        }

        if (Commons.g_user.get_verify_google().equals("Y")){

            //ui_imvGoogle.setImageResource(R.drawable.ic_social_google_red);
            ui_txvGoogle.setTextColor(getResources().getColor(R.color.splash_bg));
            ui_txvGoogleLink.setTextColor(getResources().getColor(R.color.white));
            ui_txvGoogleLink.setText(getString(R.string.linked));

        } else {

            //ui_imvGoogle.setImageResource(R.drawable.ic_social_google_negative);
            ui_txvGoogle.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvGoogleLink.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvGoogleLink.setText(getString(R.string.link));
        }

        //phone verify
        if (Commons.g_user.get_verify_phone().equals("Y")){

            //ui_imvGoogle.setImageResource(R.drawable.ic_social_google_red);
            ui_txvPhone.setTextColor(getResources().getColor(R.color.splash_bg));
            ui_txvGoogleLink.setTextColor(getResources().getColor(R.color.white));
            ui_txvGoogleLink.setText(getString(R.string.linked));

        } else {

            //ui_imvGoogle.setImageResource(R.drawable.ic_social_google_negative);
            ui_txvGoogle.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvGoogleLink.setTextColor(getResources().getColor(R.color.text_grey));
            ui_txvGoogleLink.setText(getString(R.string.link));
        }
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    doTakePhoto();

                } else if (item == 1){
                    doTakeGallery();

                } else return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imvPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
            case RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                callbackManager.onActivityResult(requestCode, resultCode, data);
                Log.d("sdfsdfsd", String.valueOf(data));
                break;
        }
    }
/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




        if (requestCode == RC_SIGN_IN){


        }

    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoMyprofileActivity();
                break;

            case R.id.txv_change_picture:

                if (!hasPermissions(this, PERMISSIONS)){

                    ActivityCompat.requestPermissions(this, PERMISSIONS, Constants.MY_PEQUEST_CODE);
                }else {
                    selectPhoto();}

                break;

            case R.id.lyt_facebook:

                facebookVerify();

                break;
            case R.id.lyt_google:

                googleVerify();
                break;
            case R.id.lyt_phone:

                phoneVerify();
                break;

            case R.id.txv_update:
                //updateUserProfile();
                showToast("Coming soon");
                break;
        }

    }

    private void gotoMyprofileActivity() {
        finish();
    }

    private void updateUserProfile() {

       try{

           File file = new File(_photoPath);

           Map<String, String> params = new HashMap<>();

           params.put(ReqConst.RES_USERID, String.valueOf(Commons.g_user.get_idx()));
           params.put(ReqConst.RES_USERNAME, ui_edtName.getText().toString().trim());

           String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATEUSER ;
           showProgress();

           MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {

                   showAlertDialog(getString(R.string.error));
                   closeProgress();
               }
           }, new Response.Listener<String>() {

               @Override
               public void onResponse(String response) {
                   parseUpdateUserProfile(response);
               }

           }, file, ReqConst.REQ_PARAM_FILE, params);

           reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0 , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           FreeBeeApplication.getInstance().addToRequestQueue(reqMultiPart, url);

       } catch (Exception e){

           e.printStackTrace();
           closeProgress();
           showAlertDialog(getString(R.string.error));
       }
    }

    private void parseUpdateUserProfile(String response){

        Log.d("update profile res:", String.valueOf(response));

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                showToast("Success Update your profile");

                Commons.g_user.set_imageUrl(_photoPath);
                Commons.g_user.set_name(ui_edtName.getText().toString());
                Commons.g_user.set_about(ui_edtAbout.getText().toString());

                gotoMyprofileActivity();

                closeProgress();

            } else if (result_code == ReqConst.CODE_UPLOADFAIL){

                showAlertDialog("Failed Update your profile");
                closeProgress();

            } else {

                showAlertDialog(getString(R.string.error));
                closeProgress();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void googleVerify() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }



    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {

        Log.d("handSignResult ==> ", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            GoogleLogin();

            Log.d("potoURL=========", String.valueOf(acct.getPhotoUrl()));

        } else {

        }
    }

    /*===============================Google+ Login Start ===============================*/

    private void GoogleLogin(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_VERIFYGOOGLE;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseVerifyGoogle(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_USERID,String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {}
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(stringRequest, url);  // url or "tag"

    }

    private void parseVerifyGoogle(String response){

        Log.d("=google+==response==", response);

        try {
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Commons.g_user.set_verify_google("Y");
                setEditPage();
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
        }
    }

    //====================================Facebook Login Start======================================

    private void facebookVerify(){

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.v("LoginActivity Response ", response.toString());

                        try {
                            Name = object.getString("name");
                            Name.replace(" ", "");
                            Id=object.getString("id");
                            Firstname = object.getString("first_name");
                            Lastname = object.getString("last_name");
                            //Gender = object.getString("gender");

                            //Password= object.getString("password");
                            FEmail = object.getString("email");
                            Image_url = "http://graph.facebook.com/(Id)/picture?type=large";
                            Image_url = URLEncoder.encode(Image_url);

                            Log.d("Email = ", " " + FEmail);
                            Log.d("Name======",Name);
                            Log.d("firstName======",Firstname);
                            Log.d("lastName======",Lastname);
                            //Log.d("Gender======",Gender);
                            Log.d("id======",Id);

                            photourl = "http://graph.facebook.com/"+Id+"/picture?type=large";

                            facebookLogin();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

                Log.d("facebook error", error.toString());

            }

        });

    }

    //====================================== Social Login Start =================================

    public void facebookLogin(){


        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_VERIFYFACEBOOK;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                //Log.d("response=====", String.valueOf(response));

                parseVerifyFacebookResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.RES_USERID,String.valueOf(Commons.g_user.get_idx()));

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FreeBeeApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseVerifyFacebookResponse(String response){

        try {

            Log.d("=facebook===response", response);
            closeProgress();

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Commons.g_user.set_verify_facebook("Y");
                setEditPage();
            }
        } catch (JSONException e) {

            closeProgress();
            showAlertDialog("Network Error.Please try again.");
            e.printStackTrace();
        }
    }


    private void phoneVerify(){

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("sms:"));
        startActivity(intent);

//        String number = "12346556";  // The number on which you want to send SMS
//        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Connect State", "onConnectionFailed:" + connectionResult);

    }

    @Override
    protected void onStart() {

        super.onStart();

    }

}
