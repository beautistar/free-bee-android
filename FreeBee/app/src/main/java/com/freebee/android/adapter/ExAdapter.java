package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.freebee.android.R;


import java.util.ArrayList;

/**
 * Created by GoldRain on 10/11/2016.
 */

public class ExAdapter extends BaseAdapter {

    Context _context;

    ArrayList<Integer> _imgs = new ArrayList<>();

    public ExAdapter(Context context, ArrayList<Integer> imgs) {
        super();
        this._context = context;
        this._imgs = imgs;
    }

    @Override
    public int getCount() {
        return _imgs.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_contents_gdv, parent, false);

        ImageView ui_imv = (ImageView)convertView.findViewById(R.id.imv_content);
        ui_imv.setImageResource(_imgs.get(position));

        return convertView;
    }
}
