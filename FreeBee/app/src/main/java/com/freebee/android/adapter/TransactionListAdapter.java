package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.freebee.android.R;
import com.freebee.android.main.MyBalanceActivity;
import com.freebee.android.model.TransactionModel;

import java.util.ArrayList;

public class TransactionListAdapter extends BaseAdapter {

    MyBalanceActivity activity;
    ArrayList<TransactionModel> allTransaction = new ArrayList<>();
    ArrayList<TransactionModel> transactionModels = new ArrayList<>();

    public TransactionListAdapter(MyBalanceActivity activity){

        this.activity = activity;
    }

    public void setTransaction(ArrayList<TransactionModel> transactions){

        transactionModels = transactions;
        allTransaction.clear();
        allTransaction = transactionModels;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allTransaction.size();
    }

    @Override
    public Object getItem(int position) {
        return allTransaction.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TransactionHolder holder;
        if (convertView == null) {

            holder = new TransactionHolder();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_transaction_list, parent, false);

            holder.txv_transaction_amount = (TextView) convertView.findViewById(R.id.txv_transaction_amount);
            holder.txv_transaction_id = (TextView) convertView.findViewById(R.id.txv_transaction_id);
            holder.txv_type = (TextView) convertView.findViewById(R.id.txv_type);
            holder.txv_date = (TextView) convertView.findViewById(R.id.txv_date);

            convertView.setTag(holder);
        }
        else {

            holder = (TransactionHolder) convertView.getTag();
        }

        TransactionModel trans = (TransactionModel) allTransaction.get(position);

        holder.txv_transaction_amount.setText("$" + trans.getAmount());
        holder.txv_transaction_id.setText(trans.getTransaction_id());
        holder.txv_type.setText("Type : " + trans.getType());
        holder.txv_date.setText(trans.getCreated_at());

        return convertView;
    }

    public class TransactionHolder{

        TextView txv_transaction_amount, txv_transaction_id,txv_type, txv_date;
    }
}
