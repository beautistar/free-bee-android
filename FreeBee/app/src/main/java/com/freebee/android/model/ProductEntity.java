package com.freebee.android.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/28/2016.
 */

public class ProductEntity implements Serializable {

    int _id = 0 ;
    String _name = "";
    String _imagUrl = "";
    String _description = "";
    int _price = 0 ;
    String _category = "";
    double _latitude = 0 ;
    double _longtitude = 0;
    String _like_product = "";            // _like_product = L: Like: U : UnLike:
    String _cart_product = "";
    String _ownerName = "";
    String _owner_imageUrl = "";
    String _owner_phone = "";
    int _owner_id = 0;
    int _count_mutual_friend = 0 ;
    String _is_sold = ""; //N: NOT SOLD; Y: SOLD:
    String _verify_facebook = "" ;//Y: Verify; N:Unverify:
    String _verify_google = "";
    String _verify_phone = "";
    int _quantity_per_user = 0;
    int _quantity_available = 0;
    int _age_max = 0;
    int _age_min = 0;
    String _gender = "", _hobby = "", _industry = "",
            _occupation = "", _zipcode = "", _city = "", _state = "", _country = "";
    int _sold_count = 0;
    int _cart_count = 0;
    String type = ""; //premium, regular;

    ArrayList<String> _productImages = new ArrayList<>();

    public int get_id() {return _id;}

    public void set_id(int _id) {this._id = _id;}

    public String get_name() {return _name;}

    public void set_name(String _name) {this._name = _name;}

    public String get_imagUrl() {return _imagUrl;}

    public void set_imagUrl(String _imagUrl) {this._imagUrl = _imagUrl;}

    public String get_description() {return _description;}

    public void set_description(String _description) {this._description = _description;}

    public int get_price() {return _price;}

    public void set_price(int _price) {this._price = _price;}

    public String get_category() {return _category;}

    public void set_category(String _category) {this._category = _category;}

    public double get_latitude() {return _latitude;}

    public void set_latitude(double _latitude) {this._latitude = _latitude;}

    public double get_longtitude() { return _longtitude;}

    public void set_longtitude(double _longtitude) {this._longtitude = _longtitude;}

    public String get_like_product() {return _like_product;}

    public void set_like_product(String _like_product) {this._like_product = _like_product;}

    public String get_ownerName() {return _ownerName;}

    public void set_ownerName(String _ownerName) {this._ownerName = _ownerName;}

    public String get_owner_imageUrl() {return _owner_imageUrl;}

    public void set_owner_imageUrl(String _owner_imageUrl) {this._owner_imageUrl = _owner_imageUrl;}

    public int get_count_mutual_friend() {return _count_mutual_friend;}

    public void set_count_mutual_friend(int _count_mutual_friend) {this._count_mutual_friend = _count_mutual_friend;}

    public String  get_is_sold() {return _is_sold;}

    public void set_is_sold(String _is_sold) {this._is_sold = _is_sold;}

    public ArrayList<String> get_productImages() {
        return _productImages;
    }

    public void set_productImages(ArrayList<String> _productImages) {
        this._productImages = _productImages;
    }

    public String get_owner_phone() {
        return _owner_phone;
    }

    public void set_owner_phone(String _owner_phone) {
        this._owner_phone = _owner_phone;
    }

    public int get_owner_id() {
        return _owner_id;
    }

    public void set_owner_id(int _owner_id) {
        this._owner_id = _owner_id;
    }

    public String get_verify_facebook() {
        return _verify_facebook;
    }

    public void set_verify_facebook(String _verify_facebook) {
        this._verify_facebook = _verify_facebook;
    }

    public String get_verify_google() {
        return _verify_google;
    }

    public void set_verify_google(String _verify_google) {
        this._verify_google = _verify_google;
    }

    public String get_verify_phone() {
        return _verify_phone;
    }

    public void set_verify_phone(String _verify_phone) {
        this._verify_phone = _verify_phone;
    }

    public int get_quantity_per_user() {
        return _quantity_per_user;
    }

    public void set_quantity_per_user(int _quantity_user) {
        this._quantity_per_user = _quantity_user;
    }

    public int get_quantity_available() {
        return _quantity_available;
    }

    public void set_quantity_available(int _quantity_available) {
        this._quantity_available = _quantity_available;
    }

    public int get_age_max() {
        return _age_max;
    }

    public void set_age_max(int _age_max) {
        this._age_max = _age_max;
    }

    public int get_age_min() {
        return _age_min;
    }

    public void set_age_min(int _age_min) {
        this._age_min = _age_min;
    }

    public String get_gender() {
        return _gender;
    }

    public void set_gender(String _gender) {
        this._gender = _gender;
    }

    public String get_hobby() {
        return _hobby;
    }

    public void set_hobby(String _hobby) {
        this._hobby = _hobby;
    }

    public String get_industry() {
        return _industry;
    }

    public void set_industry(String _industry) {
        this._industry = _industry;
    }

    public String get_occupation() {
        return _occupation;
    }

    public void set_occupation(String _occupation) {
        this._occupation = _occupation;
    }

    public String get_zipcode() {
        return _zipcode;
    }

    public void set_zipcode(String _zipcode) {
        this._zipcode = _zipcode;
    }

    public String get_city() {
        return _city;
    }

    public void set_city(String _city) {
        this._city = _city;
    }

    public String get_state() {
        return _state;
    }

    public void set_state(String _state) {
        this._state = _state;
    }

    public String get_country() {
        return _country;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    public String get_cart_product() {
        return _cart_product;
    }

    public void set_cart_product(String _cart_product) {
        this._cart_product = _cart_product;
    }

    public int get_sold_count() {
        return _sold_count;
    }

    public void set_sold_count(int _sold_count) {
        this._sold_count = _sold_count;
    }

    public String getType() {
        return type;
    }

    public int get_cart_count() {
        return _cart_count;
    }

    public void set_cart_count(int _cart_count) {
        this._cart_count = _cart_count;
    }

    public void setType(String type) {
        this.type = type;
    }
}
