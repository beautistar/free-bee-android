package com.freebee.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.freebee.android.R;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.freebee.android.commons.Constants;
import com.freebee.android.main.UserProfileActivity;
import com.freebee.android.model.ProductEntity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/6/2017.
 */

public class GridViewBoughtAdapter extends BaseAdapter {

    UserProfileActivity _activity;
    ArrayList<ProductEntity> _allBought = new ArrayList<>();

    public GridViewBoughtAdapter(UserProfileActivity activity, ArrayList<ProductEntity> bought){

        this._activity = activity;
        _allBought.addAll(bought);
    }
    @Override
    public int getCount() {
        return _allBought.size();
    }

    @Override
    public Object getItem(int position) {
        return _allBought.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        BoughtHolder boughtHolder;

        if (convertView == null){

            boughtHolder = new BoughtHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_bought_gdv, parent, false);

            boughtHolder.imvImage = (RoundedImageView) convertView.findViewById(R.id.imv_product);
            boughtHolder.txvName = (TextView)convertView.findViewById(R.id.txv_name);

            convertView.setTag(boughtHolder);
        }else {

            boughtHolder = (BoughtHolder)convertView.getTag();
        }

        final ProductEntity productEntity = _allBought.get(position);

        boughtHolder.txvName.setText(productEntity.get_name());

        Glide.with(_activity).load(productEntity.get_imagUrl()).placeholder(R.drawable.place_holder_pattern).into(boughtHolder.imvImage);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.list_position = position ;

                int productID = productEntity.get_id();
                //_activity.gotoProduct(productID);

            }
        });

        return convertView;
    }

    public class BoughtHolder{

        public RoundedImageView imvImage;
        public TextView txvName;
    }
}
